<?php 
/****** SIDEBAR WITH WIDGET AREA ******/

if (!is_active_sidebar('sidebar'))
	return;
?>

<div class="sidebar wysiwyg" role="complementary">
  <div class="cover"></div>
  <div id="sidebar-content">
  	<?php
    	$sidebar = get_dynamic_sidebar('sidebar');
      print $sidebar
    ?>
  </div>
</div>