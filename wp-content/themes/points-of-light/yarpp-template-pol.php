<?php if (have_posts()):?>

<section class="module module-type-related-content bg-white">
	<div class="container">

		<header class="related-content-header">
			<h2>Related Posts</h2>
		</header>
		<div class="related-content-cta">
			<a href="<?= get_permalink(124) ?>" class="button">See All Posts</a>
		</div>

		<div class="card-grid-wrapper">
			<div class="card-grid">

				<?php
					while (have_posts()) : the_post();
						$url = get_permalink();
						$dpol = in_category(5);
						$image = get_the_post_thumbnail($post, 'card-top');
						$title = get_the_title();
						$date = get_the_date('M j, Y');
						$author = get_the_author_meta('display_name');
						$excerpt = get_the_excerpt();
				?>

				<a href="<?= $url ?>" class="card <?= empty($dpol) ? 'card-navy' : 'card-green' ?>">

					<?php if ( !empty($image) ): ?>

					<div class="image">
						<?= $image ?>
					</div>

					<?php endif; ?>

					<div class="text">

						<h6 class="title"><?= $title ?></h6>

						<?php if ( empty($image) ): ?>

						<p class="excerpt"><?= $excerpt ?></p>

						<?php endif; ?>

						<div class="meta">
							<span class="date"><?= $date ?></span>
							<span class="byline">By: <span class="author"><?= $author ?></span></span>
						</div>
					</div>
				</a>

				<?php endwhile; ?>

			</div>
		</div>
	</div>
</section>

<?php endif; ?>
