<form action="https://www.classy.org/give/159379/#!/donation/checkout" class="donation-form">
  <span class="dollar-sign">$</span>
  <input type="text" placeholder="Gift Amount" name="amount" class="donation-number">
  <button type="submit" class="donation-submit">Make A Donation</button>
</form>
