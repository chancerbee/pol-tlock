<?php
/****** FLEX CONTENT: GLOBAL ******/
  while ( have_rows( 'content_blocks' ) ):
    the_row();
    $type = str_replace( '_', '-', get_row_layout() );
    $special = ''; //initialize a special var for inclusion in module_classes

    // SEMI-GLOBAL MODULE VARS
    $heading = get_sub_field( 'module_heading' ) ?? null;
    $subhead = get_sub_field( 'module_subhead' ) ?? null;
    $bg_color = get_sub_field( 'module_bg_color' ) ?? 'block-specific';
    $bg_type = get_sub_field( 'module_background_type') ?? null;
    if($bg_type == 'color') {
      $color = get_sub_field('color');
      if(is_array($color))
        $bg_color = $color['module_bg_color'];
      else
        $bg_color = $color;
    }

    // SPECIFIC BLOCKS
      // CTA BOX
      if ( $type == 'cta-box' ) {
        $cta_buttons = get_sub_field('cta_buttons');
        $donation_form = get_sub_field('donation_form');
        if($bg_type == 'image') {
          $bg_image = wp_get_attachment_image_src( get_sub_field('background_image'), 'bg')[0];
          //treat as navy bg for light text
          $bg_color = 'navy';
          $inline_cover_style = 'style="background-image:url('.$bg_image.')"';
        }
      }
      // STAT BOXES
      if ( $type == 'stat-boxes' && $bg_type == 'image' )
        if ( !empty($heading) ) {
          // overall block treated as white, (header is white, will get top border if after another white module)
          // bg image will be just in the stat-boxes div itself
          $bg_color = 'white';
          $special = 'bottom-edge-bleed';
        } else {
          $special = 'vertical-edge-bleed';
        }


    // HEADER CLASSES & STYLES
    $module_classes = [
      'module-type-'.$type,
      'bg-'.$bg_color,
      $special
    ];
    $module_classes = implode( ' ', $module_classes );

    // POSSIBLE ANCHOR LINKS
    $anchor_id = get_sub_field('anchor_id');
?>


<?php if ( !empty($anchor_id) ) echo '<a class="anchor" id="'.$anchor_id.'"></a>'; ?>

<section class="module <?= $module_classes ?>">
  <?php if($type == 'cta-box' && $bg_type == 'image' && !empty($bg_image)): ?>
    <div class="cover" <?= $inline_cover_style ?>></div>
  <?php endif; ?>
  <div class="container">

    <?php
      if ( !empty( $heading ) || !empty( $subhead ) ) {
        print '<header class="module-header">';
        print $heading ? '<h2 class="module-heading">'.$heading.'</h2>' : '';
        print $subhead ? '<p class="module-subheading">'.$subhead.'</p>' : '';
        print '</header>';
      }

      if ( !empty( $type ) )
        include( realpath( dirname( __FILE__ ) . '/../blocks/'.$type.'.php' ) );

      if ( have_rows( 'buttons' ) ) {
        print '<footer class="module-footer">';
        print_buttons();
        print '</footer>';
      }
      //CTA Box Buttons or form
      if($type == 'cta-box') {
        if(!empty($donation_form)) {
          include( realpath( dirname( __FILE__ ) . '/donation-form.php'));
        }elseif(is_array($cta_buttons)) {
          print_buttons_group($cta_buttons);
        }
      }
    ?>

  </div>
</section>

<?php
  endwhile;
?>
