<?php

  // double-check user login
  global $user_login;
  $check = is_user_logged_in() ? '=' : '!=';

  // unset if we are looking at ungated resource index
  if ( get_the_id() == 965 ) $check = '!=';
?>

<section class="content resource-grid">
  <div class="container">
    <div class="resource-filters">
    <?php /* WILL USE THIS IN THE FUTURE - chance
      <a <?= get_the_id() == 969 ? 'class="active gated-link"' : 'class="gated-link"' ?> href="<?= get_permalink(969) ?>"><i class="icon-locked"></i></a>
      <a <?= get_the_id() == 965 ? 'class="active gated-link"' : 'class="gated-link"' ?> href="<?= get_permalink(965) ?>"><i class="icon-unlocked"></i></a>
      */ ?>
    <?php
      $filter_args = 'id="resource_filters" ';
      $filter_args.= 'target="resource_results" ';
      echo do_shortcode('[ajax_load_more_filters '.$filter_args.']');
    ?>

    </div>

    <?php
      $alm_args = 'id="resource_results" ';
      $alm_args.= 'css_classes="card-grid bg-white" ';
      $alm_args.= 'post_type="resource" ';
      $alm_args.= 'meta_key="gate_resource" ';
      $alm_args.= 'meta_value="1" ';
      $alm_args.= 'meta_compare="'.$check.'" ';
      $alm_args.= 'repeater="template_2" ';
      $alm_args.= 'target="resource_filters" ';
      $alm_args.= 'filters="true" ';
      $alm_args.= 'transition_container="false" ';
      $alm_args.= 'posts_per_page="16" ';
      $alm_args.= 'order="DESC" ';
      $alm_args.= 'orderby="date" ';
      $alm_args.= 'scroll="false" ';
      $alm_args.= 'button_label="Load More" ';
      echo do_shortcode('[ajax_load_more '.$alm_args.']');
    ?>

  </div>
</section>
