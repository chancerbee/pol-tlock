  <?php
    $insta_profile_link = get_field('url_instagram', 'option');
    $hashtag = 'bethechange';
  ?>


  <section class="content type-instafeed">
    <div class="container">
      <ul class="instagram-grid"></ul>
      <div class="links">
        <?= !empty($insta_profile_link) ? '<a href="'.$insta_profile_link.'" class="instalink" target="_blank"><i class="icon-instagram"></i> @pointsoflight</a>' : '' ?>
        <a href="https://www.instagram.com/explore/tags/<?= $hashtag ?>" class="hashtag">#<?= $hashtag ?></a>
      </div>
    </div>
  </section>
