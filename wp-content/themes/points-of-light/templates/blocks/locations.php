<?php /****** FLEX CONTENT: OFFICE LOCATION BLOCK ******/
  global $maps_api_key;
?>
 <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=<?= $maps_api_key ?>"></script>

<?php
  $atl_label = get_field('atl_label', 'option');
  $atl_address = get_field('atl_address', 'option');
  $atl_address_link = 'https://maps.google.com/?q='.$atl_address['address'];
  $atl_lat = $atl_address['lat'];
  $atl_lng = $atl_address['lng'];

  $dc_label = get_field('dc_label', 'option');
  $dc_address = get_field('dc_address', 'option');
  $dc_address_link = 'https://maps.google.com/?q='.$dc_address['address'];
  $dc_lat = $dc_address['lat'];
  $dc_lng = $dc_address['lng'];

  $nyc_label = get_field('nyc_label', 'option');
  $nyc_address = get_field('nyc_address', 'option');
  $nyc_address_link = 'https://maps.google.com/?q='.$nyc_address['address'];
  $nyc_lat = $nyc_address['lat'];
  $nyc_lng = $nyc_address['lng'];
?>
<div class="office-locations">
  <div class="office-location">
    <div class="acf-map">
      <div class="marker" data-lat="<?= $atl_lat ?>" data-lng="<?= $atl_lng ?>"></div>
    </div>
    <?= !empty($atl_label) ? '<h3>'.$atl_label.'</h3>' : '' ?>
    <a href="<?= $atl_address_link ?>" target="_blank">
      <?php $address = explode( ",", $atl_address['address']);
      echo $address[0].'<br/>'; //street number and line break
      echo $address[1].','.$address[2].''.$address[3]; //city, state
      ?>
    </a>
  </div>
  <div class="office-location">
    <div class="acf-map">
      <div class="marker" data-lat="<?= $dc_lat ?>" data-lng="<?= $dc_lng ?>"></div>
    </div>
    <?= !empty($dc_label) ? '<h3>'.$dc_label.'</h3>' : '' ?>
    <a href="<?= $dc_address_link ?>" target="_blank">
      <?php $address = explode( ",", $dc_address['address']);
      echo $address[0].'<br/>'; //street number and line break
      echo $address[1].','.$address[2].''.$address[3]; //city, state
      ?>
    </a>
  </div>
  <div class="office-location">
    <div class="acf-map">
      <div class="marker" data-lat="<?= $nyc_lat ?>" data-lng="<?= $nyc_lng ?>"></div>
    </div>
    <?= !empty($nyc_label) ? '<h3>'.$nyc_label.'</h3>' : '' ?>
    <a href="<?= $nyc_address_link ?>" target="_blank">
      <?php $address = explode( ",", $nyc_address['address']);
      echo $address[0].'<br/>'; //street number and line break
      echo $address[1].','.$address[2].''.$address[3]; //city, state
      ?>
    </a>
  </div>
</div>
