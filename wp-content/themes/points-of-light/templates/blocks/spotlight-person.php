<?php /****** FLEX CONTENT: SPOTLIGHT PERSON BLOCK ******/ ?>
<?php
  $name = get_sub_field('spotlight_name');
  $title = get_sub_field('spotlight_title');
  $portrait = get_sub_field('spotlight_portrait');
  $bio = get_sub_field('spotlight_bio');
?>
<div class="spotlight-person">
  <div class="image">
    <?= wp_get_attachment_image($portrait, 'spotlight-person'); ?>
  </div>
  <div class="content">
    <h3 class="name"><?= $name ?></h3>
    <?= $title ? '<h4 class="title">'.$title.'</h4>' : '' ?>
    <div class="wysiwyg"><?= $bio ?></div>
  </div>
</div>
