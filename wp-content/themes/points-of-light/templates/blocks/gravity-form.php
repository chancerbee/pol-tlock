<?php
/****** FLEX CONTENT: INTRO TEXT ******/
  $headline = get_sub_field('headline');
  $text = get_sub_field('text') ?? null;
  $gform_id = get_sub_field('form') ?? null;

?>

<div class="left-col">
  <div class="cover dark" style="opacity: .25"></div>
  <div class="text">
    <h2 class="form-headline"><?= $headline ?></h2>

    <?php if ( !empty( $text ) ): ?>

      <p class="intro-text"><?= $text ?></p>

    <?php endif; ?>

  </div>
</div>

<div class="right-col">

  <?php
    gravity_form(
      $id_or_title = $gform_id,
      $display_title = false,
      $display_description = false,
      $display_inactive = false,
      $field_values = null,
      $ajax = true,
      $tabindex = 1,
      $echo = true
    );
  ?>

</div>
