<?php /****** FLEX CONTENT: PRODUCT GRID BLOCK ******/ ?>

<?php
  //Display Options
  $alignment = get_sub_field('alignment');
  $display_type = get_sub_field('display_type');

  if(have_rows('product_grid_items')) :
?>
<div class="product-grid align-<?= $alignment ?> display-<?= $display_type ?>">
  <?php while(have_rows('product_grid_items')): the_row();
    $accent_color = get_sub_field('accent_color')['color'];

    if($display_type == 'with-bg-image') {
      $accent_color = 'none';
    }

    $background_image = get_sub_field('background_image');

    $icon = get_sub_field('icon');
    $headline = get_sub_field('headline'); //required
    $body_copy = get_sub_field('body_copy');
    $link = get_sub_field('link');
    $link_type = get_sub_field('link_type');
    $extra = '';
    if($link == 'page')
      $link_url = get_sub_field('page_link');
    if($link == 'resources') {
      $resource_type = get_sub_field('resource_type');
      $resource_slug = $resource_type->slug;
      $link_url = get_home_url() . '/resources/?resource-type='.$resource_slug;
    }
    if($link == 'external') {
      $link_url = get_sub_field('external_link');
      $extra = 'target="_blank"';
    }

  ?>
    <?php if($link == 'none'): ?>
      <div class="product-grid-item accent-<?= $accent_color ?>">
        <?= $display_type == 'with-bg-image' ? '<div class="cover bg-image">'.wp_get_attachment_image($background_image, 'one-third-container').'</div>' : '' ?>
        <div class="content">
          <?= $icon ? '<i class="'.$icon.'"></i>' : '' ?>
          <h3><?= $headline ?></h3>
          <?= $body_copy ? '<p>'.$body_copy.'</p>' : '' ?>
        </div>
      </div>
    <?php elseif($link_type == 'block'): ?>
      <a href="<?= $link_url ?>" class="product-grid-item product-grid-link accent-<?= $accent_color ?>" <?= $extra ?>>
        <?= $display_type == 'with-bg-image' ? '<div class="cover bg-image">'.wp_get_attachment_image($background_image, 'one-third-container').'</div>' : '' ?>
        <div class="content">
          <?= $icon ? '<i class="'.$icon.'"></i>' : '' ?>
          <h3><?= $headline ?></h3>
          <?= $body_copy ? '<p>'.$body_copy.'</p>' : '' ?>
          <span class="learn-more">Learn More <i class="icon-arrow-right"></i></span>
        </div>
      </a>
    <?php elseif($link_type == 'text'): ?>
      <div class="product-grid-item product-grid-text-link accent-<?= $accent_color ?>">
        <?= $display_type == 'with-bg-image' ? '<div class="cover bg-image">'.wp_get_attachment_image($background_image, 'one-third-container').'</div>' : '' ?>
        <div class="content">
          <?= $icon ? '<i class="'.$icon.'"></i>' : '' ?>
          <h3><?= $headline ?></h3>
          <?= $body_copy ? '<p>'.$body_copy.'</p>' : '' ?>
          <a href="<?= $link_url ?>" class="learn-more" <?= $extra ?>>Learn More <i class="icon-arrow-right"></i></a>
        </div>
      </div>
    <?php endif; ?>
  <?php endwhile; ?>
</div>
<?php endif; ?>
