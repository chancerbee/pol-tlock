<?php
  /****** FLEX CONTENT: VOLUNTEER SEARCH ******/
?>

<?php
  // Get query vars to pre-populate filters
  $q = $_GET['q'] ?? '';
  $ip_loc = $_GET['aLLVIP'] ?? null;
    if ($ip_loc === '1' || $ip_loc === 'true') $ip_loc = true;
  $tag = $_GET['tR'][0] ?? null;

  $issues = [
    "hunger & homelessness" => "Hunger & Homelessness",
    "indoors" => "Indoor",
    "children & youth education" => "Children & Youth Education",
    "education" => "Education",
    "health & wellness" => "Health & Wellness",
    "adult education" => "Adult Education",
    "family friendly" => "Family Friendly",
    "senior services" => "Senior Services",
    "family services" => "Family Services",
    "environment" => "Environment",
    "outdoors" => "Outdoor",
    "handicap accessible" => "Accessible",
    "volunteering from home" => "Remote",
    "arts & culture" => "Arts & Culture",
    "physical labor" => "Physical Labor",
    "animals" => "Animals",
    "advocacy" => "Advocacy",
    "nonprofit organization" => "Nonprofit",
    "children & youth" => "Children & Youth",
    "technology" => "Technology",
    "fundraising" => "Fundraising",
    "marketing" => "Marketing",
    "sports & recreation" => "Sports & Recreation",
    "food prep" => "Food Preparation",
    "medical" => "Medical",
    "seniors" => "Seniors",
    "schools" => "Schools",
    "disaster & emergency services" => "Disaster & Emergency Services",
    "disabilities" => "Disabilities",
    "hunger / food" => "Hunger",
    "finance" => "Finance",
    "travel & cross-cultural education" => "Travel",
    "mentoring services" => "Mentor Services",
    "faith-based service" => "Faith-based",
    "board service" => "Board Service",
    "literacy" => "Literacy",
    "legal" => "Legal",
    "advocacy & human rights" => "Human Rights",
    "death and dying" => "Death & Dying",
    "homelessness" => "Homelessness",
    "mental health" => "Mental Health",
    "housing" => "Housing Services",
    "women" => "Women's Services",
    "youth" => "Youth",
    "immigrant & refugee services" => "Immigration & Refugee Services",
    "domestic violence" => "Domestic Violence",
    "transportation" => "Transportation",
    "office & admin" => "Office & Administrative",
    "veterans & military families" => "Veterans & Military Families",
    "emergency services" => "Emergency Services",
    "writing" => "Writing",
    "children" => "Children",
  ];
  ksort($issues);

?>

<div class="filters-wrap">
  <div class="algolia-search-inputs">

    <div class="algolia-search-filter">
      <select name="algolia-location" id="algolia-location">
        <option value="#">Filter by Location</option>
        <option value="0" <?= $ip_loc == '0' ? 'selected' : '' ?>>Everywhere</option>
        <option value="1" <?= $ip_loc == '1' ? 'selected' : '' ?>>Near Me</option>
      </select>
    </div>

    <div class="algolia-search-filter issue-filters">
      <select id="algolia-tag" name="algolia-tag">
        <option value="#">Filter by Issue</option>

        <?php foreach($issues as $value => $title): ?>

        <option value="<?= $value ?>" <?= $tag == $value ? 'selected' : ''?>>
          <?= $title ?>
        </option>

        <?php endforeach; ?>

      </select>
    </div>

    <div class="algolia-search-input site-search-form">
      <input class="search-input" name="algolia-query" id="algolia-query" type="search" placeholder="Search" value="<?= $q ?>">
      <button id="algolia-search">
        <i class="icon-search"></i>
      </button>
    </div>

  </div>
</div>

<?php // Volunteer Search Page gets results and footer
  if ( $id == 762 ):
?>

<div id="algolia-result"></div>

<footer class="volunteer-search-results-footer">
  <a id="algolia-more" class="button">Show More</a>
</footer>

<?php // All other pages get hidden link to Volunteer Search Page
  else :
?>

  <a id="algolia-fetch" class="hidden" href="<?= get_permalink(762) ?>"></a>

<?php endif; ?>
