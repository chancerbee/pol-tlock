<?php
  /****** FLEX CONTENT: RELATED RESOURCES ******/
  $headline = get_sub_field('headline');
  $related_posts = get_sub_field('posts');
  $include_huge = get_sub_field('include_huge');
  if ( !empty($include_huge) ) {
    $huge_post = get_sub_field('huge_item');
    array_unshift($related_posts , $huge_post);
  }

?>

<header class="related-content-header <?= !empty($huge_post) ? 'centered' : '' ?>">
  <?= !empty($headline) ? '<h2>'.$headline.'</h2>' : '' ?>
</header>

<?php
  if ( empty($include_huge) && !empty(get_sub_field('cta')) ) {
    echo '<div class="related-content-cta">';
      print_link('cta', 'button');
    echo '</div>';
  }
?>

<div class="card-grid-wrapper">
  <div class="card-grid<?= !empty($huge_post) ? ' related-content-grid' : '' ?>">

  <?php foreach($related_posts as $related_post):

    // general
    if ( is_array($related_post) ) {
      //this is a normal post, lives in array keyed ['post'], drill down to it
      $related_post = $related_post['post'];
      $is_huge = false;
      $img_size = 'card-top';
    } else {
      // this is our huge post object added directly to the $related_posts arr, flag it
      $is_huge = true;
      $img_size = 'card-top-huge';
    }
    $url = get_permalink($related_post->ID);
    $post_type = $related_post->post_type;
    $title = $related_post->post_title;
    $date = get_the_date('M d, Y', $related_post->ID);
    $author = get_the_author_meta('display_name', $related_post->post_author);
    $image = get_the_post_thumbnail($related_post->ID, $img_size);

    // press releases
    if ( $post_type == 'press_release' ) {
      $author = null;
      $color_class = 'blue';
      $post_type = 'Press Release';
    }

    // blog posts
    if ( $post_type == 'post' ) {
      $excerpt = get_excerpt_by_id($related_post->ID);
      $color_class = 'navy';
      $post_type = 'Story';
      if ( in_category(5, $related_post->ID) ) {
        $color_class = 'green';
        $post_type = 'Daily Point of Light';
      }
    }

    // resources
    if ( $post_type == 'resource' ) {
      $author = null;
      $color_class = 'navy';
      $image = wp_get_attachment_image(get_field('resource_image', $related_post->ID), $img_size);
      $resource_type_id = get_field('resource_type', $related_post->ID);
      $resource_type = get_term($resource_type_id);
      $resource_type_name = $resource_type->name ?? null;
      $resource_type_slug = $resource_type->slug ?? null;
      if($resource_type_slug == 'video')
        $image = wp_get_attachment_image(get_field('resource_video_image', $related_post->ID), $img_size);
        $color_class = 'orange';
      if($resource_type_slug == 'data-sheet')
        $color_class = 'green';
      if($resource_type_slug == 'toolkit')
        $color_class = 'blue';
      if($resource_type_slug == 'report')
        $color_class = 'pink';
      $post_type = $resource_type_name;
    }
  ?>

    <a href="<?= $url ?>" class="card card-<?= $color_class ?> <?= $is_huge === true ? 'huge' : '' ?>">

      <?php if ( !empty($image) ): ?>

      <div class="image ie-object-fit">
        <?= $image ?>
      </div>

      <?php endif; ?>

      <div class="text">
        <p class="post-type"><?= $post_type ?></p>
        <h6 class="title"><?= $title ?></h6>

        <?php if ( empty($image) && ($related_post->post_type == 'post') ): ?>

        <p class="excerpt"><?= $excerpt ?></p>

        <?php endif; ?>

        <div class="meta">
          <span class="date"><?= $date ?></span>

          <?php if ( !empty($author) ): ?>

          <span class="byline">By: <span class="author"><?= $author ?></span></span>

          <?php endif; ?>

        </div>
      </div>

    </a>

  <?php endforeach; ?>

  </div>
</div>

<?php if ( !empty($huge_post) && !empty(get_sub_field('cta')) ): ?>

<footer class="related-content-footer">
  <?php print_link('cta', 'button');  ?>
</footer>

<?php endif; ?>
