<?php
  /****** FLEX CONTENT: STAT BOXES ******/
  $bg_type = get_sub_field('module_background_type');
  $bg_image = wp_get_attachment_image_src( get_sub_field('background_image'), 'bg')[0];
  $inline_bg_style = '';
  if ( !empty($bg_image) && $bg_type == 'image' )
    $inline_bg_style = 'style="background-image:url('.$bg_image.')"';
  $text_style = get_sub_field('text_type');

?>


<div class="stat-boxes text-style-<?= $text_style ?> background-<?= $bg_type ?>" <?= $inline_bg_style ?>>
  <div class="container">

    <?php
      while(have_rows('stats')):
        the_row();
        $number = get_sub_field('number');
        $unit = get_sub_field('unit');
        $text = get_sub_field('text');
        $cta = get_sub_field('cta');
    ?>

    <div class="stat-box">
      <h5 class="number"><?= $number ?></h5>
      <h6 class="unit"><?= $unit ?></h6>
      <figure class="accent"></figure>
      <p class="text"><?= $text ?></p>

      <?php if ( !empty($cta) ) print_link('cta', 'cta', 'arrow-right'); ?>
    </div>

    <?php endwhile; ?>

  </div>
</div>
