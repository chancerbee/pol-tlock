<?php /****** FLEX CONTENT: INTRO TEXT ******/ ?>

<!-- Vertical Tabber (Desktop) -->
<div class="vertical-tabber js-vertical-tabber">

  <div class="control-group">

    <figure class="indicator"></figure>

    <?php
      $titles = [];
      $tabs = [];
      $i = 0;
      while(have_rows('tabs')):
        the_row();

        if ( $i == 0 ) $state = 'active';
        else $state = 'inactive';

        $tab = get_sub_field('tab');
        $titles[] = $tab;
        $tabs[] = get_sub_field('tab_content');
    ?>

    <a class="tab-control js-tab-control"
       data-tab="<?= $i ?>"
       data-state="<?= $state ?>"
       >
      <?= $tab ?>
    </a>

    <?php
      $i++;
      endwhile;
    ?>

  </div>

  <div class="content-group">

    <?php

      foreach( $tabs as $i => $tab):

        //use output buffer to capture this markup, so we can reuse it in responsive accordion
        ob_start();

        if ( $i == 0 ) $state = 'active';
        else $state = 'inactive';

        $columns = $tab['columns'];
        $one_col_content = $tab['one-col-content'] ?? null;
    ?>

    <div class="tab-content js-tab-content <?= $state == 'active' ? '' : 'js-pre-init' ?>"
      data-tab="<?= $i ?>"
      data-state="<?= $state ?>"
    >

      <?php if ( $columns == 'one' ): ?>

      <div class="single-col wysiwyg">
        <?= $one_col_content ?>
      </div>

      <?php else: ?>

      <div class="cols">

        <?php
          $cols = ['left', 'right'];
          foreach ( $cols as $pos ):

            $col_content = $tab[$pos.'-col-content'];

            $add_image = $col_content[$pos.'_add_image'] ?? null;
            $add_bullet_box = $col_content[$pos.'_add_bullet_box'] ?? null;
            $add_cta = $col_content[$pos.'_add_cta'] ?? null;
            $cta = !empty($add_cta) ? $col_content[$pos.'_cta'] : null;

            $image = wp_get_attachment_image($col_content[$pos.'_image'], 'one-third-container') ?? null;
            $bullet_box_bg = $col_content[$pos.'_bullet_box_bg']['color'] ?? 'transparent';
            $bullets = $col_content[$pos.'_bullets'] ?? null;
            $content = $col_content[$pos.'_content'] ?? null;
        ?>

        <div class="col <?= $pos ?>-col">

          <?php if ( !empty($image) ): ?>

          <div class="image">
            <?= $image ?>
          </div>

          <?php endif; if ( !empty($bullets) ): ?>

          <div class="bullet-box bg-<?= $bullet_box_bg ?>">
            <ul>
              <?php foreach ( $bullets as $k => $v ) echo '<li>'.$v['bullet'].'</li>'; ?>
            </ul>
          </div>

          <?php endif; if ( !empty($cta) ): ?>

          <a class="button max" href="<?= $cta['url'] ?>" target="<?= $cta['target'] ?>">
            <?= $cta['title'] ?>
          </a>

          <?php endif; if ( !empty($content) ): ?>

          <div class="wysiwyg">
            <?= $content ?>
          </div>

          <?php endif; ?>

        </div>

        <?php endforeach; ?>

      </div>

      <?php endif; ?>

    </div>

    <?php

      // use "variable variable" to store each output buffer under an indexed var name
      // so that we can loop through tabs and output this content again in our responsive accordion
      $$i = ob_get_clean();
      // print now
      echo ${$i};
    ?>

    <?php endforeach; ?>

  </div>
</div>

<!-- Accordion Group (mobile) -->
<div class="vertical-tabber-accordion">

  <?php foreach( $titles as $i => $title): ?>

  <div class="dropdown">
    <a class="dropdown-button button max sharp green">
      <span class="title"><?= $title ?></span>
      <i class="icon-arrow-down"></i>
    </a>
    <div class="dropdown-items">

      <?php
        // print output buffer for each tab again, sans unnecessary js class
        echo str_replace('js-pre-init', '', ${$i});
      ?>

    </div>
  </div>

  <?php endforeach; ?>

</div>
