<?php /****** FLEX CONTENT: Large Image ******/ ?>

<?php
  $image = get_sub_field('image_block_image');
  if(!empty($image))
    echo '<div class="image-wrap">'.wp_get_attachment_image($image, 'full-container').'</div>';
?>
