<?php /****** FLEX CONTENT: EVENTS BLOCK ******/ ?>

<?php
  if(have_rows('events')) :
?>
<ul class="events-list">
  <?php while(have_rows('events')): the_row();
    $date = get_sub_field('date');
    $title = get_sub_field('title');
    $venue = get_sub_field('venue');
    $location = get_sub_field('location');
    $desc = get_sub_field('description');
    $link = get_sub_field('link');

  ?>
  <li class="events-list-item">
    <div class="content">
      <span class="date"><?= $date ?></span>
      <h3 class="title"><?= $title ?></h3>
      <?= $venue ? '<span class="venue">'.$venue.'</span>' : ''?>
      <?= $location ? '<span class="location">'.$location.'</span>' : ''?>
      <?= $desc ? '<p class="desc">'.$desc.'</p>' : ''?>
      <?= $link ? '<a href="'.$link.'">Learn More</a>' : '' ?>
    </div>
  </li>
  <?php endwhile; ?>
</ul>
<?php endif; ?>
