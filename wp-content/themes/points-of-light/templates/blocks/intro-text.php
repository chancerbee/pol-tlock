<?php
/****** FLEX CONTENT: INTRO TEXT ******/
  $column_count = get_sub_field('columns');
  $heading = get_sub_field('heading');
  $text = get_sub_field('text');
  $use_cta = get_sub_field('cta');
  $cta = get_sub_field('cta_link');
  if ( $column_count === 'two' )
    $col_type = get_sub_field('col_type');
?>

<div class='columns-<?= $column_count ?>'>

  <div class="main-col">
    <h2 class="main-heading"><?= $heading ?></h2>
    <div class="main-text wysiwyg">
      <?= $text ?>
    </div>

    <?= !empty($cta) && !empty($use_cta) ? '<a class="button" href="'.$cta['url'].'" target="'.$cta['target'].'" style="margin-top:20px">'.$cta['title'].'</a>' : '' ?>
  </div>

  <?php if ( $column_count === 'two' ): ?>

  <div class="left-col col-type-<?= $col_type ?>">
    <?php
      switch ($col_type):
        case 'text':
          $left_heading = get_sub_field('left_heading');
          $left_text = get_sub_field('left_text');
        ?>

        <h2 class="left-heading"><?= $left_heading ?></h2>
        <p class="left-text"><?= $left_text ?></p>

        <?php break;

        case 'image':
          $left_image = wp_get_attachment_image( get_sub_field( 'left_image' ), 'one-third-container' );
        ?>

        <div class="left-image">
          <?= $left_image ?>
        </div>

        <?php break;
        case 'event':
          $date = get_sub_field('left_date');
          $details = get_sub_field('left_date_details');
          $img = wp_get_attachment_image( get_sub_field('left_date_img'), 'megamenu-blog' );
          $meta = get_sub_field('left_date_meta');
        ?>

          <div class="event-content">
            <h2 class="left-heading date"><?= $date ?></h2>
            <?= !empty($details) ? '<p class="event-details">'.$details.'</p>' : '' ?>
            <?= !empty($img) ? '<div class="event-img">'.$img.'</div>' : '' ?>
            <?= !empty($meta) ? '<p class="event-meta">'.$meta.'</p>' : '' ?>
          </div>

        <?php break;

      endswitch;
    ?>
  </div>

  <?php endif; ?>

</div>
