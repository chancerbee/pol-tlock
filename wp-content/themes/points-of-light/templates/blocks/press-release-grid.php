<?php
  /****** FLEX CONTENT: PRESS RELEASE GRID ******/
  $headline = get_sub_field('headline');
?>

<header class="related-content-header">
  <h2><?= $headline ?></h2>
</header>

<div class="filters-wrap">
  <?php
    $filter_args = 'id="press_filters" ';
    $filter_args.= 'target="press_results" ';
    echo do_shortcode('[ajax_load_more_filters '.$filter_args.']');
  ?>
</div>

<?php
  $alm_args = 'id="press_results" ';
  $alm_args.= 'css_classes="card-grid press-results" ';
  $alm_args.= 'post_type="press_release" ';
  $alm_args.= 'repeater="template_4" ';
  $alm_args.= 'target="press_filters" ';
  $alm_args.= 'filters="true" ';
  $alm_args.= 'transition_container="false" ';
  $alm_args.= 'posts_per_page="8" ';
  $alm_args.= 'meta_key="year"  ';
  $alm_args.= 'orderby="meta_value_num" ';
  $alm_args.= 'order="DESC" ';
  $alm_args.= 'scroll="false" ';
  $alm_args.= 'button_label="Load More" ';
  echo do_shortcode('[ajax_load_more '.$alm_args.']');
?>
