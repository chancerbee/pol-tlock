<?php /****** FLEX CONTENT: BOARD/HONOREE BLOCK ******/ ?>

<?php if(have_rows('board_list')): ?>
<ul class="board-list">
  <?php while(have_rows('board_list')): the_row();
    $headline = get_sub_field('headline');
    $subhead = get_sub_field('subhead');
    $desc = get_sub_field('description');
    $link = get_sub_field('link');
    if($link == 'person'){
      $person_id = get_sub_field('person_link');
      $url = get_permalink($person_id);
      $extra = null;
    }elseif($link == 'page'){
      $url = get_sub_field('page_link');

    }elseif($link == 'url') {
      $url = get_sub_field('url');
      $external = get_sub_field('external_link');
      if($external)
        $extra = 'target="_blank"';
    }
  ?>
  <li class="list-member">
    <?php if($link != 'none') {
      echo '<a href="'.$url.'" '.$extra.'><h3>'.$headline.'</h3></a>';
    }else {
      echo '<h3>'.$headline.'</h3>';
    }
    ?>
    <?= $subhead ? '<p class="subhead">'.$subhead.'</p>' : '' ?>
    <?= $desc ? '<p class="desc">'.$desc.'</p>' : '' ?>
  </li>
   <?php endwhile; ?>
</ul>
<?php endif; ?>
