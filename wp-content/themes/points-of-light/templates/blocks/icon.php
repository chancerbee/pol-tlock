<?php
/****** FLEX CONTENT: ICON BLOCK ******/
 $icon_display_type = get_sub_field('icon_display_type') ?? null;

  $icon_count = count(get_sub_field('icons'));

  $icon_class = '';
  if($icon_count < 6 )
    $icon_class = 'single-row';

  $icon_leftover = $icon_count % 6;


if($icon_display_type == 'grid'):
  //Grid View
 if(have_rows('icons')):
?>
<div class="grid-icon-wrapper">
  <a class="grid-icon-prev-arrow"><i class="icon-arrow-left"></i></a>
  <ul class="<?= $icon_display_type ?>-icon <?= $icon_class ?> leftover-<?= $icon_leftover ?>">
    <?php while(have_rows('icons')): the_row();
      $icon = get_sub_field('icon') ?? null;
    ?>
    <li>
      <?= wp_get_attachment_image($icon, 'logo'); ?>
    </li>
   <?php endwhile; ?>
  </ul>
  <a class="grid-icon-next-arrow"><i class="icon-arrow-right"></i></a>
</div>
 <?php endif; ?>

 <?php elseif($icon_display_type == 'slider'):

  $icon_display_type = 'slick slider-icon';
  //Slider View
  if(have_rows('icons')):
?>
<div class="slider-icon-wrap">
  <a class="slider-icon-prev-arrow"><i class="icon-arrow-left"></i></a>
  <ul class="<?= $icon_display_type ?> ">
    <?php while(have_rows('icons')): the_row();
      $icon = get_sub_field('icon') ?? null;
      $label = get_sub_field('label') ?? null;
    ?>
    <li>
      <?= wp_get_attachment_image($icon, 'logo'); ?>
      <?= !empty($label) ? '<span class="label">'.$label.'</span>' : '' ?>
    </li>
  <?php endwhile; ?>
  </ul>
  <a class="slider-icon-next-arrow"><i class="icon-arrow-right"></i></a>
</div>
 <?php endif; ?>
<?php endif; ?>
