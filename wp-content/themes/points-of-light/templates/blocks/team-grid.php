<?php
/****** FLEX CONTENT: TEAM GRID ******/
  if(have_rows('members')):
?>
<ul class="team-member-grid">
  <?php while(have_rows('members')): the_row();
    $member_object = get_sub_field('member');

    $member_id = $member_object->ID;
    $member_photo = get_field('person_portrait', $member_id);
    $member_link = get_permalink($member_id);
    $member_name = get_the_title($member_id);
    $member_pos = get_field('job_title', $member_id)
  ?>
    <li class="team-member-card">
      <a href="<?= $member_link ?>">
        <div class="zoom-wrap">
          <div class="bg-wrap" style="background-image:url('<?= wp_get_attachment_image_url($member_photo,'team-grid'); ?>');"></div>
        </div>
        <p class="name"><?= $member_name ?></p>
        <span class="title"><?= $member_pos ?></span>
      </a>
    </li>
  <?php endwhile; ?>
</ul>
<?php endif; ?>
