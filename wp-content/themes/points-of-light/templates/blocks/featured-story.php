<?php /****** FLEX CONTENT: FEATURED STORY BLOCK ******/ ?>

<?php $format = get_sub_field('featured_story_format'); ?>

<?php if($format == 'single'):
  $logo = get_sub_field('featured_story_logo');
  $prehead = get_sub_field('featured_story_preheadline');
  $headline = get_sub_field('featured_story_headline');
  $copy = get_sub_field('featured_story_copy');
  $bg = get_sub_field('featured_story_background_image');
  $overlay = get_sub_field('featured_story_overlay');
  $bg_color = null;

    if(empty($bg))
      $bg_color = 'bg-'.$overlay;

?>
<div class="featured-story single-story  break-container " >
  <div class="cover overlay-<?= $overlay ?> <?= $bg_color ? $bg_color : '' ?>" <?= $bg ? 'style="background: url('.wp_get_attachment_url($bg, 'full-container').') center/cover no-repeat;"' : '' ?>></div>
  <div class="pseudo-container">
    <div class="text">
      <?= $logo ? wp_get_attachment_image($logo, 'logo') : '' ?>
      <?= $prehead ? '<span class="prehead">'.$prehead.'</span>' : '' ?>
      <h2><?= $headline ?></h2>
      <?= $copy ? '<p>'.$copy.'</p>' : '' ?>
      <?php print_buttons('featured_story_cta_'); ?>
    </div>
  </div>
</div>

<?php elseif($format == 'multiple') :
  if(have_rows('featured_stories')):
?>
<div class="featured-stories stories-slider break-container">
  <?php while(have_rows('featured_stories')): the_row();
    $logo = get_sub_field('featured_story_logo');
    $prehead = get_sub_field('featured_story_preheadline');
    $headline = get_sub_field('featured_story_headline');
    $copy = get_sub_field('featured_story_copy');
    $bg = get_sub_field('featured_story_background_image');
    $overlay = get_sub_field('featured_story_overlay');
    $bg_color = null;

    if(empty($bg))
      $bg_color = 'bg-'.$overlay;
  ?>
  <div class="featured-story story-slide break-container">
    <div class="cover overlay-<?= $overlay ?> <?= $bg_color ? $bg_color : '' ?>" <?= $bg ? 'style="background: url('.wp_get_attachment_url($bg, 'full-container').') center/cover no-repeat;"' : '' ?>></div>
    <div class="pseudo-container">
      <div class="text">
        <?= $logo ? wp_get_attachment_image($logo, 'logo') : '' ?>
        <?= $prehead ? '<span class="prehead">'.$prehead.'</span>' : '' ?>
        <h2><?= $headline ?></h2>
        <?= $copy ? '<p>'.$copy.'</p>' : '' ?>

      </div>
    </div>
  </div>
  <?php endwhile; ?>
</div>
<div class="story-dots"></div>
  <?php endif; ?>


<?php endif; ?>
