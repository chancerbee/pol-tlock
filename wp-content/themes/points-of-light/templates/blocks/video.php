<?php /****** FLEX CONTENT: VIDEO BLOCK ******/ ?>

<?php
  $video_url = get_sub_field('video_url');
  $video_img = get_sub_field('video_image');
  $video_title = get_sub_field('video_title');
?>
<div class="video-wrapper embed-container">
  <?= $video_url ?>
  <div class="overlay" style="background: url(<?= wp_get_attachment_image_url($video_img, 'two-third-container')?>) center/cover no-repeat">
    <div class="cover"></div>
    <a class="play-button">
      <?php include_once dirname(__FILE__).'/../../assets/img/play.svg'; ?>
      <?= $video_title ? '<span class="video-title">'.$video_title.'</span>' : '' ?>
    </a>
  </div>
</div>
