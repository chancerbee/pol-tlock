<?php
/****** SINGLE RESOURCE ******/

  $gated = get_field('gate_resource');
  $resource_type = get_field('resource_type');
  $resource_download = get_field('resource_download');
  $subheadline = get_field('resource_subheadline');
  $main_content = get_field('main_resource_content');
  $resource_image = get_field('resource_image');
  $video_title = get_field('resource_video_title');
  $video_url = get_field('resource_video', false, false);

  $tax_name = get_term($resource_type)->name ?? null;
  $tax_slug = get_term($resource_type)->slug ?? null;

  $type_link = '';
  if ( !empty($tax_name) && !empty($gated) )
    $type_link = '<a href="'.get_permalink(969).'?resource-type='.$tax_slug.'">Gated '.$tax_name.'s</a>';
  if ( !empty($tax_name) && empty ($gated) )
    $type_link = '<a href="'.get_permalink(965).'?resource-type='.$tax_slug.'">'.$tax_name.'s</a>';

  $prev_link = '';
  if ( !empty($gated) )
    $prev_link = get_adjacent_resource_link('gated', 'previous');
  if ( empty($gated) )
    $prev_link = get_adjacent_resource_link('ungated', 'previous');

  $next_link = '';
  if ( !empty($gated) )
    $next_link = get_adjacent_resource_link('gated', 'next');
  if ( empty($gated) )
    $next_link = get_adjacent_resource_link('ungated', 'next');

?>
<section class="content">
  <div class="container">

    <nav class="single-nav">
      <?= $prev_link ?>
      <?= $type_link ?>
      <?= $next_link ?>
    </nav>

    <header class="single-header">
      <h2><?= get_the_title() ?></h2>
      <?= !empty($subheadline) ? '<p class="subhead">'.$subheadline.'</p>' : '' ?>

      <div class="share-links">
        <a href="https://www.facebook.com/sharer?u=<?php the_permalink();?>;&amp;t=<?php the_title(); ?>;" target="_blank" rel="noopener">
          <i class="icon-facebook"></i>
        </a>
        <a title="Click to share this post on Twitter" href="http://twitter.com/intent/tweet?text=Currently reading <?php the_title(); ?>&amp;url=<?php the_permalink(); ?>" target="_blank" rel="noopener">
          <i class="icon-twitter"></i>
        </a>
        <a href="http://www.linkedin.com/shareArticle?mini=true&url=<?php the_permalink() ?>&title=<?php the_title(); ?>&summary=&amp;source=<?php bloginfo('name'); ?>" target="_new">
          <i class="icon-linkedin"></i>
        </a>
        <?php if($resource_type != 10): ?>
          <a href="<?= $resource_download ?>" download>
            <i class="icon-download"></i>
          </a>
        <?php endif; ?>
      </div>

    </header>

    <main class="main">
      <div class="wysiwyg">
        <?= $main_content ?>
      </div>

      <?php if($resource_type != 10): ?>

      <a href="<?= $resource_download ?>" class="button green" download>
        Download The <?= $tax_name ?> PDF
      </a>
      <div class="image-wrap">
        <?= wp_get_attachment_image($resource_image, 'full-container') ?>
      </div>

      <?php
        elseif($resource_type == 10) :
          $video_url = get_field('resource_video');
          $video_img = get_field('resource_video_image');
          $video_title = get_field('resource_video_title');
      ?>

      <div class="video-wrapper embed-container">
        <?= $video_url ?>

        <?php if($video_img):?>

        <div class="overlay" style="background: url(<?= wp_get_attachment_image_url($video_img, 'two-third-container')?>) center/cover no-repeat">
          <div class="cover"></div>
          <a class="play-button">
            <?php include_once dirname(__FILE__).'/../../assets/img/play.svg'; ?>
            <?= $video_title ? '<span class="video-title">'.$video_title.'</span>' : '' ?>
          </a>
        </div>

        <?php endif; ?>

      </div>

      <?php endif; ?>

    </main>
  </div>
</section>

<?php /*TODO: related posts*/ ?>
