<?php
/****** SINGLE PRESS RELEASE ******/

  $headline = get_the_title();
  $subheadline = get_field('subheadline');
  $date = get_field('date');
  $city = get_field('city');
  $content = get_field('content');
  $feature_image = get_the_post_thumbnail( get_the_id(), 'full-container' );
  $feature_text = get_field('feature_text');
  $downloadable = get_field('include_download');

  if ( empty($date) ) $date = get_the_date('M j, Y');
  if ( !empty($downloadable) ) $file = get_field('file');

  $prev_link = get_previous_post_link('%link', '<i class="icon-arrow-left"></i> Previous');
  $index_link = '<a href="'.get_permalink(513).'">Newsroom</a>';
  $next_link = get_next_post_link('%link', 'Next <i class="icon-arrow-right"></i>');

?>

<!-- MAIN SECTION -->
<section class="content">
  <div class="container">

    <nav class="single-nav">
      <?= str_replace( 'href=', 'class="previous" href=', $prev_link ) ?>
      <?= $index_link ?>
      <?= str_replace('href=', 'class="next" href=', $next_link ) ?>
    </nav>

    <header class="single-header">

      <h2><?= $headline ?></h2>
      <?= !empty($subheadline) ? '<p class="subhead">'.$subheadline.'</p>' : '' ?>

      <div class="meta">
        <?= $city ? '<span class="city">'.$city.'</span><span class="pipe">|</span>' : '' ?>
        <?= '<span class="date">'.$date.'</span>' ?>
      </div>

      <div class="share-links">
        <a href="https://www.facebook.com/sharer?u=<?php the_permalink();?>;&amp;t=<?php the_title(); ?>;" target="_blank" rel="noopener">
          <i class="icon-facebook"></i>
        </a>
        <a title="Click to share this post on Twitter" href="http://twitter.com/intent/tweet?text=Currently reading <?php the_title(); ?>&amp;url=<?php the_permalink(); ?>" target="_blank" rel="noopener">
          <i class="icon-twitter"></i>
        </a>
        <a href="http://www.linkedin.com/shareArticle?mini=true&url=<?php the_permalink() ?>&title=<?php the_title(); ?>&summary=&amp;source=<?php bloginfo('name'); ?>" target="_new">
          <i class="icon-linkedin"></i>
        </a>
        <?php if( !empty($downloadable) ): ?>
          <a href="<?= $file ?>" download>
            <i class="icon-download"></i>
          </a>
        <?php endif; ?>
      </div>

    </header>

    <main class="main">

      <?php if ( !empty($feature_image) ): ?>

      <div class="featured-image">
        <?= $feature_image ?>
      </div>

      <?php endif; ?>

      <div class="wysiwyg">
          <?= $content ?>
      </div>

      <?php if ( !empty($feature_text) ): ?>

      <div class="hidden">
        <p class="feature-text"><?= $feature_text ?></p>
      </div>

      <?php endif; ?>

    </main>

  </div>
</section>

<!-- FORM SECTION -->
<section class="module module-type-gravity-form">
  <div class="container">
    <div class="left-col">
      <div class="cover dark" style="opacity: .25"></div>
      <div class="text">
        <h2 class="form-headline"><?= get_field('PR_form_headline', 'cpt_press_release') ?></h2>
        <p class="intro-text"><?= get_field('PR_form_text', 'cpt_press_release') ?></p>
      </div>
    </div>
    <div class="right-col">

      <?php
        gravity_form(
          $id_or_title = 9,
          $display_title = false,
          $display_description = false,
          $display_inactive = false,
          $field_values = null,
          $ajax = true,
          $tabindex = 1,
          $echo = true
        );
      ?>

    </div>
  </div>
</section>
