<?php
/****** SINGLE POST ******/
?>

<?php
	$pid = get_the_ID();
	$headline = get_the_title();
	$subheadline = get_field('post_subhead');
	$date = get_the_date('M j, Y');
	$author = get_the_author_meta('display_name');
  $feature_image = get_the_post_thumbnail($pid, 'bg');
  $dpol = in_category(5);
  if ( !empty($dpol) )
    $category = get_category(5);
  else
    $category = get_the_category()[0];
  $category_slug = $category->slug;
  $category_name = $category->name;
  if ( !empty($category_slug) ) {
    $index_link = '<a href="'.get_permalink(124).'?category='.$category_slug.'">'.$category_name.'</a>';
  } else {
    $index_link = '<a href="'.get_permalink(124).'">Blog</a>';
  }
  $prev_link = get_previous_post_link('%link', '<i class="icon-arrow-left"></i> Older');
  $next_link = get_next_post_link('%link', 'Newer <i class="icon-arrow-right"></i>');
?>

<section class="content single-post-content">
  <div class="container">

    <nav class="single-nav">
      <?= str_replace( 'href=', 'class="previous" href=', $prev_link ) ?>
      <?= $index_link ?>
      <?= str_replace('href=', 'class="next" href=', $next_link ) ?>
    </nav>

    <header class="single-header">

      <h2><?= $headline ?></h2>
      <?= !empty($subheadline) ? '<p class="subhead">'.$subheadline.'</p>' : '' ?>

      <div class="meta">
				<?= '<span class="date">'.$date.'</span>' ?>
				&nbsp;&nbsp;<span class="byline">By <span class="author"><?= $author ?></span>
      </div>

      <div class="share-links">
        <a href="https://www.facebook.com/sharer?u=<?php the_permalink();?>;&amp;t=<?php the_title(); ?>;" target="_blank" rel="noopener">
          <i class="icon-facebook"></i>
        </a>
        <a title="Click to share this post on Twitter" href="http://twitter.com/intent/tweet?text=Currently reading <?php the_title(); ?>&amp;url=<?php the_permalink(); ?>" target="_blank" rel="noopener">
          <i class="icon-twitter"></i>
        </a>
        <a href="http://www.linkedin.com/shareArticle?mini=true&url=<?php the_permalink() ?>&title=<?php the_title(); ?>&summary=&amp;source=<?php bloginfo('name'); ?>" target="_new">
          <i class="icon-linkedin"></i>
        </a>
      </div>

    </header>

    <main class="main">

      <?php if ( !empty($feature_image) && $pid > 4268 ): ?>

      <div class="featured-image">
				<?= $feature_image ?>
      </div>
			<?php //TODO: image caption ?>

      <?php endif; ?>

      <div class="wysiwyg">
        <?php the_content(); ?>
      </div>

			<h6 class="share-prompt">Share this post</h6>
      <div class="share-links footer-share-links">
        <a href="https://www.facebook.com/sharer?u=<?php the_permalink();?>;&amp;t=<?php the_title(); ?>;" target="_blank" rel="noopener">
          <i class="icon-facebook"></i>
        </a>
        <a title="Click to share this post on Twitter" href="http://twitter.com/intent/tweet?text=Currently reading <?php the_title(); ?>&amp;url=<?php the_permalink(); ?>" target="_blank" rel="noopener">
          <i class="icon-twitter"></i>
        </a>
        <a href="http://www.linkedin.com/shareArticle?mini=true&url=<?php the_permalink() ?>&title=<?php the_title(); ?>&summary=&amp;source=<?php bloginfo('name'); ?>" target="_new">
          <i class="icon-linkedin"></i>
        </a>
      </div>

		</main>

		<div class="hidden">
			<div class="blog-topics">
        <h3>Topics</h3>
        <div class="link-list">
          <?php
            $category_list_links = get_the_category_list();
            // remove trailing slashes at end of href="...../"
            $category_list_links = str_replace('/"', '"', $category_list_links);
            // replace dir tree with query format
            $category_list_links = str_replace('/category/', '?category=', $category_list_links);
            echo $category_list_links;
          ?>
        </div>
        <a href="<?= get_permalink(2206) ?>" class="button">Nominate a daily<br>point of light</a>
			</div>
		</div>
  </div>
</section>

<?php // YARPP
  if ( function_exists('related_posts') ) related_posts();
?>
