<?php
  /*Single Person Template*/
  $person_role = get_field('person_role');
  $person_title = get_field('job_title');
  $person_company = get_field('person_company');
  $person_bio = get_field('person_bio');
  $person_portrait = get_field('person_portrait');

  $person_fb = get_field('person_fb');
  $person_twitter = get_field('person_twitter');
  $person_linkedin = get_field('person_linkedin');

  $prev_link = get_previous_post_link('%link', '<i class="icon-arrow-left"></i> Previous', true, ' ', 'role');
  $index_link = '<a href="'.get_permalink(512).'">Leadership</a>';
  $next_link = get_next_post_link('%link', 'Next <i class="icon-arrow-right"></i>', true, ' ', 'role');

?>


<section class="content main-person-content">
  <div class="container">

    <nav class="single-nav">
      <?= str_replace( 'href=', 'class="previous" href=', $prev_link ) ?>
      <?= $index_link ?>
      <?= str_replace('href=', 'class="next" href=', $next_link ) ?>
    </nav>

    <header class="person-header">
      <h2><?= get_the_title(); ?></h2>
      <?= $person_title ? '<span class="job-title">'.$person_title.'</span>' : ''; ?>
      <?= $person_company ? '<span class="company">'.$person_company.'</span>' : '' ?>
      <div class="social-links">
        <?= !empty($person_fb) ? '<a href="'.$person_fb.'"><i class="icon-facebook"></i></a>' : '' ?>
        <?= !empty($person_twitter) ? '<a href="'.$person_twitter.'"><i class="icon-twitter"></i></a>' : '' ?>
        <?= !empty($person_linkedin) ? '<a href="'.$person_linkedin.'"><i class="icon-linkedin"></i></a>' : '' ?>
      </div>
    </header>
    <div class="main">
      <div class="wysiwyg"><?= $person_bio ?></div>
      <?= wp_get_attachment_image($person_portrait, 'single-team','',["class"=>"portrait"]); ?>
    </div>
  </div>
</section>

<?php
  $cpt_options = 'cpt_people';

  $partner_link = get_field('partner_link', $cpt_options);
  $partner_text = get_field('partner_text', $cpt_options);
  $partner_bg = get_field('partner_background_image', $cpt_options);

  $volunteer_link = get_field('volunteer_link', $cpt_options);
  $volunteer_text = get_field('volunteer_text', $cpt_options);
  $volunteer_bg = get_field('volunteer_background_image', $cpt_options);

  $donation_link = get_field('donation_link', $cpt_options);
  $donation_text = get_field('donation_text', $cpt_options);
  $donation_bg = get_field('donation_background_image', $cpt_options);

?>

<section class="content module get-involved">
  <div class="container">
    <header class="module-header">
      <h2 class="module-heading">Get Involved &amp; Light the Way</h2>
    </header>
    <div class="involved-links">
      <a href="<?= $partner_link ?>" class="involved-link">
        <div class="cover bg-image" ><?= wp_get_attachment_image($partner_bg, 'one-third-container') ?></div>
        <div class="text">
          <h3><?= $partner_text ?></h3>
          <span class="learn-more">Learn More <i class="icon-arrow-right"></i></span>
        </div>
      </a>
      <a href="<?= $volunteer_link ?>" class="involved-link">
        <div class="cover bg-image"><?= wp_get_attachment_image($volunteer_bg, 'one-third-container') ?></div>
        <div class="text">
          <h3><?= $volunteer_text ?></h3>
          <span class="learn-more">Learn More <i class="icon-arrow-right"></i></span>
        </div>
      </a>
      <a href="<?= $donation_link ?>" class="involved-link">
        <div class="cover bg-image"><?= wp_get_attachment_image($donation_bg, 'one-third-container') ?></div>
        <div class="text">
          <h3><?= $donation_text ?></h3>
          <span class="learn-more">Learn More <i class="icon-arrow-right"></i></span>
        </div>
      </a>
    </div>
  </div>
</section>
