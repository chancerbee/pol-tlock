<?php
/****** PAGE HEADER ******/

  if ( is_front_page() ) {
    include_once(realpath(dirname(__FILE__).'/home-slider.php'));
    return;
  }

  if ( is_singular() && !is_singular('page') ){
    include_once(realpath(dirname(__FILE__).'/single-header.php'));
    return;
  }

  if ( is_search() ) {
    include_once(realpath(dirname(__FILE__).'/single-header.php'));
    return;
  }


  $id = get_the_id();
  $headline = get_field('heading');
  $image = wp_get_attachment_image_url( get_field( 'image' ), 'bg' );
  $subhead = get_field('subhead');
  $color = get_field('color');
  $opacity = get_field('opacity');

  if ( is_404() ) {
    $headline = 'Ooops, there doesn\'t seem to be anything here!';
    $image = wp_get_attachment_image_url( get_field( 'image', 10414 ), 'bg' );
    $subhead = 'Sending you back to the home page in 3, 2, 1...';
    $color = get_field('color');
    $opacity = get_field('opacity');
  }

  // send demo pages that never got header config to the simple header template
  if ( empty($headline) ) {
    $headline = get_the_title();
    include_once(realpath(dirname(__FILE__).'/single-header.php'));
    return;
  }

?>

<header class="page-header full-page-header">

  <div class="cover image" style="background-image:url('<?= $image ?>')"></div>

  <div class="container">
    <div class="text">
      <h1 class="headline"><?= $headline ?></h1>
      <p class="subhead"><?= $subhead ?></p>
    </div>
  </div>

  <figure
    class="highlight-bar bg-<?= $color ?>"
    style="opacity: .<?= $opacity ?>">
  </figure>
  <figure class="static-bar left"></figure>
  <figure class="static-bar right"></figure>
  <figure class="text-bg"></figure>


</header>

<?= get_option('page_'.$id.'_anchor_nav') ?>
