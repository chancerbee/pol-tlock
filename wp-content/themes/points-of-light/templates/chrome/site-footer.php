<?php
  /****** SITE FOOTER ******/
?>


<footer class="site-footer">

  <!-- Quote -->
  <?php

    $default_footer_quote_id = 1856;
    $footer_quote_id = get_field('cta_footer_quote_id');
    if(empty($footer_quote_id))
      $footer_quote_id = $default_footer_quote_id;

    $footer_quote_content = get_field('cta_footer_quote', $footer_quote_id);
    //strip p tags and leave color spans
    $footer_quote_content = str_ireplace('<p>', '', $footer_quote_content);
    $footer_quote_content = str_ireplace('</p>', '', $footer_quote_content);
    //add straight quotation to end of quote
    //big curly quote added in css
    $footer_quote_content = $footer_quote_content.'"';
    $footer_quote_cite = get_field('cta_footer_citation', $footer_quote_id);
    $footer_quote_img = get_field('cta_footer_image', $footer_quote_id);

  ?>
  <section class="footer-quote">
    <div class="container">
      <blockquote class="blockquote">
        <?= $footer_quote_content ?>
        <?= $footer_quote_cite ? '<cite>— '.$footer_quote_cite.'</cite>' : '' ?>
      </blockquote>

      <div class="image">
        <?= wp_get_attachment_image($footer_quote_img, 'footer-quote') ?>
      </div>
      <div class="rhombi"></div>
    </div>
  </section>

  <!-- Site Footer -->
  <?php
    $address = get_field('address', 'option');
    $email = get_field('email', 'option');
    $use_social = get_field('include_social_links', 'option');

    if ( !empty($use_social) ) {
      $social_links = '';
      $social_opts = array('facebook','twitter','linkedin','youtube','instagram','gplus','pinterest');
      foreach ($social_opts as $v)
      if ($url = get_field('url_'.$v, 'option'))
      $social_links .= '<li><a href="'.$url.'" target="_blank"><i class="icon-'.$v.'"></i></a></li>';
      if ($social_links != '')
      $social_links = '<ul class="social-links">'.$social_links.'</ul>';
    }
  ?>

  <main class="footer-main">
    <div class="container">

      <div class="left">
        <!-- LOGO -->
        <a class="footer-logo" href="<?= esc_url( home_url( '/' ) ); ?>">
          <?= file_get_contents(get_template_directory_uri() . '/assets/img/POL-logo_stacked_white.svg'); ?>
        </a>

        <!-- Contact -->
        <?= !empty($address) ? '<p class="street-address">'.$address.'</p>' : '' ?>
        <?= !empty($email) ? '<a href="mailto:'.$email.'">'.$email.'</a>' : '' ?>
      </div>

      <div class="center">
        <!-- Social -->
        <h6>Follow Us</h6>
        <?= !empty($use_social) ? $social_links : '' ?>

        <!-- Menu -->
        <nav class="footer-nav">
          <?php wp_nav_menu( array( 'menu' => 'Footer Navigation', 'menu_class' => 'menu js-nice-cols' ) ); ?>
        </nav>
      </div>

      <div class="right">
        <!-- Subscribe -->
        <h6>Subscribe to Email Alerts</h6>
        <div class="mailchimp-subscribe">
          <?php gravity_form( 10, false, false, false, '', true ); ?>
        </div>

        <!-- Donate & Search -->
        <?php
          $donation_nav_link = get_field('global_donation_link', 'option');
        ?>
        <a href="<?= $donation_nav_link ?>" class="button tight site-footer-donate-button">Donate</a>
        <form role="search" method="get" action="<?= get_site_url() ?>" class="site-footer-search-form">
          <input class="search-input" type="search" name="s" placeholder="Search" value="">
          <button>
            <i class="icon-search"></i>
          </button>
        </form>

        <!-- Copyright -->
        <p class="copyright">&copy;<?= date("Y") ?> Points of Light. All rights reserved.</p>
      </div>




    </div>
  </main>

  <?php include_once('mobile-footer.php'); ?>

</footer>
