<!-- Site Footer -->
  <?php
    $address = get_field('address', 'option');
    $email = get_field('email', 'option');
    $use_social = get_field('include_social_links', 'option');

    if ( !empty($use_social) ) {
      $social_links = '';
      $social_opts = array('facebook','twitter','linkedin','youtube','instagram','gplus','pinterest');
      foreach ($social_opts as $v)
      if ($url = get_field('url_'.$v, 'option'))
      $social_links .= '<li><a href="'.$url.'" target="_blank"><i class="icon-'.$v.'"></i></a></li>';
      if ($social_links != '')
      $social_links = '<ul class="social-links">'.$social_links.'</ul>';
    }
  ?>

  <main class="footer-main footer-mobile">
    <div class="container">

        <!-- Donate & Search -->
        <form role="search" method="get" action="<?= get_site_url() ?>" class="site-footer-search-form">
          <input class="search-input" type="search" name="s" placeholder="Search" value="">
          <button>
            <i class="icon-search"></i>
          </button>
        </form>
        <a href="#" class="button tight site-footer-donate-button">Donate</a>

        <!-- Menu -->
        <nav class="footer-nav">
          <?php wp_nav_menu( array( 'menu' => 'Footer Navigation', 'menu_class' => 'menu js-nice-cols' ) ); ?>
        </nav>

        <!-- Subscribe -->
        <div class="mailchimp-subscribe">
          <?php gravity_form( 10, false, false, false, '', true ); ?>
        </div>

        <div class="cols cols-2">
          <div class="col col-left">
            <!-- LOGO -->
            <a class="footer-logo" href="<?= esc_url( home_url( '/' ) ); ?>">
              <?= file_get_contents(get_template_directory_uri() . '/assets/img/POL-logo_stacked_white.svg'); ?>
            </a>
          </div>
          <div class="col col-right">
            <!-- Contact -->
            <?= !empty($address) ? '<p class="street-address">'.$address.'</p>' : '' ?>
            <?= !empty($email) ? '<a href="mailto:'.$email.'">'.$email.'</a>' : '' ?>
          </div>
        </div>

        <!-- Social -->
        <?= !empty($use_social) ? $social_links : '' ?>
        <!-- Copyright -->
        <p class="copyright">&copy;<?= date("Y") ?> Points of Light. All rights reserved.</p>





    </div>
  </main>
