  <header class="site-header">

    <!-- NOTICE -->
    <div class="notice bg-green">
      <?php if ( !empty( $_GET['logout'] ) && ( $_GET['logout'] == 'true' ) ): ?>
        <p style="text-align:center">You were successfully logged out</p>
      <?php endif; ?>
    </div>

    <div class="container">

      <!-- LOGO -->
      <a class="site-logo" href="<?= esc_url( home_url( '/' ) ); ?>">
        <?php
          $logo = get_field('site_logo', 'option');
          if ($logo)
            echo wp_get_attachment_image($logo, 'logo');
          else
            bloginfo('name');
        ?>
      </a>

      <div class="navigation">

        <!-- Auxiliary Nav -->
        <nav class="auxiliary-nav">
          <?php wp_nav_menu( array( 'menu' => 'Auxiliary Navigation' ) ); ?>
        </nav>

        <!-- Main Nav -->
        <nav class="main-nav">

          <?php include_once('mega-menu.php'); ?>

          <?php
            $donation_nav_link = get_field('global_donation_link', 'option');
          ?>
          <a href="<?= $donation_nav_link ?>" class="button tight site-header-donate-button">Donate</a>

          <form role="search" method="get" action="<?= get_site_url() ?>" class="site-search-form">
            <input class="search-input" type="search" id="s" name="s" placeholder="Search" value="">
            <button>
              <i class="icon-search"></i>
            </button>
          </form>

        </nav>

        <!-- Mobile Menu Toggle -->
        <a class="mobile-menu-toggle">
          <i class="icon-hamburger"></i>
          <i class="icon-cross"></i>
        </a>

      </div>

      <!-- Mobile Menu -->
      <?php include_once('mobile-menu.php'); ?>

    </div>
  </header>
