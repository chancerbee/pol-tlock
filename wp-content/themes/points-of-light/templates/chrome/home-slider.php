<?php
/****** HOMEPAGE SLIDER ******/
?>

<header class="page-header home-page-header">
  <div class="home-slider">

    <?php while ( have_rows( 'slides' ) ): the_row();
      $headline = get_sub_field( 'heading' );
      $image = wp_get_attachment_image_url( get_sub_field( 'image' ), 'bg' );
      $text = get_sub_field('text');
      $button = get_sub_field('button');
      $color = get_sub_field('color');
      $opacity = get_sub_field('opacity');
    ?>

      <div class="slide">
        <div class="cover image" style="background-image:url('<?= $image ?>')"></div>
        <div class="container">
          <div class="slide-text">
            <h1 class="slide-headline"><?= $headline ?></h1>
            <div class="slide-details">
              <p class="slide-paragraph"><?= $text ?></p>
              <a href="<?= $button['url'] ?>" target="<?= $button['target'] ?>" class="button navy">
                <?= $button['title'] ?>
              </a>
            </div>
          </div>
        </div>

        <figure
          class="highlight-bar bg-<?= $color ?>"
          style="opacity: .<?= $opacity ?>">
        </figure>

        <figure class="static-bar left"></figure>
        <figure class="static-bar right"></figure>
        <figure class="text-bg"></figure>
      </div>

    <?php endwhile; ?>

  </div>
  <div class="container header-dots"></div>
</header>
