<?php
/****** SINGLE HEADER ******/
  $bg_color = 'green';

  // demo pages without header config can be passed here, this will take care of them
  if ( is_singular('page') )
    $heading = get_the_title();

  if ( is_singular('post') ){
    $heading = 'Blog';
  }

  if ( is_singular('people') ){
    $people_tax_name = get_term(get_field('person_role'))->name;
    $heading = 'People';
    if( !empty($people_tax_name) )
      $heading = $people_tax_name;
  }

  if ( is_singular('resource') ){
    $heading = 'Resource';
    $gated = get_field('gate_resource');
    if ( $gated )
      $heading = 'Gated Resource';
  }

  if ( is_singular('press_release') ){
    $heading = 'Press';
    $bg_color = 'blue';
  }

  if ( is_search() ) {
    $query = get_search_query();
    global $wp_query;
    $heading = 'Search Results For: "'.$query.'" <span class="counter">('.$wp_query->found_posts.')</span>';
  }
?>

<!-- PAGE HEADER -->
<header class="page-header header-single bg-<?= $bg_color ?>">
  <div class="container">
    <div class="header-single-text">
    	<h2><?= $heading ?></h2>
    </div>
  </div>
</header>
