<?php
  /****** MEGA MENU ******/
  global $menu_items;
?>

<ul class="top-level-list">

  <?php foreach( $menu_items as $menu_item ): ?>

  <li class="top-level-item">
    <a class="top-level-link has-<?= $menu_item['type'] ?>" href="<?= $menu_item['url'] ?>">
      <?= $menu_item['title'] ?>
    </a>

    <?php if ( $menu_item['type'] == 'dropdown'): ?>

    <div class="mega-menu-dropdown">
      <div class="container">
        <div class="left left-type-<?= $menu_item['dropdown']['left_type']?>">

        <?php switch ( $menu_item['dropdown']['left_type'] ):

          // TEXT LINK BLOCKS
          case "text":

            foreach ( $menu_item['dropdown']['text_blocks'] as $block ):
              $title = $block['text_block_title'];
              $text = $block['text_block_text'];
              $link = $block['text_block_link'] ?? null;
              $quicklinks = $block['quick_links'] ?? null;
              if ( !empty( $block['quick_links_title'] ) )
                $quicklinks_title = $block['quick_links_title'];
              else
                $quicklinks_title = 'Quicklinks';
            ?>

            <div class="text-link-block">
              <strong class="title"><?= $title ?></strong>
              <p class="text"><?= $text ?></p>

              <?= !empty($link) ? '<a class="learn-more" href="'.$link.'">Learn More</a>' : '' ?>

              <?php if ( !empty($quicklinks) ): ?>

              <strong class="title quicklinks-title"><?= $quicklinks_title ?></strong>

              <?php foreach ( $quicklinks as $k => $v): $quicklink = $v['quick_link']; ?>

              <a class="quicklink" href="<?= $quicklink['url'] ?>" target="<?= $quicklink['target'] ?>">
                <?= $quicklink['title']?>
              </a>

              <?php
                endforeach;
                endif;
              ?>

            </div>

            <?php endforeach ?>

          <?php break;

          // IMAGE LINK BLOCKS
          case "image":

            foreach ( $menu_item['dropdown']['image_blocks'] as $block ):
              $image = wp_get_attachment_image( $block['image'], 'megamenu-link' );
              $link = $block['image_link'];
            ?>

            <a class="image-link-block" href="<?= $link['url'] ?>" target="<?= $link['target'] ?>">
              <?= $image ?>
              <strong class="title"><?= $link['title'] ?></strong>
            </a>

            <?php endforeach; ?>

          <?php break;

          // ICON LINK BLOCKS
          case "icon":

            foreach ( $menu_item['dropdown']['icon_blocks'] as $block ):
              $title = $block['icon_block_title'];
              $iconlinks = $block['icon_links'];
            ?>

            <div class="icon-link-block">
              <strong class="title"><?= $title ?></strong>

              <?php foreach ( $iconlinks as $icon_link): ?>

              <a class="icon-link" href="<?= $icon_link['icon_link']['url'] ?>" target="<?= $icon_link['icon_link']['target'] ?>">
                <i class="<?= $icon_link['icon'] ?>"></i>
                <span><?= $icon_link['icon_link']['title']?></span>
              </a>

              <?php endforeach; ?>

            </div>

            <?php endforeach ?>

        <?php
          break;
          endswitch;
        ?>

        </div>
        <div class="right right-type-<?= $menu_item['dropdown']['right_type']?>">

        <?php switch ( $menu_item['dropdown']['right_type'] ):

          // TEXT AND LINKS
          case "text_links":

            $headline = $menu_item['dropdown']['right_headline'];
            $intro = $menu_item['dropdown']['right_text'];
            $links_headline = $menu_item['dropdown']['right_links_headline'];
            $links = $menu_item['dropdown']['right_links'];
          ?>

            <div class="top">
              <strong class="title"><?= $headline ?></strong>
              <p class="text"><?= $intro ?></p>
            </div>

            <div class="bottom">
              <strong class="title"><?= $links_headline ?></strong>

              <?php foreach ( $links as $k => $v): $quicklink = $v['right_link']; ?>

              <a class="quicklink" href="<?= $quicklink['url'] ?>" target="<?= $quicklink['target'] ?>">
                <?= $quicklink['title']?>
              </a>

              <?php endforeach; ?>
            </div>

          <?php break;

          // BLOG LINKS
          case "blog":

            $headline = $menu_item['dropdown']['right_headline'];
            $intro = $menu_item['dropdown']['right_text'];
            $image = wp_get_attachment_image($menu_item['dropdown']['right_image'], 'megamenu-blog') ?? null;
            $categories = $menu_item['dropdown']['category_links'];
          ?>

            <a href="<?= get_permalink(124) ?>">
              <strong class="title"><?= $headline ?></strong>
            </a>

            <p class="text"><?= $intro ?></p>

            <div class="blog-links">

              <?php if ( !empty($image) ): ?>

              <div class="image">
                <a href="<?= get_permalink(124) ?>"><?= $image ?></a>
              </div>

              <?php endif; ?>

              <div class="category-links">

                <?php if ( !empty($categories) ): ?>

                <strong class="title">Categories</strong>

                <?php foreach ( $categories as $category_id ):
                  $category_array = get_category($category_id);
                  $cat_slug = $category_array->slug;
                  $blog_id = 124;
                  $cat_link = get_permalink($blog_id). '?category='. $cat_slug;
                ?>

                <a class="quicklink" href="<?= $cat_link ?>">
                  <?= get_cat_name($category_id) ?>
                </a>

                <?php
                  endforeach;
                  endif;
                ?>

              </div>
            </div>

          <?php break;

          // EVENT LINKS
          case "events":

            $headline = $menu_item['dropdown']['right_headline'];
            $image = wp_get_attachment_image($menu_item['dropdown']['right_image'], 'megamenu-event') ?? null;
            $upcoming_events = $menu_item['dropdown']['upcoming_events'];
          ?>

            <div class="event-links">

              <?php if ( !empty( $image ) ): ?>

              <a class="image-link" href="<?= get_permalink(534) ?>">
                <?= $image ?>
                <strong class="title"><?= $headline ?></strong>
              </a>

              <?php endif; ?>
              <?php if(is_array($upcoming_events)): ?>
              <div class="events">
                <strong class="title">Upcoming Events</strong>
                <ul class="events-list">
                <?php foreach($upcoming_events as $upcoming_event):
                  $label = $upcoming_event['label'];
                  $link = $upcoming_event['link'];
                ?>
                  <li class="events-list-event">
                    <span class="label"><?= $label ?></span>
                    <?php print_link_array($link); ?>
                  </li>
                <?php endforeach; ?>
                </ul>
              </div>
              <?php endif; ?>
            </div>

          <?php break;

          // FEATURED RESOURCE
          case "resource":

            $fr = $menu_item['dropdown']['featured_resource'];
            $fr_id = $fr->ID;
            $fr_type = get_field('resource_type', $fr_id);
            $fr_title = $fr->post_title;
            $fr_url = get_permalink($fr_id);
            $fr_content = get_field('main_resource_content', $fr_id);
            $fr_button = $menu_item['dropdown']['resource_button'];
            if ( $fr_type == 10)
              $fr_image_field = 'resource_video_image';
            else
              $fr_image_field = 'resource_image';
            $fr_thumb = wp_get_attachment_image(get_field($fr_image_field, $fr_id), 'megamenu-resource');

            //not working properly from functions/misc.php, using inline... ¯\_(ツ)_/¯
            $fr_excerpt = strip_tags(strip_shortcodes($fr_content));
            $words = explode(' ', $fr_excerpt, 31);

            if(count($words) > 30) :
              array_pop($words);
              array_push($words, '…');
              $fr_excerpt = implode(' ', $words);
            endif;
          ?>

            <div class="featured-resource">

              <?php if ( !empty($fr_thumb) ): ?>

              <div class="image">
                <a href="<?= $fr_url ?>">
                  <?= $fr_thumb ?>
                </a>
              </div>

              <?php endif; ?>

              <div class="text">
                <small>Featured Resource</small>
                <a href="<?= $fr_url ?>">
                  <strong class="title"><?= $fr_title ?></strong>
                </a>
                <?= !empty($fr_excerpt) ? '<p class="text">'.$fr_excerpt.'</p>' : ''; ?>
                <a href="<?= $fr_url ?>" class="button">
                  <?= $fr_button ?>
                </a>
              </div>
            </div>

          <?php
            break;
          endswitch;
        ?>

        </div>
      </div>
    </div>

    <?php endif; ?>

  </li>

  <?php endforeach; ?>

</ul>
