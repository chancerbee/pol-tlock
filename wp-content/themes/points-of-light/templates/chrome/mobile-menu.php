<?php
  /****** MEGA MENU ******/
  global $menu_items;
?>

<div class="mobile-menu">
  <div class="container">

    <?php // Search ?>
    <form role="search" method="get" action="<?= get_site_url() ?>" class="site-search-form">
      <input class="search-input" type="search" name="s" placeholder="Search" value="">
      <button>
        <i class="icon-search"></i>
      </button>
    </form>

    <?php // Top Level Loop
      foreach( $menu_items as $menu_item ):
    ?>

      <?php if ( $menu_item['type'] == 'link'): ?>

      <a class="mobile-basic-link button max sharp darkblue" href="<?= $menu_item['url'] ?>">
        <?= $menu_item['title'] ?>
      </a>

      <?php elseif ( $menu_item['type'] == 'dropdown'): ?>

      <div class="dropdown">
        <a class="dropdown-button button max sharp darkblue" href="#">
          <span class="title"><?= $menu_item['title'] ?></span>
          <i class="icon-arrow-down"></i>
        </a>
        <div class="dropdown-items">

        <a href="<?= $menu_item['url'] ?>" class="mobile-main-link">Overview</a>

          <?php // 'Left Side' (top on mobile menu)
          switch ( $menu_item['dropdown']['left_type'] ):

          case "text":

            foreach ( $menu_item['dropdown']['text_blocks'] as $block ):
              $title = $block['text_block_title'];
              $link = $block['text_block_link'] ?? null;
              $quicklinks = $block['quick_links'] ?? null;
            ?>

              <?php if ( !empty($link) ): ?>

              <a class="mobile-main-link" href="<?= $link ?>"><?= $title ?></a>

              <?php else: ?>

              <span class="mobile-main-link"><?= $title ?></span>

              <?php endif; ?>

              <?php
                if ( !empty($quicklinks) ):
                  foreach ( $quicklinks as $k => $v): $quicklink = $v['quick_link'];
                ?>

                  <a class="mobile-additional-link" href="<?= $quicklink['url'] ?>" target="<?= $quicklink['target'] ?>">
                    <?= $quicklink['title']?>
                  </a>

                <?php
                  endforeach;
                endif;
              ?>

            <?php endforeach ?>

          <?php break;

          case "image":

            foreach ( $menu_item['dropdown']['image_blocks'] as $block ):
              $link = $block['image_link'];
            ?>

            <a class="mobile-main-link" href="<?= $link['url'] ?>" target="<?= $link['target'] ?>">
              <?= $link['title'] ?>
            </a>

            <?php endforeach; ?>

          <?php break;

          case "icon": ?>

            <div class="icon-link-blocks">

              <?php foreach ( $menu_item['dropdown']['icon_blocks'] as $block ):
                $title = $block['icon_block_title'];
                $iconlinks = $block['icon_links'];
              ?>

              <div class="icon-link-block">
                <span class="mobile-main-link"><?= $title ?></span>

                <?php foreach ( $iconlinks as $icon_link): ?>

                <a class="icon-link" href="<?= $icon_link['icon_link']['url'] ?>" target="<?= $icon_link['icon_link']['target'] ?>">
                  <i class="<?= $icon_link['icon'] ?>"></i>
                  <span><?= $icon_link['icon_link']['title']?></span>
                </a>

                <?php endforeach; ?>

              </div>

              <?php endforeach ?>

            </div>

          <?php
            break;
          endswitch;
        ?>


        <?php // 'Right Side' (bottom on mobile menu)
          switch ( $menu_item['dropdown']['right_type'] ):
            case "blog":
          ?>

            <a href="<?= get_permalink(124) ?>" class="mobile-main-link">Blog</a>

          <?php
            break;
            case "events":
              $upcoming_events = $menu_item['dropdown']['upcoming_events'] ?? null;
              if( !empty($upcoming_events) ):
            ?>

              <a href="<?= get_permalink(534) ?>" class="mobile-main-link">Upcoming Events</a>

              <?php foreach($upcoming_events as $upcoming_event):
                $label = $upcoming_event['label'];
                $link = $upcoming_event['link'];
              ?>

              <span class="mobile-event-link-label"><?= $label ?></span>
              <?php print_link_array($link, 'mobile-additional-link'); ?>

              <?php
                endforeach;
              endif;
            break;
          endswitch;
        ?>
        </div>
      </div>

      <?php endif; ?>

    <?php endforeach; ?>

    <?php // Donate Button ?>
    <a href="<?= get_permalink(2493) ?>" class="mobile-donate-link button max green">Donate</a>

    <?php // Auxiliary Nav ?>
    <nav class="auxiliary-nav">
      <?php wp_nav_menu( array( 'menu' => 'Auxiliary Navigation' ) ); ?>
    </nav>

  </div>
</div>
