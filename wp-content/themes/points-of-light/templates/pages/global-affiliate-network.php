<?php
  /* GOLBAL AFFILIATES NETWORK PAGE */

  $region_query = $_GET['region'] ?? null;
  $country_query = $_GET['country'] ?? null;
  $nonprofits_query = $_GET['for_nonprofits'] ?? null;
  $individuals_query = $_GET['for_individuals'] ?? null;
  $companies_query = $_GET['for_companies'] ?? null;

  if ( !empty($region_query) )
    $location_query = $region_query;
  elseif ( !empty($country_query) )
    $location_query = $country_query;

  if ( !empty($nonprofits_query) )
    $service_query = $nonprofits_query;
  elseif ( !empty($individuals_query) )
    $service_query = $individuals_query;
  elseif ( !empty($companies_query) )
    $service_query = $companies_query;
?>


<section class="content affiliates-map bg-white">
  <div class="container">
    <header class="module-header">
      <h2 class="module-heading">About Our Global Nonprofit Network</h2>
      <p class="module-subheading">Points of Light operates through a network of innovative volunteer-mobilizing organizations located in more than 250 cities across 37 countries. Together we are inspiring, equipping and mobilizing more people to use their time, talent, voice and money to create positive change in their communities. By taking a global view of service, we help more citizens and many of the world’s leading companies to take action to solve community problems. If you are a civic entrepreneur or innovative volunteer organization who shares our vision and mission, we would love to get to know you. Please write us at <a href="mailto:global@pointsoflight.org">global@pointsoflight.org</a>.</p>
    </header>

    <?php
      // get all lat/lng and pass to client
      $args = array(
        'posts_per_page'   => -1,
        'post_type'        => 'affiliate'
      );
      $affiliates = get_posts( $args ) ?? null;
      global $maps_api_key;
      $markerArr = [];
      foreach($affiliates as $k => $affiliate) {
        $markerArr[$k]['name'] = $affiliate->post_title;
        $markerArr[$k]['location'] = get_field('location', $affiliate->ID);
      }
      echo '<script>var places = '.json_encode($markerArr).';</script>';
    ?>

    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=<?= $maps_api_key ?>"></script>
    <div class="google-map" style="height:600px"></div>

  </div>
</section>

<section class="content affiliates-index bg-white">
  <div class="container">

    <header class="module-header">
      <h2 class="module-heading">Search Partner Nonprofits</h2>
      <p class="module-subheading">By Location & Services Offered</p>
    </header>

    <?php
      $region = [
        'asia-pacific' => 'Asia-Pacific',
        'central-america' => 'Central America',
        'south-america' => 'South America',
        'europe' => 'Europe',
        'africa' => 'Africa',
        'middle-east' => 'Middle East',
        'north-america' => 'North America',
      ];

      // must be gotten dynamically, will change as affiliates are added
      $countriesArr = affiliate_country_filters();
      $countries = [];
      foreach( $countriesArr as $country) {
        $countries[$country['value']] = $country['label'];
      }

      $for_nonprofits = [
        'volunteer-management-training' => 'Volunteer Management Training',
        'nonprofit-consulting' => 'Nonprofit Consulting',
        'volunteer-recruitment-and-placement' => 'Volunteer Recruitment and Placement',
        'service-enterprise-certification' => 'Service Enterprise Certification',
        'executive-search-job-placement' => 'Executive Search/Job Placement',
        'leadership-training' => 'Leadership Training',
        'board-matching-services' => 'Board Matching Services'
      ];

      $for_individuals = [
        'large-scale-events-days-of-service' => 'Large Scale Events &amp; Days of Service',
        'flexible-volunteer-opportunities' => 'Flexible Volunteer Opportunities (Calendar)',
        'long-term-volunteer-opportunities' => 'Long-Term Volunteer Opportunities',
        'cross-border-volunteer-opportunities' => 'Cross Border Volunteer Opportunities',
        'pro-bono-skills-based' => 'Pro-bono/skills-based',
        'board-service-opportunities' => 'Board Service Opportunities',
        'family-youth-volunteer-opportunities' => 'Family &amp; Youth Volunteer Opportunities'
      ];

      $for_companies = [
        'employee-volunteer-program-consulting' => 'Employee Volunteer Program Consulting',
        'corporate-volunteer-council' => 'Corporate Volunteer Council (CVC)',
        'volunteer-program-dev-delivery' => 'Volunteer Program Dev/Delivery',
        'training-leadership-development' => 'Training/Leadership Development',
        'pro-bono-skills-based' => 'Pro-bono/Skills-based'
      ];
    ?>

    <div class="filters-wrap">
      <?= do_shortcode('[ajax_load_more_filters id="affiliate_filters" target="affiliate_results"]') ?>

      <!-- location combo -->
      <div
        class="dropdown combo-dropdown hidden"
        data-value="<?= empty($location_query) ? '#' : $location_query ?>"
        data-linked="region,country"
      >
        <a class="dropdown-button button navy" data-value="#" href="#">
          <span class="title">
            <?php
              if ( !empty($location_query) ) {
                if ( !empty($region_query) )
                  echo $region[$location_query];
                elseif ( !empty($country_query) )
                  echo $countries[$location_query];
              } else {
                echo 'Filter by Location';
              }
            ?>
          </span>
          <i class="icon-arrow-down"></i>
        </a>
        <div class="dropdown-items with-columns">

          <div class="dropdown-column">
            <a class="dropdown-item" href="#" data-value="#">
              Filter By Location
            </a>
            <h4 class="dropdown-heading">Region</h4>
            <div class="dropdown-column-items">
              <?php foreach($region as $key => $value)
                echo '<a class="dropdown-item" href="#" data-value="'.$key.'">'.$value.'</a>';
              ?>
            </div>
          </div>

          <div class="dropdown-column">
            <h4 class="dropdown-heading">Country</h4>
            <div class="dropdown-column-items">
              <?php foreach($countries as $key => $value)
                echo '<a class="dropdown-item" href="#" data-value="'.$key.'">'.$value.'</a>';
              ?>
            </div>
          </div>

        </div>
      </div>

      <!-- services combo -->
      <div
        class="dropdown combo-dropdown hidden"
        data-value="<?= empty($service_query) ? '#' : $service_query ?>"
        data-linked="for_nonprofits,for_individuals,for_companies"
      >
        <a class="dropdown-button button navy" data-value="#" href="#">
          <span class="title">
            <?php
              if ( !empty($service_query) ) {
                if ( !empty($nonprofits_query) )
                  echo $for_nonprofits[$service_query];
                elseif ( !empty($individuals_query) )
                  echo $for_individuals[$service_query];
                elseif ( !empty($companies_query) )
                  echo $for_companies[$service_query];
              } else {
                echo 'Filter by Services';
              }
            ?>
          </span>
          <i class="icon-arrow-down"></i>
        </a>
        <div class="dropdown-items with-columns">

          <div class="dropdown-column">
            <a class="dropdown-item" href="#" data-value="#">
              Filter By Services
            </a>
            <h4 class="dropdown-heading">For Nonprofits</h4>
            <div class="dropdown-column-items">
              <?php foreach($for_nonprofits as $key => $value)
                echo '<a class="dropdown-item" href="#" data-value="'.$key.'">'.$value.'</a>';
              ?>
            </div>
          </div>

          <div class="dropdown-column">
            <h4 class="dropdown-heading">For Individuals</h4>
            <div class="dropdown-column-items">
              <?php foreach($for_individuals as $key => $value)
                echo '<a class="dropdown-item" href="#" data-value="'.$key.'">'.$value.'</a>';
              ?>
            </div>
          </div>

          <div class="dropdown-column">
            <h4 class="dropdown-heading">For Companies</h4>
            <div class="dropdown-column-items">
              <?php foreach($for_companies as $key => $value)
                echo '<a class="dropdown-item" href="#" data-value="'.$key.'">'.$value.'</a>';
              ?>
            </div>
          </div>

        </div>
      </div>

    </div>

    <?= do_shortcode('[ajax_load_more id="affiliate_results" css_classes="affiliate-results" post_type="affiliate" repeater="template_1" target="affiliate_filters" filters="true" transition_container="false" posts_per_page="12" order="ASC" orderby="title" scroll="false" button_label="Load More"]') ?>

  </div>
</section>
