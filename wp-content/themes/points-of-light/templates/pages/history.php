<?php
  /* HISTORY PAGE */

  $args = array(
    'posts_per_page'   => -1,
    'orderby'          => 'meta_value_num',
    'order'            => 'ASC',
    'include'          => '',
    'exclude'          => '',
    'meta_key'         => 'date',
    'meta_value'       => '',
    'post_type'        => 'history'
  );
  $posts_array = get_posts( $args );
  // var_dump($posts_array);
  if(is_array($posts_array)): ?>
<section class="content events">
  <?php
  foreach($posts_array as $post):
    $id = get_the_id();
    $date = get_field('date');
    $desc = get_field('event_desc');
    $event_bg = get_field('event_bg');
    $sidebar_type = get_field('sidebar_type');
    $video_url = get_field('video_url', false, false);
    $icon = get_field('history_icon');
    $cta_title = get_field('cta_title');
    $cta_desc = get_field('cta_desc');
    $cta_button = get_field('cta_button_buttons');
    $quote = get_field('event_quote');
    $author = get_field('author');

  ?>
  <div class="event <?= !empty($event_bg) ? ' with-bg' : '' ?> <?= $sidebar_type == 'quote' ? ' with-quote' : '' ?>" <?= !empty($event_bg) ? 'style="background: url('.wp_get_attachment_image_url($event_bg, 'bg').') center/cover no-repeat;"' : '' ?>>
    <div class="pseudo-container">
      <div class="main">
        <h2><?= $date ?></h2>
        <div class="wysiwyg"><?= $desc ?></div>
      </div>
      <?php if(!empty($sidebar_type) && $sidebar_type != 'none') : ?>
        <div class="sidebar">
          <?php if($sidebar_type == 'quote') : ?>
          <blockquote>
            <?= $quote ?>
            <cite><span class="mdash">&mdash;</span> <?= $author ?></cite>
          </blockquote>
          <?php elseif($sidebar_type == 'video') : ?>
            <a href="<?= $video_url ?>" class="video-link"><?= file_get_contents(get_template_directory_uri() . '/assets/img/play.svg'); ?></a>
          <?php elseif($sidebar_type == 'cta') : ?>
          <div class="cta">
            <i class="<?= $icon ?>"></i>
            <?= $cta_title ? '<h3>'.$cta_title.'</h3>' : '' ?>
            <?= $cta_desc ? '<p>'.$cta_desc.'</p>' : '' ?>
            <?php print_buttons_group_prefix('cta_button_', $id); ?>
          </div>
          <?php endif; ?>
        </div>
      <?php endif; ?>
    </div>
  </div>

<?php
  endforeach;
  wp_reset_postdata();
endif;
?>
</section>
