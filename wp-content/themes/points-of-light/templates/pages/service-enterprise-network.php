<?php
  /* SERVICE ENTERPRISE NETWORK PAGE */

  $nonprofits_query = $_GET['for_nonprofits'] ?? null;
  $individuals_query = $_GET['for_individuals'] ?? null;
  $companies_query = $_GET['for_companies'] ?? null;

  if ( !empty($nonprofits_query) )
    $service_query = $nonprofits_query;
  elseif ( !empty($individuals_query) )
    $service_query = $individuals_query;
  elseif ( !empty($companies_query) )
    $service_query = $companies_query;
?>

<section class="content affiliates-map bg-white">
  <div class="container">
    <header class="module-header">
      <h2 class="module-heading">Service Enterprise Network</h2>
      <p class="module-subheading">Lorem ipsum dolor sit amet consectetur adipisicing elit. Eos temporibus facere veniam doloribus voluptate blanditiis inventore consequuntur aliquam, suscipit corporis!</p>
    </header>

    <?php
      // get all lat/lng and pass to client
      $args = array(
        'posts_per_page'   => -1,
        'post_type'        => 'service-enterprise'
      );
      $affiliates = get_posts( $args ) ?? null;
      global $maps_api_key;
      $markerArr = [];
      foreach($affiliates as $k => $affiliate) {
        $markerArr[$k]['name'] = $affiliate->post_title;
        $markerArr[$k]['location'] = get_field('location', $affiliate->ID);
      }
      echo '<script>var places = '.json_encode($markerArr).';</script>';
    ?>

    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=<?= $maps_api_key ?>"></script>
    <div class="google-map" style="height:600px"></div>
  </div>
</section>

<section class="content affiliates-index bg-white">
  <div class="container">
    <header class="module-header">
      <h2 class="module-heading">Search Partner Nonprofits</h2>
      <p class="module-subheading">By Location & Services Offered</p>
    </header>

    <div class="filters-wrap">
      <?= do_shortcode('[ajax_load_more_filters id="enterprise_filters" target="enterprise_results"]') ?>
    </div>

    <?= do_shortcode('[ajax_load_more id="enterprise_results" css_classes="affiliate-results" post_type="service-enterprise" repeater="template_1" target="enterprise_filters" filters="true" transition_container="false" posts_per_page="12" order="ASC" orderby="title" scroll="false" button_label="Load More"]') ?>

  </div>
</section>
