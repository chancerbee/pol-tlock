<?php
/****** Blog ******/
?>

<section class="content bg-white">
  <div class="container">

    <div class="filters-wrap">
      <?php
        $filter_args = 'id="blog_filters" ';
        $filter_args.= 'target="blog_results" ';
        echo do_shortcode('[ajax_load_more_filters '.$filter_args.']');
      ?>
    </div>

    <?php
      $alm_args = 'id="blog_results" ';
      $alm_args.= 'css_classes="card-grid-advanced blog-results" ';
      $alm_args.= 'post_type="post" ';
      $alm_args.= 'repeater="template_3" ';
      $alm_args.= 'target="blog_filters" ';
      $alm_args.= 'filters="true" ';
      $alm_args.= 'transition_container="false" ';
      $alm_args.= 'posts_per_page="16" ';
      $alm_args.= 'scroll="false" ';
      $alm_args.= 'button_label="Load More" ';
      echo do_shortcode('[ajax_load_more '.$alm_args.']');
    ?>
  </div>
</section>
