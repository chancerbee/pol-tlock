<?php
  $query = get_search_query();
  global $wp_query;
?>

<section class="content site-search-results bg-white">
  <div class="container">
    <header class="results-header">
      <form role="search" method="get" action="<?= get_site_url() ?>" class="site-search-form">
        <input class="search-input" type="search" id="s" name="s" placeholder="Try a new search" value="">
        <button>
          <i class="icon-search"></i>
        </button>
      </form>
    </header>

    <?php
      if ( have_posts() ):
        while ( have_posts() ) : the_post();

          $post_type = get_post_type();
          $title = get_the_title();
          $exceprt = get_the_excerpt();
          $image = get_the_post_thumbnail($post, 'one-third-container');
          $url = get_permalink();
          $target = '';

          if ( $post_type == 'affiliate' ) {
            $url = get_field('url');
            $target = 'target="_blank"';
            $image = null;
            $exceprt = 'One of our Global Affiliate Network Partners';
          } elseif ( $post_type == 'service-enterprise' ) {
            $url = get_field('url');
            $target = 'target="_blank"';
            $image = null;
            $exceprt = 'One of our Service Enterprise Network Partners';
          } elseif ( $post_type == 'resource') {
            $image = wp_get_attachment_image(get_field('resource_image'), 'one-third-container');
          } elseif ( $post_type == 'resource' && get_field('resource_type') == 'video') {
            $image = wp_get_attachment_image(get_field('resource_video_image'), 'one-third-container');
          } elseif ( $post_type == 'page' ) {
            $image = wp_get_attachment_image(get_field('image'), 'one-third-container');
            $exceprt = get_field('subhead');
          } elseif ( $post_type == 'people') {
            $image = wp_get_attachment_image(get_field('person_portrait'), 'one-third-container');
          }

    ?>

    <div class="module-type-search-result">
      <div class="columns-two">
        <div class="main-col">
          <h2 class="main-heading"><?= $title ?></h2>
          <div class="main-text wysiwyg">
            <p><?= !empty($exceprt) ? $exceprt : '' ?></p>
          </div>

          <?php if ( !empty($url) ) : ?>

          <a class="button" href="<?= $url ?>" <?= $target ?>>Learn More</a>

          <?php endif; ?>

        </div>
        <div class="left-col col-type-image">

          <?php if ( !empty($image) ): ?>

          <div class="left-image">
            <a href="<?= $url ?>">
              <?= $image ?>
            </a>
          </div>

          <?php endif; ?>

        </div>
      </div>
    </div>

    <?php
        endwhile;

        the_posts_pagination( array(
          'mid_size'  => 2,
          'prev_text' => '<i class="icon-arrow-left"></i> Previous',
          'next_text' => 'Next <i class="icon-arrow-right"></i>',
          'screen_reader_text' => 'Search Results Pagination'
        ) );

      else:
    ?>

    <h3 class="no-results">Sorry, we couldn't find any content matching "<?= $query ?>"</h3>

    <?php endif; ?>

  </div>
</section>
