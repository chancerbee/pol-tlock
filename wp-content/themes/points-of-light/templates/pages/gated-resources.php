<?php
/****** Gated Resources ******/
  $login_heading = get_field('login_form_header');
  $login_subheading = get_field('login_form_subheading');

?>

    <?php
    global $user_login;

    // If user is already logged in.
    if ( is_user_logged_in() ) :
      include_once dirname(__FILE__) . '/../includes/resource-feed.php';
      get_footer();
      //exit here so we dont get extra flexible content
      exit;
    // If user is not logged in.
    else:

        // Login form arguments.
        $args = array(
          'echo'           => true,
          'redirect'       => home_url( '/gated-resources/' ),
          'form_id'        => 'loginform',
          'label_username' => __( 'Username' ),
          'label_password' => __( 'Password' ),
          'label_remember' => __( 'Remember Me' ),
          'label_log_in'   => __( 'Log In' ),
          'id_username'    => 'user_login',
          'id_password'    => 'user_pass',
          'id_remember'    => 'rememberme',
          'id_submit'      => 'wp-submit',
          'remember'       => true,
          'value_username' => NULL,
          'value_remember' => true
        );
        ?>
  <section class="module module-type-gravity-form login-form">
    <div class="container">
      <div class="left-col">
        <div class="text">
          <h2 class="module-heading"><?= $login_heading ?></h2>
          <?= $login_subheading ? '<p class="module-subheading">'.$login_subheading.'</p>' : '' ?>
        </div>
      </div>
      <div class="right-col">
        <h2>Log In Below</h2>
        <?php // In case of a login error.
        if ( isset( $_GET['login'] ) && $_GET['login'] == 'failed' ) : ?>
          <div class="login-error">
            <p><?php _e( 'FAILED: Try again!', 'AA' ); ?></p>
          </div>
        <?php
        endif;
        ?>
        <?php
          // Calling the login form.
          wp_login_form( $args );
        ?>
      </div>
    </div>
  </section>
<?php
  endif;
?>
