<?php
/****** SINGLE POST VIEW ******/

get_header();
?>

<?php
	while ( have_posts() ) : the_post();

    // compound word post types use underscores, but we want file system to use hyphens
    $type = str_replace('_', '-', get_post_type() );
    include_once('templates/single/'.$type.'.php');

	endwhile; // End of the loop.
?>

<?php get_footer(); ?>
