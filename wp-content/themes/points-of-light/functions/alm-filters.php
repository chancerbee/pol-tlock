<?php
// GLOBAL AFFILIATES
  // Get region filters dynamically from region custom field choices
  function affiliate_region_filters(){

    $types = get_field_object('field_5c61412bd0b3d')['choices'];

    if ( $types ) {
      foreach( $types as $key => $value ) {
        $values[] = array(
          'label' => $value,
          'value' => $key
        );
      }
    }
    return $values;

  }
  add_filter( 'alm_filters_affiliate_filters_region', 'affiliate_region_filters' );

  // Get country filters dynamically from all post countries
  function affiliate_country_filters(){

    $affiliates = get_posts( ['posts_per_page' => -1, 'post_type' => 'affiliate'] ) ?? [];
    $countries = [];
    foreach ( $affiliates as $affiliate ) {
      $country = get_field('location', $affiliate->ID)['country'];
      if ( !in_array($country, $countries) )
        $countries[] = $country;
    }

    if ( $countries ) {
      sort($countries);
      foreach ( $countries as $country ) {
        $values[] = array(
          'label' => $country,
          'value' => sanitize_title($country)
        );
      }
    }
    return $values;

  }
  add_filter( 'alm_filters_affiliate_filters_country', 'affiliate_country_filters');

  // Get nonprofit service filter dynamically from nonprofits custom field choices
  function affiliate_fornonprofit_filters(){
    $types = get_field_object('field_5c63db510c38d')['choices'];

    if ( $types ) {
      foreach ( $types as $key => $value ) {
        $values[] = array(
          'label' => $value,
          'value' => $key
        );
      }
    }
    return $values;

  }
  add_filter( 'alm_filters_affiliate_filters_for_nonprofits', 'affiliate_fornonprofit_filters');

  // Get individual service filter dynamically from individuals custom field choices
  function affiliate_forindividual_filters(){
    $types = get_field_object('field_5c73e2a036bcd')['choices'];

    if ( $types ) {
      foreach ( $types as $key => $value ) {
        $values[] = array(
          'label' => $value,
          'value' => $key
        );
      }
    }
    return $values;

  }
  add_filter( 'alm_filters_affiliate_filters_for_individuals', 'affiliate_forindividual_filters');

  // Get company service filter dynamically from companies custom field choices
  function affiliate_forcompany_filters(){
    $types = get_field_object('field_5c73e3aa7f109')['choices'];

    if ( $types ) {
      foreach ( $types as $key => $value ) {
        $values[] = array(
          'label' => $value,
          'value' => $key
        );
      }
    }
    return $values;

  }
  add_filter( 'alm_filters_affiliate_filters_for_companies', 'affiliate_forcompany_filters');

// SERVICE ENTERPRISES

  // Get state filters dynamically from all post states
  function enterprise_state_filters(){

    $enterprises = get_posts( ['posts_per_page' => -1, 'post_type' => 'service-enterprise'] );
    $states = [];
    foreach ( $enterprises as $enterprise ) {
      $state = get_field('state', $enterprise->ID);
      if ( !in_array($state, $states) && $state !== null)
        $states[] = $state;
    }

    $map = [
      'AL'=>'Alabama',
      'AK'=>'Alaska',
      'AZ'=>'Arizona',
      'AR'=>'Arkansas',
      'CA'=>'California',
      'CO'=>'Colorado',
      'CT'=>'Connecticut',
      'DE'=>'Delaware',
      'DC'=>'District of Columbia',
      'FL'=>'Florida',
      'GA'=>'Georgia',
      'HI'=>'Hawaii',
      'ID'=>'Idaho',
      'IL'=>'Illinois',
      'IN'=>'Indiana',
      'IA'=>'Iowa',
      'KS'=>'Kansas',
      'KY'=>'Kentucky',
      'LA'=>'Louisiana',
      'ME'=>'Maine',
      'MD'=>'Maryland',
      'MA'=>'Massachusetts',
      'MI'=>'Michigan',
      'MN'=>'Minnesota',
      'MS'=>'Mississippi',
      'MO'=>'Missouri',
      'MT'=>'Montana',
      'NE'=>'Nebraska',
      'NV'=>'Nevada',
      'NH'=>'New Hampshire',
      'NJ'=>'New Jersey',
      'NM'=>'New Mexico',
      'NY'=>'New York',
      'NC'=>'North Carolina',
      'ND'=>'North Dakota',
      'OH'=>'Ohio',
      'OK'=>'Oklahoma',
      'OR'=>'Oregon',
      'PA'=>'Pennsylvania',
      'RI'=>'Rhode Island',
      'SC'=>'South Carolina',
      'SD'=>'South Dakota',
      'TN'=>'Tennessee',
      'TX'=>'Texas',
      'UT'=>'Utah',
      'VT'=>'Vermont',
      'VA'=>'Virginia',
      'WA'=>'Washington',
      'WV'=>'West Virginia',
      'WI'=>'Wisconsin',
      'WY'=>'Wyoming'
    ];

    if ( $states ) {
      sort($states);
      foreach ( $states as $state ) {
        $values[] = array(
          'label' => $map[$state],
          'value' => $state
        );
      }
    }
    return $values;

  }
  add_filter( 'alm_filters_enterprise_filters_state', 'enterprise_state_filters');

// PRESS RELEASES

  // Get years from press_years in options table
  function press_year_filter(){
    $years = get_option('press_years');
    $values = [];
    foreach ( $years as $year ) {
      $values[] = array(
        'label' => $year,
        'value' => $year
      );
    }
    return $values;
  }
  add_filter( 'alm_filters_press_filters_date', 'press_year_filter');

// BLOG POSTS

  // Get years from post years
  function blog_year_filter(){
    $years = get_option('post_years');
    $values = [];
    foreach ( $years as $year ) {
      $values[] = array(
        'label' => $year,
        'value' => $year
      );
    }
    return $values;
  }
  add_filter( 'alm_filters_blog_filters_year', 'blog_year_filter');
