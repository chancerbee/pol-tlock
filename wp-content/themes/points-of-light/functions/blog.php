<?php
/**
 * Functions related to posts and blogs
 */

  // REMOVE TAGS FROM POSTS
// function myprefix_unregister_tags() {
//     unregister_taxonomy_for_object_type('post_tag', 'post');
// }
// add_action('init', 'myprefix_unregister_tags');

  // SEARCH ONLY POSTS
// function search_filter($query) {
//     if ($query->is_search && !is_admin() ) {
//         $query->set('post_type', 'post');
//     }
//     return $query;
// }
// add_filter('pre_get_posts','search_filter');

  // GET EXCERPT OUTSIDE THE LOOP
function get_excerpt_by_id($post_id){
  $the_post = get_post($post_id); //Gets post ID
  $the_excerpt = $the_post->post_content; //Gets post_content to be used as a basis for the excerpt
  $excerpt_length = 30; //Sets excerpt length by word count
  $the_excerpt = strip_tags(strip_shortcodes($the_excerpt)); //Strips tags and images
  $words = explode(' ', $the_excerpt, $excerpt_length + 1);

  if(count($words) > $excerpt_length) :
    array_pop($words);
    array_push($words, '…');
    $the_excerpt = implode(' ', $words);
  endif;
  return $the_excerpt;
}

  // CUSTOMIZE EXCERPT
function custom_excerpt_length( $length ) {
	return 30;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );
function custom_excerpt_more( $more ) {
	return '…';
}
add_filter('excerpt_more', 'custom_excerpt_more');

  // TRACK POST VIEWS
function wpb_set_post_views($postID) {
  $count_key = 'wpb_post_views_count';
  $count = get_post_meta($postID, $count_key, true);
  if($count==''){
    $count = 0;
    delete_post_meta($postID, $count_key);
    add_post_meta($postID, $count_key, '0');
  }else{
    $count++;
    update_post_meta($postID, $count_key, $count);
  }
}
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
function wpb_track_post_views ($post_id) {
  if ( !is_single() ) return;
  if ( empty ( $post_id) ) {
    global $post;
    $post_id = $post->ID;
  }
  wpb_set_post_views($post_id);
}
add_action( 'wp_head', 'wpb_track_post_views');
