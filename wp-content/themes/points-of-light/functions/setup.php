<?php
/**
 * Theme Setup
 */

if ( ! function_exists( 'hive_setup' ) ) :
function hive_setup() {
	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

  // Let Wordpress manage the document title
	add_theme_support( 'title-tag' );

  // Add support for thumbnails on posts
	add_theme_support( 'post-thumbnails', array( 'post', 'press_release' ) );

	// function add_tags_categories() {
	// 	register_taxonomy_for_object_type('category', 'resource');
	// 	register_taxonomy_for_object_type('post_tag', 'resource');
	// }
	// add_action('init', 'add_tags_categories');

	// Using wp_nav_menu() in one location.
	register_nav_menus( array(
		'header' => esc_html__( 'Header', 'hive' )
	) );

  // Valid HTML 5 for core markup
	add_theme_support( 'html5', array('search-form','comment-form','comment-list','gallery','caption') );
}
endif;
add_action( 'after_setup_theme', 'hive_setup' );
