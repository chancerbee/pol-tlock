<?php
/**
 * Custom taxonomies
 */

  // EXTEND WORDPRESS POST CATEGORIES TO OTHER CPTs
//  add_action( 'init', 'extend_wp_categories' );
// function extend_wp_categories() {
// 	register_taxonomy_for_object_type( 'category', 'ebook');
// 	register_taxonomy_for_object_type( 'category', 'video');
// 	register_taxonomy_for_object_type( 'category', 'case_study');
// 	register_taxonomy_for_object_type( 'category', 'infographic');
// }


  // CREATE CUSTOM TAXONOMY
// add_action( 'init', 'create_book_taxonomies', 0 );
// function create_book_taxonomies() {
//   $labels = array(
//       'name'              => _x( 'Genres', 'taxonomy general name', 'textdomain' ),
//       'singular_name'     => _x( 'Genre', 'taxonomy singular name', 'textdomain' ),
//       'search_items'      => __( 'Search Genres', 'textdomain' ),
//       'all_items'         => __( 'All Genres', 'textdomain' ),
//       'parent_item'       => __( 'Parent Genre', 'textdomain' ),
//       'parent_item_colon' => __( 'Parent Genre:', 'textdomain' ),
//       'edit_item'         => __( 'Edit Genre', 'textdomain' ),
//       'update_item'       => __( 'Update Genre', 'textdomain' ),
//       'add_new_item'      => __( 'Add New Genre', 'textdomain' ),
//       'new_item_name'     => __( 'New Genre Name', 'textdomain' ),
//       'menu_name'         => __( 'Genre', 'textdomain' ),
//   );
//
//   $args = array(
//       'hierarchical'      => true,
//       'labels'            => $labels,
//       'show_ui'           => true,
//       'show_admin_column' => true,
//       'query_var'         => true,
//       'rewrite'           => array( 'slug' => 'genre' ),
//   );
//
//   register_taxonomy( 'genre', array( 'book' ), $args );
//
//   // Add new taxonomy, NOT hierarchical (like tags)
//   $labels = array(
//       'name'                       => _x( 'Writers', 'taxonomy general name', 'textdomain' ),
//       'singular_name'              => _x( 'Writer', 'taxonomy singular name', 'textdomain' ),
//       'search_items'               => __( 'Search Writers', 'textdomain' ),
//       'popular_items'              => __( 'Popular Writers', 'textdomain' ),
//       'all_items'                  => __( 'All Writers', 'textdomain' ),
//       'parent_item'                => null,
//       'parent_item_colon'          => null,
//       'edit_item'                  => __( 'Edit Writer', 'textdomain' ),
//       'update_item'                => __( 'Update Writer', 'textdomain' ),
//       'add_new_item'               => __( 'Add New Writer', 'textdomain' ),
//       'new_item_name'              => __( 'New Writer Name', 'textdomain' ),
//       'separate_items_with_commas' => __( 'Separate writers with commas', 'textdomain' ),
//       'add_or_remove_items'        => __( 'Add or remove writers', 'textdomain' ),
//       'choose_from_most_used'      => __( 'Choose from the most used writers', 'textdomain' ),
//       'not_found'                  => __( 'No writers found.', 'textdomain' ),
//       'menu_name'                  => __( 'Writers', 'textdomain' ),
//   );
//
//   $args = array(
//       'hierarchical'          => false,
//       'labels'                => $labels,
//       'show_ui'               => true,
//       'show_admin_column'     => true,
//       'update_count_callback' => '_update_post_term_count',
//       'query_var'             => true,
//       'rewrite'               => array( 'slug' => 'writer' ),
//   );
//   register_taxonomy( 'writer', 'book', $args );
// }

//ADD GATED RESOURCE TAX
add_action( 'init', 'create_resource_taxonomies', 0 );
function create_resource_taxonomies() {
  $labels = array(
      'name'              => _x( 'Resource Types', 'taxonomy general name', 'textdomain' ),
      'singular_name'     => _x( 'Resource Type', 'taxonomy singular name', 'textdomain' ),
      'search_items'      => __( 'Search Types', 'textdomain' ),
      'all_items'         => __( 'All Types', 'textdomain' ),
      'parent_item'       => __( 'Parent Type', 'textdomain' ),
      'parent_item_colon' => __( 'Parent Type:', 'textdomain' ),
      'edit_item'         => __( 'Edit Type', 'textdomain' ),
      'update_item'       => __( 'Update Type', 'textdomain' ),
      'add_new_item'      => __( 'Add New Type', 'textdomain' ),
      'new_item_name'     => __( 'New Type Name', 'textdomain' ),
      'menu_name'         => __( 'Resource Type', 'textdomain' ),
  );

  $args = array(
      'hierarchical'      => false,
      'labels'            => $labels,
      'show_ui'           => false,
      'show_admin_column' => true,
      'query_var'         => true,
      'rewrite'           => array( 'slug' => 'resource-type' ),
  );

  register_taxonomy( 'resource-type', array( 'resource' ), $args );

}

//ADD PEOPLE TAX
add_action( 'init', 'create_people_taxonomies', 0 );
function create_people_taxonomies() {
  $labels = array(
      'name'              => _x( 'Role', 'taxonomy general name', 'textdomain' ),
      'singular_name'     => _x( 'Role', 'taxonomy singular name', 'textdomain' ),
      'search_items'      => __( 'Search Roles', 'textdomain' ),
      'all_items'         => __( 'All Roles', 'textdomain' ),
      'parent_item'       => __( 'Parent Role', 'textdomain' ),
      'parent_item_colon' => __( 'Parent Role:', 'textdomain' ),
      'edit_item'         => __( 'Edit Role', 'textdomain' ),
      'update_item'       => __( 'Update Role', 'textdomain' ),
      'add_new_item'      => __( 'Add New Role', 'textdomain' ),
      'new_item_name'     => __( 'New Role Name', 'textdomain' ),
      'menu_name'         => __( 'Role', 'textdomain' ),
  );

  $args = array(
      'hierarchical'      => false,
      'labels'            => $labels,
      'show_ui'           => false,
      'show_admin_column' => true,
      'query_var'         => true,
      'rewrite'           => array( 'slug' => 'role' ),
  );

  register_taxonomy( 'role', array( 'people' ), $args );

}

?>
