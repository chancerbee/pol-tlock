<?php

// GET EXCERPT FROM NON-BLOG POSTS
function get_post_type_excerpt($post_id){
  $content = get_field('content', $post_id);
  if ( get_post_type($post_id) == 'resource' )
    $content = get_field('main_resource_content');
  $excerpt_length = 30;
  $the_excerpt = strip_tags(strip_shortcodes($content));
  $words = explode(' ', $the_excerpt, $excerpt_length + 1);

  if(count($words) > $excerpt_length) :
    array_pop($words);
    array_push($words, '…');
    $the_excerpt = implode(' ', $words);
  endif;
  return $the_excerpt;
}

// STORE ARRAY OF POST YEARS IN DATABASE (avoid looping all posts to construct filter)
  function get_post_years() {
    $args = [
      'posts_per_page' => -1,
      'post_type' => 'post',
      'fields' => 'ids'
    ];
    $post_ids = get_posts( $args );
    $years = [];
    foreach ( $post_ids as $post_id ) {
      $year = get_the_date('Y', $post_id);
      if ( !in_array( $year, $years ) )
        $years[] = $year;
    }
    rsort($years);
    return $years;
  }
  function save_post_years($post_id) {

    // If this isn't a 'post' do nothing
    if ( "post" != get_post_type($post_id) ) return;

    update_option('post_years', get_post_years() );
    return;
  }
  add_action( 'save_post', 'save_post_years', 10 );

// STORE ARRAY OF PRESS RELEASE YEARS IN DATABASE (avoid looping all of them to construct filter)
  function get_press_years() {
    $args = [
      'post_type' => 'press_release',
      'posts_per_page' => -1,
      'fields' => 'ids'
    ];
    $press_ids = get_posts($args);
    $years = [];
    foreach ( $press_ids as $press_id ) {
      $year = strstr(get_field('date', $press_id), ', ');
      $year = str_replace(', ', '', $year);
      if ( !in_array( $year, $years ) )
        $years[] = $year;
    }
    rsort($years);
    return $years;
  }

  function save_press_years($post_id) {

    // If this isn't a 'press_release' do nothing
    if ( "press_release" != get_post_type($post_id) ) return;

    update_option('press_years', get_press_years() );
    return;
  }
  add_action( 'save_post', 'save_press_years', 10 );


// STORE ANCHOR NAV MARKUP IN DATABASE (avoid looping content blocks on every page load)
  function get_anchor_nav($post_id) {

    $blocks = get_field('content_blocks', $post_id);
    $anchor_nav = '<nav class="page-header-anchor-nav">';
    foreach ( $blocks as $block ) {
      if ( !empty($block['anchor_title']) && !empty($block['anchor_id']) ) {
        $anchor_id = '#'.$block['anchor_id'];
        $anchor_title = $block['anchor_title'];
        $anchor_link = '<a href="'.$anchor_id.'">'.$anchor_title.'</a>';
        $anchor_nav .= $anchor_link;
      }
    }
    $anchor_nav .= '</nav>';
    $anchor_nav .= '<div data-value="#" class="dropdown anchor-dropdown">
                      <a href="#" class="dropdown-button button navy" data-value="#">
                        <span class="title">About</span>
                        <i class="icon-arrow-down"></i>
                      </a>
                      <div class="dropdown-items">';
    foreach ( $blocks as $block ) {
      if ( !empty($block['anchor_title']) && !empty($block['anchor_id']) ) {
        $anchor_id = '#'.$block['anchor_id'];
        $anchor_title = $block['anchor_title'];
        $anchor_link = '<a class="dropdown-item" href="'.$anchor_id.'">'.$anchor_title.'</a>';
        $anchor_nav .= $anchor_link;
      }
    }
    $anchor_nav .= '</div></div>';

    return $anchor_nav;
  }
  function save_anchor_nav($post_id) {

    // If this isn't a 'page' do nothing
    if ( "page" != get_post_type($post_id) ) return;

    // If nav is not enabled, delete the option and exit
    $nav_enabled = get_field('anchor_nav_enabled', $post_id);
    if ( empty($nav_enabled) )  {
      delete_option('page_'.$post_id.'_anchor_nav');
      return;
    }

    update_option('page_'.$post_id.'_anchor_nav', get_anchor_nav($post_id) );
    return;
  }
  add_action( 'save_post', 'save_anchor_nav', 10 );

// STORE ARRAY OF RESOURCES IN DATABASE FOR USE IN PREV/NEXT LINKS
  function get_ungated_resources_list() {
    $args = [
      'post_type' => 'resource',
      'posts_per_page' => -1,
      'order' => 'DESC',
      'meta_key' => 'gate_resource',
      'meta_value' => 1,
      'meta_compare' => '!=',
      'fields' => 'ids'
    ];
    $ungated_arr = get_posts($args);
    return $ungated_arr;
  }
  function get_gated_resources_list() {
    $args = [
      'post_type' => 'resource',
      'posts_per_page' => -1,
      'order' => 'DESC',
      'meta_key' => 'gate_resource',
      'meta_value' => 1,
      'meta_compare' => '=',
      'fields' => 'ids'
    ];
    $gated_arr = get_posts($args);
    return $gated_arr;
  }

  // create/update the resource array 'option' on save
  function save_resource_arr( $post_id, $post, $update ) {

    // If this isn't a 'resource' do nothing
    if ( "resource" != get_post_type($post_id) ) return;

    update_option('ungated_resources_list', get_ungated_resources_list() );
    update_option('gated_resources_list', get_gated_resources_list() );
    return;
  }
  add_action( 'save_post', 'save_resource_arr', 10, 3 );

// DEFINE FUNCTIONS FOR PREV/NEXT GATED/UNGATED RESOURCES
function get_adjacent_resource_link( $access_level, $direction ) {
  $post_id = get_the_ID();
  $resources = get_option( $access_level.'_resources_list' ) ?? false;

  // Abort if no resources found
  if( $resources === false ) return;

  // Get the position of the current resource
  $pos = array_search( $post_id, $resources );

  // Get the postion of the previous or next resource
  if( $direction == 'previous' ) {
    $new_pos = $pos - 1;
  } elseif ( $direction == 'next' ) {
    $new_pos = $pos + 1;
  }

  // Get the target resource id
  if( !empty($resources[$new_pos]) ) {
    $adjacent_id = $resources[$new_pos];

    // compose the link markup
    $string = sprintf(
      '<a class="%s" href="%s">%s</a>',
      $direction,
      get_permalink( $adjacent_id ),
      $direction == 'previous' ? '<i class="icon-arrow-left"></i> '.ucfirst($direction) : ucfirst($direction).' <i class="icon-arrow-right"></i>'
    );
    return $string;
  } else {
    return false;
  }
}

// ADD LOGIN/LOGOUT LINK TO AUX MENU
function add_logout_link($items, $args) {

  if ( $args->menu == 'Auxiliary Navigation' ) {
    if ( is_user_logged_in() ) {
      $link = '<a href="'.wp_logout_url(home_url().'?logout=true').'">Logout</a>';
    } else {
      $link = '<a href="'.get_permalink(969).'">Login</a>';
    }
    $items .= '<li class="menu-item">'.$link.'</li>';
  }
  return $items;
}
//REMOVED - uncomment when gated resources are live
// add_filter('wp_nav_menu_items', 'add_logout_link', 10, 2);

// HIDE ADMIN BAR FOR SUBSCRIBERS
function cc_hide_admin_bar() {
  if (current_user_can('subscriber')) {
    show_admin_bar(false);
  }
}
add_action('set_current_user', 'cc_hide_admin_bar');

// LOCK SUBSCRIBERS OUT OF ADMIN
function wpse23007_redirect(){
  if( is_admin() && !defined('DOING_AJAX') && current_user_can('subscriber') ) {
    wp_redirect(home_url());
    exit;
  }
}
add_action('init','wpse23007_redirect');

// REVERSE GEOCODE ADDRESS TO GET FINER GRAIN INFO ABOUT LOCATION
function rev_geocode($address){
   global $maps_api_key;
   $address = str_replace (" ", "+", $address);
   $ping = 'https://maps.googleapis.com/maps/api/geocode/json?address='.$address.'&key='.$maps_api_key;

   $ch = curl_init();
   curl_setopt($ch, CURLOPT_URL, $ping);
   curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
   $geoloc = json_decode(curl_exec($ch), true)['results']['0'];

   $city_match = ['locality', 'political'];
   $state_match = ['administrative_area_level_1', 'political'];
   $country_match = ['country', 'political'];

   $city = '';
   $state = '';
   $country = '';
   foreach ($geoloc['address_components'] as $addr_component) {
     if ( $addr_component['types'] == $city_match )
      $city = $addr_component['long_name'];
     if ( $addr_component['types'] == $state_match )
      $state = $addr_component['short_name'];
     if ( $addr_component['types'] == $country_match )
      $country = $addr_component['long_name'];
   }
   $breakdown['city'] = $city;
   $breakdown['state'] = $state;
   $breakdown['country'] = $country;

   return $breakdown;
}

// STORE MENUS AS GLOBAL
function menu_items() {
	$menu_arr = [];
  if ( have_rows( 'menu_items', 'option' ) ) :
    while ( have_rows( 'menu_items', 'option' ) ): the_row();
      $top_item = [];
      $top_item['title'] = get_sub_field('title');
      $top_item['type'] = get_sub_field('type');
      $top_item['dropdown'] = get_sub_field('dropdown');
      $top_item['url'] = get_permalink( get_sub_field('link') );
      if ( empty($top_item['url']) )
        $top_item['url'] = '';
      array_push( $menu_arr, $top_item);
		endwhile;
	endif;

	return $menu_arr;
}
global $menu_items;
$menu_items = menu_items();

// PRINT LINKS
function print_link($field_name, $classes = '', $icon = '') {
  $link = get_sub_field($field_name);
  if ( !empty($icon) ) $icon = ' <i class="icon-'.$icon.'"></i>';
  print '<a href="'.$link['url'].'" target="'.$link['target'].'" class="'.$classes.'">'.$link['title'].$icon.'</a>';
}
function print_link_array($field_name, $classes = '', $icon = '') {
  $link = $field_name;
  if ( !empty($icon) ) $icon = ' <i class="icon-'.$icon.'"></i>';
  print '<a href="'.$link['url'].'" target="'.$link['target'].'" class="'.$classes.'">'.$link['title'].$icon.'</a>';
}

// PRINT BUTTONS
  function print_buttons($prefix = '') {

    if (have_rows($prefix.'buttons')):

      print '<div class="buttons">';

      while( have_rows($prefix.'buttons') ): the_row();

        $link = get_sub_field('link');
		    $color = get_sub_field('color');
        $icon = get_sub_field('icon');

        if ($icon)
          $text = '<i class="fa '.$icon.'"></i> '.$link['title'];
        else
          $text = $link['title'];

        print '<a href="'.$link['url'].'" target="'.$link['target'].'" class="button '.$color.'">'.$text.'</a>';

      endwhile;

      print '</div>';
    endif;

  }

  //Print buttons for group not seamless
  function print_buttons_group($array) {
    $button_arr = $array['buttons'];
    if (is_array($button_arr)):

      print '<div class="buttons">';

      foreach($button_arr as $button ):

        $link = $button['link'];
		    $color = $button['color'];
        $icon = $button['icon'];

        if ($icon)
          $text = '<i class="fa '.$icon.'"></i> '.$link['title'];
        else
          $text = $link['title'];

        print '<a href="'.$link['url'].'" target="'.$link['target'].'" class="button '.$color.'">'.$text.'</a>';

      endforeach;

      print '</div>';
    endif;

  }

  //Print buttons for group not seamless
  function print_buttons_group_prefix($prefix = '', $id) {
    $button_arr = get_field($prefix.'buttons', $id);
    if (is_array($button_arr)):

      print '<div class="buttons">';

      foreach($button_arr as $button ):

        $link = $button['link'];
		    $color = $button['color'];
        $icon = $button['icon'];

        if ($icon)
          $text = '<i class="fa '.$icon.'"></i> '.$link['title'];
        else
          $text = $link['title'];

        print '<a href="'.$link['url'].'" target="'.$link['target'].'" class="button '.$color.'">'.$text.'</a>';

      endforeach;

      print '</div>';
    endif;

  }


  //DISABLE EMOJIS
function disable_emojis() {
 remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
 remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
 remove_action( 'wp_print_styles', 'print_emoji_styles' );
 remove_action( 'admin_print_styles', 'print_emoji_styles' );
 remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
 remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
 remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
 add_filter( 'tiny_mce_plugins', 'disable_emojis_tinymce' );
 add_filter( 'wp_resource_hints', 'disable_emojis_remove_dns_prefetch', 10, 2 );
}
add_action( 'init', 'disable_emojis' );

function disable_emojis_tinymce( $plugins ) {
 if ( is_array( $plugins ) ) {
 return array_diff( $plugins, array( 'wpemoji' ) );
 } else {
 return array();
 }
}

function disable_emojis_remove_dns_prefetch( $urls, $relation_type ) {
 if ( 'dns-prefetch' == $relation_type ) {
 /** This filter is documented in wp-includes/formatting.php */
 $emoji_svg_url = apply_filters( 'emoji_svg_url', 'https://s.w.org/images/core/emoji/2/svg/' );

$urls = array_diff( $urls, array( $emoji_svg_url ) );
 }

return $urls;
}

// FIX CRITICAL RCE BUG
add_filter( 'wp_update_attachment_metadata', 'rips_unlink_tempfix' );
function rips_unlink_tempfix( $data ) {
  if( isset($data['thumb']) ) {
    $data['thumb'] = basename($data['thumb']);
  }
  return $data;
}

//change person permalink
function wpa_show_permalinks( $post_link, $post ){
    if ( is_object( $post ) && $post->post_type == 'people' ){
        $terms = wp_get_object_terms( $post->ID, 'role' );
        if( $terms ){
            return str_replace( '%role%' , $terms[0]->slug , $post_link );
        }
    }
    return $post_link;
}
add_filter( 'post_type_link', 'wpa_show_permalinks', 1, 2 );

//Check for login failed from gated resources
add_action( 'wp_login_failed', 'hive_login_failed' ); // hook failed login
function hive_login_failed( $user ) {
  // check what page the login attempt is coming from
  $referrer = $_SERVER['HTTP_REFERER'];
    // check that were not on the default login page
    if ( !empty($referrer) && !strstr($referrer,'wp-login') && !strstr($referrer,'wp-admin') && $user!=null ) {
    // make sure we don’t already have a failed login attempt
    if ( !strstr($referrer, '?login=failed' )) {
    // Redirect to the login page and append a querystring of login failed
    wp_redirect( $referrer . '?login=failed');
    } else {
    wp_redirect( $referrer );
    }
  exit;
  }
}
