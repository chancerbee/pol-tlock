<?php
/**
 * Functions related to Advanced Custom Fields
 */

  // ACF: OPTIONS PAGE
if( function_exists('acf_add_options_page') ) {

  acf_add_options_page(array(
		'page_title' 	=> 'Mega Menu',
		'menu_title'	=> 'Mega Menu',
		'menu_slug'	=> 'mega-menus',
		'capability'	=> 'edit_posts',
		'redirect'		=> true
	));

	acf_add_options_page(array(
		'page_title' 	=> 'Miscellany',
		'menu_title'	=> 'Miscellany',
		'menu_slug' 	=> 'miscellany',
		'capability'	=> 'edit_posts',
		'redirect'		=> true
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Global Scripts',
		'menu_title'	=> 'Global Scripts',
		'parent_slug'	=> 'miscellany'
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Locations',
		'menu_title'	=> 'Locations',
		'parent_slug'	=> 'miscellany'
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Site Logos',
		'menu_title'	=> 'Site Logos',
		'parent_slug'	=> 'miscellany'
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Site Footer',
		'menu_title'	=> 'Site Footer',
		'parent_slug'	=> 'miscellany'
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Social Links',
		'menu_title'	=> 'Social Links',
		'parent_slug'	=> 'miscellany'
	));
}

  // ACF: GOOGLE MAPS API KEY
function my_acf_init() {
	global $maps_api_key;
	acf_update_setting('google_api_key', $maps_api_key);
}
add_action('acf/init', 'my_acf_init');

  // ACF: SAVE REVERSE GEOLOCATED DATA TO FIELDS
function my_acf_save_post( $post_id ) {

    // bail early if no ACF data
    if( empty($_POST['acf']) ) {
      return;
		}
		// bail if not a service enterprise or global affiliate
		$post_type = get_post_type($post_id);
		if ($post_type !== 'affiliate' && $post_type !== 'service-enterprise') {
			return;
		}

		$mapFields = ['field_5c614020d0b3b', 'field_5c63f42aa37f1'];
		foreach( $mapFields as $mapField ) {
			if ( !empty($_POST['acf'][$mapField]) ) {
				$rev_geocode = rev_geocode($_POST['acf'][$mapField]['address']);
				$_POST['acf'][$mapField]['city'] = $rev_geocode['city'];
				$_POST['acf'][$mapField]['state'] = $rev_geocode['state'];
				$_POST['acf'][$mapField]['country'] = $rev_geocode['country'];
				if ( $mapField == 'field_5c614020d0b3b') // global affiliate
					$_POST['acf']['field_5c65c24d0faa7'] = sanitize_title($rev_geocode['country']);
				if ( $mapField == 'field_5c63f42aa37f1') // service enterprise
					$_POST['acf']['field_5c65c288cf767'] = $rev_geocode['state'];
			}
		}
}

add_action('acf/save_post', 'my_acf_save_post', 9);

	// ACF SAVE HIDDEN YEAR FIELD ON PRESS RELEASES
function acf_save_press_release( $post_id ) {

    // bail early if no ACF data
    if( empty($_POST['acf']) ) {
      return;
		}

		// bail if not a press release
		$post_type = get_post_type($post_id);
		if ($post_type !== 'press_release') {
			return;
		}

		$date = $_POST['acf']['field_5c6fe4a7c77a5'];
		$year = substr($date, 0, -4);

		$_POST['acf']['field_5c7020c6281ea'] = $year;
}
add_action('acf/save_post', 'acf_save_press_release', 9);


function hive_layout_title($title, $field, $layout, $i) {
  //If only Block titles wanted, uncomment out the return $values and remove final if.
	//If only Default Layout title wanted either, remove final if or comment the whole function and filter
	$value = '';
  $heading = get_sub_field('module_heading');
	if(!empty($heading)) {
    $value = $heading;
    //return $value;
	} elseif($layout['name'] == 'text_image') {
  	$col1_type = get_sub_field('col1_type');
  	$col2_type = get_sub_field('col2_type');
  	if($col1_type == 'text') {
    	$content = get_sub_field('col1_content');
    	$dom = new DOMDocument;
      $dom->loadHTML($content);
      foreach($dom->getElementsByTagName('h2') as $node) {
        $matches[] = $dom->saveHtml($node);
      }
      $value = strip_tags($matches[0]);
        //return $value;
  	}elseif($col2_type == 'text') {
    	$content = get_sub_field('col2_content');
    	$dom = new DOMDocument;
      $dom->loadHTML($content);
      foreach($dom->getElementsByTagName('h2') as $node) {
        $matches[] = $dom->saveHtml($node);
      }
      $value = strip_tags($matches[0]);
        //return $value;
  	}
	}elseif($layout['name'] == 'wysiwyg') {
  	$content = get_sub_field('content');
  	$dom = new DOMDocument;
    $dom->loadHTML($content);
    foreach($dom->getElementsByTagName('h2') as $node) {
      $matches[] = $dom->saveHtml($node);
    }
    $value = strip_tags($matches[0]);
      //return $value;
	}
	if($value) {
  	$value = '<span class="hive-acf-block-title">'.$value.'</span>';
  	$title = $title.'  '.$value;
	}
	return $title;
}
add_filter('acf/fields/flexible_content/layout_title', 'hive_layout_title', 10, 4);
