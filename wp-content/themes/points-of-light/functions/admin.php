<?php
/**
 * Functions to improve the admin area of the site
 */

  // CUSTOM LOGIN
function hive_custom_login()
{
  echo '<link rel="stylesheet" type="text/css" href="' . get_bloginfo('stylesheet_directory') . '/assets/build/admin/login.css" />';
}
add_action('login_head', 'hive_custom_login');

  // CUSTOM LOGIN LOGO
function hive_custom_login_logo() {
  $login_logo = get_field('login_logo', 'option');
  if ($login_logo)
    print '<style type="text/css">
      #login h1 a, .login h1 a {
        background-image: url('.$login_logo['sizes']['logo'].'); max-height: 150px; max-width: 320px; width: 100%; height: 150px; background-size: contain; background-repeat: no-repeat;
      }
    </style>';
}
add_action( 'login_enqueue_scripts', 'hive_custom_login_logo' );

  // FORCE WP COLORS
add_filter('get_user_option_admin_color', 'hive_change_admin_color');
function hive_change_admin_color($result) {
    return 'hive';
}

  // CUSTOM ADMIN COLOR SCHEME
function hive_add_color_scheme() {
  wp_admin_css_color(
    'hive', //Key
    __( 'Hive', 'admin_schemes' ), //Name
    get_template_directory_uri() . '/assets/build/admin/backend.css', //Path
    array( '#2C333B', '#FFC200', '#47515D', '#F1F1F1' ), //Colours
    array( 'base' => '#FFFFFF', 'focus' => '#FFFFFF', 'current' => '#FFFFFF' ) //Icons
  );
}
add_action('admin_init', 'hive_add_color_scheme');

  // CUSTOM ADMIN STYLES
function hive_admin_styles() {
  echo '<link rel="stylesheet" type="text/css" href="' . get_bloginfo('stylesheet_directory') . '/assets/build/admin/admin.css" />';
}
add_action('admin_head', 'hive_admin_styles');

  // CUSTOM ADMIN SCRIPTS
function hive_admin_scripts($hook) {
    wp_enqueue_script('admin_scripts', get_bloginfo('stylesheet_directory') . '/assets/build/admin_scripts.js', array( 'jquery' ), '20190227', true );
}
add_action('admin_enqueue_scripts', 'hive_admin_scripts');

  // WP SEO LESS IMPORTANT!
add_filter( 'wpseo_metabox_prio', function() { return 'low';});

  // UPDATE ADMIN MENU
function aob_remove_from_admin_bar($wp_admin_bar) {
  // Remove only from front-end
  if ( ! is_admin() ) {
    // Remove items
    //$wp_admin_bar->remove_node('site-name');
    //$wp_admin_bar->remove_node('my-account');

    // Add items
  	// $args = array(
  	// 	'id'    => 'backend',
  	// 	'title' => 'Dashboard',
  	// 	'href'  => '/wp-admin/',
  	// 	'meta'  => array( 'class' => 'backend' )
  	// );
  	// $wp_admin_bar->add_node( $args );
  }

  // Remove from front-end and back-end
  $wp_admin_bar->remove_node('wp-logo');
  $wp_admin_bar->remove_node('updates');
  $wp_admin_bar->remove_node('comments');
  $wp_admin_bar->remove_node('customize');
  $wp_admin_bar->remove_node('new-content');
  $wp_admin_bar->remove_node('search');
  $wp_admin_bar->remove_node('themes');
  $wp_admin_bar->remove_node('widgets');
  $wp_admin_bar->remove_node('menus');
}
add_action('admin_bar_menu', 'aob_remove_from_admin_bar', 999);
