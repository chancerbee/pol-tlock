<?php
/**
 * Custom Hive Functions
 */

  // CUSTOM QUERY VARS
add_filter('query_vars', 'my_register_query_vars' );
function my_register_query_vars( $qvars ){
    //Add query variable to $qvars array
    $qvars[] = 'form_success';
    return $qvars;
}

  // GET DYNAMIC SIDEBAR
function get_dynamic_sidebar($index = 1)
{
  $sidebar_contents = "";
  ob_start();
  dynamic_sidebar($index);
  $sidebar_contents = ob_get_clean();
  return $sidebar_contents;
}

  // FIX WP_GET_ATTACHMENT_IMAGE FOR SVGs
function hive_fix_svg_size_attributes( $out, $id ) {
  $image_url  = wp_get_attachment_url( $id );
  $file_ext   = pathinfo( $image_url, PATHINFO_EXTENSION );

  if ( is_admin() || 'svg' !== $file_ext ) {
      return false;
  }

  return array( $image_url, null, null, false );
}
add_filter( 'image_downsize', 'hive_fix_svg_size_attributes', 10, 2 );

  // YOAST SEO EXCLUDES
// add_filter('ysacf_exclude_fields', function(){
//     return array(
//       'text_color',
//       'bg_type',
//       'bg_color',
//       'bg_color_custom',
//       'bg_video',
//       'bg_video_poster',
//       'overlay',
//       'overlay_opacity',
//       'prehead_content',
//       'subhead_content',
//       'background',
//       'hide_header',
//       'no_bottom_padding'
//     );
// });


  //SOCIAL LINKS FUNCTION
function social_links($class = '') {
  $social_opts = array('twitter','facebook','linkedin','youtube','gplus','instagram','pinterest', 'rss');
  foreach ($social_opts as $v)

    if ($url = get_field('url_'.$v, 'option'))
      $content .= '<li><a href="'.$url.'" target="_blank"><i class="icon-'.$v.'"></i></a></li>';
  if ($content != '')
    $content = '<ul class="social-links '.$class.'">'.$content.'</ul>';

    print $content;
}

  //GET RELATED POSTS BY TAG AND CATEGORY
  //used on patientco if use reference needed
/*
function get_max_related_posts( $taxonomy_1 = 'post_tag', $taxonomy_2 = 'category', $total_posts = 3 )
{
    // First, make sure we are on a single page, if not, bail
    if ( !is_single() )
        return false;

    // Sanitize and vaidate our incoming data
    if ( 'post_tag' !== $taxonomy_1 ) {
        $taxonomy_1 = filter_var( $taxonomy_1, FILTER_SANITIZE_STRING );
        if ( !taxonomy_exists( $taxonomy_1 ) )
            return false;
    }

    if ( 'category' !== $taxonomy_2 ) {
        $taxonomy_2 = filter_var( $taxonomy_2, FILTER_SANITIZE_STRING );
        if ( !taxonomy_exists( $taxonomy_2 ) )
            return false;
    }

    if ( 4 !== $total_posts ) {
        $total_posts = filter_var( $total_posts, FILTER_VALIDATE_INT );
            if ( !$total_posts )
                return false;
    }

    // Everything checks out and is sanitized, lets get the current post
    $current_post = sanitize_post( $GLOBALS['wp_the_query']->get_queried_object() );

    // Lets get the first taxonomy's terms belonging to the post
    $terms_1 = get_the_terms( $current_post, $taxonomy_1 );
    // Set a varaible to hold the post count from first query
    $count = 0;
    // Set a variable to hold the results from query 1
    $q_1   = [];

    // Make sure we have terms
    if ( $terms_1 ) {
        // Lets get the term ID's
        $term_1_ids = wp_list_pluck( $terms_1, 'term_id' );

        // Lets build the query to get related posts
        $args_1 = [
            'post_type'      => $current_post->post_type,
            'post__not_in'   => [$current_post->ID],
            'posts_per_page' => $total_posts,
            'fields'         => 'ids',
            'tax_query'      => [
                [
                    'taxonomy'         => $taxonomy_1,
                    'terms'            => $term_1_ids,
                    'include_children' => false
                ]
            ],
        ];
        $q_1 = get_posts( $args_1 );
        // Count the total amount of posts
        $q_1_count = count( $q_1 );
        // Update our counter
        $count = $q_1_count;
    }

    // We will now run the second query if $count is less than $total_posts
    if ( $count < $total_posts ) {
        $terms_2 = get_the_terms( $current_post, $taxonomy_2 );
        // Make sure we have terms
        if ( $terms_2 ) {
            // Lets get the term ID's
            $term_2_ids = wp_list_pluck( $terms_2, 'term_id' );

            // Calculate the amount of post to get
            $diff = $total_posts - $count;

            // Create an array of post ID's to exclude
            if ( $q_1 ) {
                $exclude = array_merge( [$current_post->ID], $q_1 );
            } else {
                $exclude = [$current_post->ID];
            }

            $args_2 = [
                'post_type'      => $current_post->post_type,
                'post__not_in'   => $exclude,
                'posts_per_page' => $diff,
                'fields'         => 'ids',
                'tax_query'      => [
                    [
                        'taxonomy'         => $taxonomy_2,
                        'terms'            => $term_2_ids,
                        'include_children' => false
                    ]
                ],
            ];
            $q_2 = get_posts( $args_2 );

            if ( $q_2 ) {
                // Merge the two results into one array of ID's
                $q_1 = array_merge( $q_1, $q_2 );
            }
        }
    }

    // Make sure we have an array of ID's
    if ( !$q_1 )
        return false;

    // Run our last query, and output the results
    $final_args = [
        'ignore_sticky_posts' => 1,
        'post_type'           => $current_post->post_type,
        'posts_per_page'      => count( $q_1 ),
        'post__in'            => $q_1,
        'order'               => 'DESC',
        'orderby'             => 'post__in',
        'suppress_filters'    => true,
        'no_found_rows'       => true
    ];
    $final_query = get_posts( $final_args );

    return $final_query;
}
*/

  //BODY CLASSES FOR BROWSER
  function hive_browser_body_class($classes) {
        global $is_lynx, $is_gecko, $is_IE, $is_opera, $is_NS4, $is_safari, $is_chrome, $is_iphone;
        if($is_lynx) $classes[] = 'lynx';
        elseif($is_gecko) $classes[] = 'gecko';
        elseif($is_opera) $classes[] = 'opera';
        elseif($is_NS4) $classes[] = 'ns4';
        elseif($is_safari) $classes[] = 'safari';
        elseif($is_chrome) $classes[] = 'chrome';
        elseif($is_IE) {
                $classes[] = 'ie';
                if(preg_match('/MSIE ([0-9]+)([a-zA-Z0-9.]+)/', $_SERVER['HTTP_USER_AGENT'], $browser_version))
                $classes[] = 'ie'.$browser_version[1];
        } else $classes[] = 'unknown';
        if($is_iphone) $classes[] = 'iphone';
        if ( stristr( $_SERVER['HTTP_USER_AGENT'],"mac") ) {
                 $classes[] = 'osx';
           } elseif ( stristr( $_SERVER['HTTP_USER_AGENT'],"linux") ) {
                 $classes[] = 'linux';
           } elseif ( stristr( $_SERVER['HTTP_USER_AGENT'],"windows") ) {
                 $classes[] = 'windows';
           }
        return $classes;
  
  add_filter('body_class','hive_browser_body_class');
}

  // DISABLE ALL COMMENTS

function aob_remove_admin_menus() {
  remove_menu_page( 'edit-comments.php' );
}
add_action( 'admin_menu', 'aob_remove_admin_menus' );
function aob_remove_comment_support() {
  remove_post_type_support( 'post', 'comments' );
  remove_post_type_support( 'page', 'comments' );
}
add_action('init', 'aob_remove_comment_support', 100);
function aob_admin_bar_render() {
  global $wp_admin_bar;
  $wp_admin_bar->remove_menu('comments');
}
add_action( 'wp_before_admin_bar_render', 'aob_admin_bar_render' );


  // DISABLE RESIZING FOR GIFS
function disable_upload_sizes( $sizes, $metadata ) {
    // Get filetype data.
    $filetype = wp_check_filetype($metadata['file']);

    // Check if is gif. 
    if($filetype['type'] == 'image/gif') {
        // Unset sizes if file is gif.
        $sizes = array();
    }

    // Return sizes you want to create from image (None if image is gif.)
    return $sizes;
}   
add_filter('intermediate_image_sizes_advanced', 'disable_upload_sizes', 10, 2); 

  // DONT LOAD EMOJI BS
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');

