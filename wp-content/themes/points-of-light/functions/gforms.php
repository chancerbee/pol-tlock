<?php
/**
 * Gravity Forms
 */

  //CHANGE GFORM SUBMIT INPUT TO BUTTON
/*
add_filter( 'gform_submit_button', 'form_submit_button', 10, 5 );
function form_submit_button ( $button, $form ){
  $button = str_replace( "input", "button", $button );
  $button = str_replace( "/", "", $button );
  $button .= "{$form['button']['text']}</button>";
  return $button;
}
*/

//SCROLL FORMS TO ANCHOR ON CONFIRMATION / VALIDATION
  /** this is useful when not using AJAX **/
  /*
  add_filter( 'gform_confirmation_anchor', '__return_true' );
  */

//FIRE SCRIPT WHEN GFORM RENDERS (ON PAGE LOAD, ON VALIDATION, AND ON CONFIRMATION)
  /* all forms */
  /*
  add_action('gform_register_init_scripts', 'register_custom_init_script', 10, 2);
  function register_custom_init_script($form, $field_values){
      $script =  "alert('I AM A SCRIPT');";
      GFFormDisplay::add_init_script($form['id'], 'my_custom_script', GFFormDisplay::ON_PAGE_RENDER, $script);
  }
  */

  /* by form id ("2")*/
  /*
  add_action('gform_register_init_scripts_2', 'register_custom_init_script_2', 10, 2);
  function register_custom_init_script_2($form, $field_values){
      $script =  "alert('I AM A SCRIPT');";
      GFFormDisplay::add_init_script($form['id'], 'my_custom_script', GFFormDisplay::ON_PAGE_RENDER, $script);
  }
  */

//FIRE SCRIPT ON VALIDATION ONLY
  /* by form id ("6") */
  /*
  add_filter( 'gform_validation_message_6', 'sw_gf_validation_message_6', 10, 2 );

  function sw_gf_validation_message_6( $validation_message ) {
    add_action( 'wp_footer', 'sw_gf_js_error_6' );
  }
  function sw_gf_js_error_6() { /*?>
    <script type="text/javascript">
      alert('I AM A SCRIPT');
    </script>
  <?php }
  */

  //GRAVITY FORMS, TAKE USER TO FORM ANCHOR IF NOT AJAX
add_filter("gform_confirmation_anchor", create_function("","return true;"));

/**
 * Filters the next, previous and submit buttons.
 * Replaces the forms <input> buttons with <button> while maintaining attributes from original <input>.
 *
 * @param string $button Contains the <input> tag to be filtered.
 * @param object $form Contains all the properties of the current form.
 *
 * @return string The filtered button.
 */

  // CHANGE SUBMIT INPUT TO BUTTON TYPE
add_filter( 'gform_submit_button', 'input_to_button', 10, 2 );
function input_to_button( $button, $form ) {
  $dom = new DOMDocument();
  $dom->loadHTML( $button );
  $input = $dom->getElementsByTagName( 'input' )->item(0);
  $new_button = $dom->createElement( 'button' );
  $new_button->appendChild( $dom->createTextNode( $input->getAttribute( 'value' ) ) );

  $input->removeAttribute( 'value' );
  foreach( $input->attributes as $attribute ) {
      $new_button->setAttribute( $attribute->name, $attribute->value );
  }
  $input->parentNode->replaceChild( $new_button, $input );

  return $dom->saveHtml( $new_button );
}
