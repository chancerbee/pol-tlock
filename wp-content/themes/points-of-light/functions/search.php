<?php

// Change 'get_the_excerpt' to ACF field depending on the post-type
function get_the_excerpt_extended() {

    global $post;
    $text = get_the_content();

    $switch = array(
      //'post_type' => 'custom_field_name'
      'resource' => 'main_resource_content',
      'press_release' => 'content',
      'people' => 'person_bio'
    );

    foreach($switch as $post_type => $acf_field) {
        if (get_post_type($post) == $post_type) {
            $text = get_field($acf_field);
        }
    }

    if ( '' != $text ) {
        $text = strip_shortcodes( $text );
        $text = apply_filters('the_content', $text);
        $text = str_replace(']]&gt;', ']]&gt;', $text);
        $excerpt_length = 22;
        $excerpt_more = apply_filters('excerpt_more', '...');
        $text = wp_trim_words( $text, $excerpt_length, $excerpt_more );
    }
    return $text;
}
add_filter( 'get_the_excerpt', 'get_the_excerpt_extended');

// PREVENT HOMEPAGE FROM SHOWING UP IN RESULTS
function exclude_pages_from_search($query) {
  if ( !is_admin() && $query->is_main_query() ) {
    if ($query->is_search) {
      $query->set('post__not_in', array(123));
    //   $query->unset
    }
  }
}
add_action('pre_get_posts','exclude_pages_from_search');

// EXCLUDE HISTORY ITEMS & QUOTES FROM SEARCH
function remove_pages_from_search() {
    global $wp_post_types;
    $wp_post_types['history']->exclude_from_search = true;
    $wp_post_types['quote']->exclude_from_search = true;
}
add_action('init', 'remove_pages_from_search', 11);


// EXTEND SEARCH TO INCLUDE CUSTOM FIELDS

// this is a straight copy-pasta from:
// http://adambalee.com/search-wordpress-by-custom-fields-without-a-plugin/


    /**
     * Join posts and postmeta tables
     *
     * http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_join
     */
    function cf_search_join( $join ) {
        global $wpdb;

        if ( is_search() ) {
            $join .=' LEFT JOIN '.$wpdb->postmeta. ' ON '. $wpdb->posts . '.ID = ' . $wpdb->postmeta . '.post_id ';
        }

        return $join;
    }
    add_filter('posts_join', 'cf_search_join' );

    /**
     * Modify the search query with posts_where
     *
     * http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_where
     */
    function cf_search_where( $where ) {
        global $pagenow, $wpdb;

        if ( is_search() ) {
            $where = preg_replace(
                "/\(\s*".$wpdb->posts.".post_title\s+LIKE\s*(\'[^\']+\')\s*\)/",
                "(".$wpdb->posts.".post_title LIKE $1) OR (".$wpdb->postmeta.".meta_value LIKE $1)", $where );
        }

        return $where;
    }
    add_filter( 'posts_where', 'cf_search_where' );

    /**
     * Prevent duplicates
     *
     * http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_distinct
     */
    function cf_search_distinct( $where ) {
        global $wpdb;

        if ( is_search() ) {
            return "DISTINCT";
        }

        return $where;
    }
    add_filter( 'posts_distinct', 'cf_search_distinct' );
