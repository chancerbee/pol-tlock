<?php
/**
 * Custom post types
 */

add_action( 'init', 'aob_register_post_types');

  // REGISTER POST TYPES
function aob_register_post_types() {
  $post_types = array(
//    'MACHINE_NAME' => array(
//      'single_label' => '',
//      'plural_label' => '',
//      'supports' => array('title', 'editor', 'thumbnail'),
//      'rewrite' => array('with_front' => false, 'slug' => ''),
//      'publicly_queryable' => true,
//      'has_archive' => false,
//      'menu_icon' => 'dashicons-'
//    ),
    'affiliate' => array(
      'single_label' => 'Global Affiliate',
      'plural_label' => 'Global Affiliates',
      'featured_image_label' => 'Thumbnail Image',
      'supports' => array('title', 'revisions'),
      'rewrite' => array('with_front' => false, 'slug' => 'affiliates'),
      'publicly_queryable' => true,
      'exclude_from_search' => false,
      'has_archive' => false,
      'menu_icon' => 'dashicons-admin-site'
    ),
    'service-enterprise' => array(
      'single_label' => 'Service Enterprise',
      'plural_label' => 'Service Enterprises',
      'featured_image_label' => 'Thumbnail Image',
      'supports' => array('title', 'revisions'),
      'rewrite' => array('with_front' => false, 'slug' => 'service-enterprises'),
      'publicly_queryable' => true,
      'exclude_from_search' => false,
      'has_archive' => false,
      'menu_icon' => 'dashicons-location-alt'
    ),
    'resource' => array(
      'single_label' => 'resource',
      'plural_label' => 'resources',
      'featured_image_label' => 'Thumbnail Image',
      'supports' => array('title', 'thumbnail', 'revisions'),
      'rewrite' => array('with_front' => false, 'slug' => 'resources'),
      'publicly_queryable' => true,
      'exclude_from_search' => false,
      'has_archive' => false,
      'taxonomies' => array('resource-type'),
      'menu_icon' => 'dashicons-welcome-learn-more'
    ),
    'history' => array(
      'single_label' => 'history event',
      'plural_label' => 'history events',
      'featured_image_label' => 'Thumbnail Image',
      'supports' => array('title', 'thumbnail', 'revisions'),
      'rewrite' => false,
      'publicly_queryable' => false,
      'exclude_from_search' => true,
      'has_archive' => false,
      'query_var' => false,
      'menu_icon' => 'dashicons-pressthis'
    ),
    'press_release' => array(
      'single_label' => 'press release',
      'plural_label' => 'press releases',
      'featured_image_label' => 'Thumbnail Image',
      'supports' => array('title', 'thumbnail', 'revisions'),
      'rewrite' => array('with_front' => false, 'slug' => 'press-releases'),
      'publicly_queryable' => true,
      'exclude_from_search' => false,
      'has_archive' => true,
      'menu_icon' => 'dashicons-media-document'
    ),
    'people' => array(
      'single_label' => 'person',
      'plural_label' => 'people',
      'featured_image_label' => 'Thumbnail Image',
      'supports' => array('title', 'thumbnail', 'revisions'),
      'rewrite' => array('with_front' => false, 'slug' => 'people/%role%'),
      'publicly_queryable' => true,
      'exclude_from_search' => false,
      'has_archive' => true,
      'menu_icon' => 'dashicons-businessman'
      ),
    'quote' => array(
      'single_label' => 'quote',
      'plural_label' => 'quotes',
      'supports' => array('title', 'revisions'),
      'rewrite' => false,
      'publicly_queryable' => false,
      'exclude_from_search' => true,
      'has_archive' => false,
      'menu_icon' => 'dashicons-format-quote'
    )

  );
  foreach ($post_types as $machine_name => $post_type) {
    $labels = array(
      'name' => _x(ucwords($post_type['plural_label']), 'post type general name'),
      'singular_name' => _x(ucwords($post_type['single_label']), 'post type singular name'),
      'add_new' => _x('Add New ', ucwords($post_type['single_label'])),
      'add_new_item' => __('Add New '.ucwords($post_type['single_label'])),
      'edit_item' => __('Edit '.ucwords($post_type['single_label'])),
      'new_item' => __('New '.$post_type['single_label']),
      'view_item' => __('View '.ucwords($post_type['single_label'])),
      'all_items' => __('All '.ucwords($post_type['plural_label'])),
      'search_items' => __('Search '.$post_type['plural_label']),
      'not_found' =>  __('No '.$post_type['plural_label'].' found'),
      'not_found_in_trash' => __('No '.$post_type['plural_label'].' found in Trash'),
      'parent_item_colon' => ''
    );
    register_post_type( $machine_name,
      array(
        'labels' => $labels,
        'public' => true,
        'hierarchical' => false,
        'supports' => $post_type['supports'],
        'has_archive' => $post_type['has_archive'],
        'rewrite' => $post_type['rewrite'],
        'menu_icon' => $post_type['menu_icon'],
      )
    );
  }
}

  // UPDATE "POSTS" LABELS TO "BLOG POSTS"
function aob_change_post_label() {
  global $menu;
  global $submenu;
  $menu[5][0] = 'Blog Posts';
  $submenu['edit.php'][5][0] = 'All Blog Posts';
  $submenu['edit.php'][10][0] = 'Add New';
}
function aob_change_post_object() {
  global $wp_post_types;
  $labels = &$wp_post_types['post']->labels;
  $labels->name = 'Blog Posts';
  $labels->singular_name = 'Blog Post';
  $labels->add_new = 'Add New Blog Post';
  $labels->add_new_item = 'Add New Blog Post';
  $labels->edit_item = 'Edit Blog Post';
  $labels->new_item = 'New blog post';
  $labels->view_item = 'View Blog Post';
  $labels->search_items = 'Search blog posts';
  $labels->not_found = 'No blog posts found';
  $labels->not_found_in_trash = 'No blog posts in Trash';
  $labels->all_items = 'All Blog Posts';
  $labels->menu_name = 'Blog Posts';
  $labels->name_admin_bar = 'Blog Post';
}
add_action( 'admin_menu', 'aob_change_post_label' );
add_action( 'init', 'aob_change_post_object' );
