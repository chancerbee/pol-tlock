<?php
/**
 * Functions to plug into TinyMCE
 */

  // ADD EDITOR CSS TO WP-ADMIN
add_editor_style('/assets/build/admin/editor.css');

  // TINYMCE REMOVE UNWANTED FORMATS
function tiny_mce_remove_unused_formats($init) {
   $init['block_formats'] = 'Paragraph=p;Heading 2=h2;Heading 3=h3;Blockquote=blockquote';
   return $init;
}
add_filter('tiny_mce_before_init', 'tiny_mce_remove_unused_formats' );

  // TINYMCE ADD CUSTOM FORMATS
function custom_formats($init) {
  $style_formats = array(
    array(
      'title' => 'Paragraph',
      'block' => 'p'
    ),
    array(
      'title' => 'Heading 2',
      'block' => 'h2'
    ),
    array(
      'title' => 'Heading 3',
      'block' => 'h3'
    ),
  );
  $init['style_formats'] = json_encode( $style_formats );

  return $init;
}
add_filter('tiny_mce_before_init', 'custom_formats' );

	// CUSTOM WYSIWYG TYPES
add_filter( 'acf/fields/wysiwyg/toolbars' , 'my_toolbars'  );
function my_toolbars( $toolbars )
{
	$toolbars['B+I' ] = array();
  $toolbars['B+I' ][1] = array('bold' , 'italic');

  $toolbars['Colors' ] = array();
  $toolbars['Colors' ][1] = array('forecolor');

  $toolbars['B+I, Links' ] = array();
	$toolbars['B+I, Links' ][1] = array('bold' , 'italic', 'link', 'unlink');

	$toolbars['B+I, Lists, Links' ] = array();
	$toolbars['B+I, Lists, Links' ][1] = array('bold', 'italic', 'bullist', 'numlist', 'link', 'unlink');

	$toolbars['B+I, Lists, Links, Buttons, More' ] = array();
	$toolbars['B+I, Lists, Links, Buttons, More' ][1] = array('styleselect', '|', 'bold', 'italic', 'bullist', 'numlist', 'link', 'unlink', 'fb_button_button_key', 'hr', 'alignleft', 'aligncenter', 'alignright');

	return $toolbars;

  // For your referencing pleasure:
  // $toolbars['Full'][1] = array('bold', 'italic', 'underline', 'bullist', 'numlist', 'alignleft', 'aligncenter', 'alignright', 'alignjustify', 'link', 'unlink', 'hr', 'spellchecker', 'wp_more', 'wp_adv' );
  // $toolbars['Full'][2] = array('styleselect', 'formatselect', 'fontselect', 'fontsizeselect', 'forecolor', 'pastetext', 'removeformat', 'charmap', 'outdent', 'indent', 'undo', 'redo', 'wp_help' );
}

  // CUSTOM TINYMCE COLORS
function hive_change_tinymce_colors( $init ) {
    $default_colours = '
        "8CBB3F", "Green",
        "4280C2", "Blue",
        ';
    $custom_colours = '';
    $init['textcolor_map'] = '['.$default_colours.']';
    $init['textcolor_rows'] = 6; // expand colour grid to 6 rows
    return $init;
}
add_filter('tiny_mce_before_init', 'hive_change_tinymce_colors');

  // CUSTOM TINYMCE FONT SIZES
// function mycustom_change_tinymce_fontsize( $init ) {
//     //$init['fontsize_formats'] = "10px 11px 12px 13px 14px 15px 16px 17px 18px 19px 20px 21px 22px 23px 24px 25px 26px 27px 28px 29px 30px 32px";
//     return $init;
// }
// add_filter('tiny_mce_before_init', 'mycustom_change_tinymce_fontsize', 12); //-->somewhat important '12', at least more than 10 to keep your changes
