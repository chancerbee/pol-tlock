<?php
/**
 * Widgets
 */

function hive_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Blog Sidebar', 'hive' ),
		'id'            => 'sidebar',
		'description'   => esc_html__( 'Add widgets here.', 'hive' ),
		'before_widget' => '<aside id="%1$s" class="sidebar-block widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<header><h4>',
		'after_title'   => '</h4></header>',
	) );
}
add_action( 'widgets_init', 'hive_widgets_init' );
