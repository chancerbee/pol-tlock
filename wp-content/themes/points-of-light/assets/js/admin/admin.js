(function ($) {

  $(document).on('ready', function () {

    // Custom conditional show/hide for ACF Anchor Nav elements
    const $hiddenCheckbox = $('#acf-field_5c6d56a3fdc7a'),
          $navToggle = $hiddenCheckbox.next('.acf-switch'),
          $titleField = $('[data-name="anchor_title"]'),
          $idField = $('[data-name="anchor_id"]');

    function setVisibility() {
      const toggleState = $navToggle.hasClass('-on');

      toggleState ? $titleField.show() : $titleField.hide();
      toggleState ? $idField.show() : $idField.hide();
    }

    function hideFields() {
      $titleField.hide();
      $idField.hide();
    }

    if ( $navToggle.length ) {

      //this page has a nav toggle, do the conditional rendering
      setVisibility();
      $hiddenCheckbox.on('change', () => {
        setTimeout(() => setVisibility(), 10);
      });

    } else {

      // this page doesn't have a nav toggle, hide the navigation fields
      hideFields();
    }

  });

}) (jQuery);
