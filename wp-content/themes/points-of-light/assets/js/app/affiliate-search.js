function affiliateSearch() {
  (function ($) {

    function init() {
      if ($('.js-affiliate-card').length) {

        const $cards = $('.js-affiliate-card');

        $cards.on('click', function () {

          const $card = $(this),
            $close = $card.find('.js-close-card'),
            $region = $card.find('.js-region') || null,
            $cityState = $card.find('.js-city-state'),
            $address = $card.find('.js-address'),
            $links = $card.find('.js-links');
          // use these later to make nicer.

          $cards.removeClass('active');
          $card.addClass('active');

          $close.on('click', function (e) {
            e.stopPropagation();
            $card.removeClass('active');
          });

        });
      }
    }

    init();
    $.fn.almFilterComplete = () => init();

  })(jQuery)
}
