// global helpers
// Find Matched Selects
function findMatchedSelects(dropdown) {
  let matchedSelects = [];
  const selectIdentifiers = dropdown.getAttribute("data-linked").split(",");

  selectIdentifiers.forEach(identifier => {
    matchedSelects.push(
      document.querySelector(`[data-meta-key="${identifier}"] select`)
    );
  });
  return matchedSelects;
}

// Refresh Dropdown State (used in popstate, see bottom of dropdownify-selects.js)
function refreshDropdownState(dropdown, selects) {
  if (!window.location.search) {
    dropdown.setAttribute("data-value", "#");
    dropdown.querySelector(".title").textContent = dropdown.querySelector(
      ".dropdown-item"
    ).textContent;
  } else {
    const activeSelect = selects.filter(select => select.value !== "#");
    if (activeSelect.length) {
      dropdown.setAttribute("data-value", activeSelect[0].value);
      dropdown.querySelector(".title").textContent = document.querySelector(
        `option[value="${activeSelect[0].value}"]`
      ).textContent;
    } else {
      dropdown.setAttribute("data-value", "#");
      dropdown.querySelector(".title").textContent = dropdown.querySelector(
        ".dropdown-item"
      ).textContent;
    }
  }
}

function comboSelectLink() {
  // Bail if this is IE 11 and below
  const version = detectIE();
  if (version !== false && version <= 12) return;

  let comboDropdowns = Array.from(document.querySelectorAll(".combo-dropdown"));

  // Link our dropdown-items to our select-options
  function linkDropdownToSelects(dropdown, selects) {
    selects.forEach(select => {
      select.closest(".alm-filter").classList.add("hidden");
    });
    selects[0].closest(".alm-filter").before(dropdown);
    dropdown.classList.remove("hidden");

    const MutationObserver =
      window.MutationObserver ||
      window.WebKitMutationObserver ||
      window.MozMutationObserver;

    const observer = new MutationObserver(mutations => {
      mutations.forEach(mutation => {
        if (mutation.type == "attributes") {
          const value = dropdown.getAttribute("data-value");

          if (value == "#") {
            selects.forEach(select => {
              if (select.value !== value) {
                select.value = value;
                processAlmFilters(select);
              }
            });
          } else {
            const matchedSelect = document.querySelector(
              `option[value="${value}"]`
            ).parentElement;

            if (matchedSelect.value !== value) {
              selects.forEach(select => (select.value = "#"));
              matchedSelect.value = value;
              processAlmFilters(matchedSelect);
            }
          }
        }
      });
    });
    observer.observe(dropdown, { attributes: true });
  }

  // Handlers
  comboDropdowns.forEach(dropdown => {
    linkDropdownToSelects(dropdown, findMatchedSelects(dropdown));
  });
}
