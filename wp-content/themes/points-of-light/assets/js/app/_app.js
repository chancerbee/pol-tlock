(function($) {
  $(document).on("ready", function() {
    const pageId = parseInt(document.body.className.split("page-id-")[1]);

    // Enqueues
    affiliateSearch();
    comboSelectLink();
    dropdownify();
    dropdowns();
    floatLabels();
    filters();
    hiveFunctions();
    IEObjectFitFix();
    jsNiceCols();
    mapModule();
    verticalTabber();
    pageId == 762 ? volunteerSearch() : volunteerSearchModule();
    pageId == 861 ? gMapInit(pageId) : null;
    pageId == 953 ? gMapInit(pageId) : null;
  });
})(jQuery);

// Public Utility Functions
function below(bp) {
  if (window.matchMedia(`(max-width: ${bp}px)`).matches) {
    return true;
  } else {
    return false;
  }
}
function replaceHistory(qs) {
  if (history.replaceState) {
    const currentUrl = window.location.href;
    let targetUrl;
    if (window.location.search) {
      targetUrl = currentUrl.replace(window.location.search, `?${qs}`);
    } else {
      targetUrl = `${currentUrl}?${qs}`;
    }
    history.replaceState({ path: targetUrl }, "", targetUrl);
  }
}
function pushToHistory(qs) {
  if (history.pushState) {
    const currentUrl = window.location.href;
    let targetUrl;
    if (window.location.search) {
      targetUrl = currentUrl.replace(window.location.search, `?${qs}`);
    } else {
      targetUrl = `${currentUrl}?${qs}`;
    }
    history.pushState({ path: targetUrl }, "", targetUrl);
  }
}

// IE Object-fit Fix
function IEObjectFitFix() {
  (function($) {
    const version = detectIE();
    if (version !== false && version <= 12 && $(".ie-object-fit").length) {
      $(".ie-object-fit").each(function() {
        var $container = $(this),
          imgUrl = $container.find("img").prop("src");
        if (imgUrl) {
          $container
            .css("backgroundImage", "url(" + imgUrl + ")")
            .addClass("ie-object-fit-active");
        }
      });
    }
  })(jQuery);
}
