function floatLabels() {
  (function ($) {

    var floatlabels = new FloatLabels('form:not(.no-float-label)', {
      style: 1
    });

    // Extend fl-active state to phone number fields
    $('.ginput_container_phone input').on('focus', function () {
      $(this).closest('.fl-wrap').addClass('fl-is-active');
    });
    $('.ginput_container_phone input').on('blur', function () {
      if (!$(this).val().length > 0) {
        $(this).closest('.fl-wrap').removeClass('fl-is-active');
      }
    });

    // Reload float labels after ajax submit
    jQuery(document).bind('gform_post_render', function () {
      var floatlabels = new FloatLabels('form:not(.no-float-label)', {
        style: 1,
        prioritize: 'placeholder'
      });
    });

    // Add asterisks to required fields
    function addAsterisks($el) {
      var placeHolder = $el.find('input').attr('placeholder')
        || $el.find('textarea').attr('placeholder')
        || $el.find('select option:first').text()
        || $el.find('.gf_placeholder').text();

      placeHolder += ' *';

      $el.find('input').attr('placeholder', placeHolder).attr('data-placeholder', placeHolder);
      $el.find('textarea').attr('placeholder', placeHolder).attr('data-placeholder', placeHolder);
      $el.find('select option:first').text(placeHolder);
      $el.find('.gf_placeholder').text(placeHolder);
      $el.find('label').text(placeHolder);
    }

    $('.gfield_contains_required').each(function () {
      let type;
      const $inputContainer = $(this).find('.ginput_container');

      type = $inputContainer.hasClass('ginput_complex') ? 'complex' : type;
      type = $inputContainer.hasClass('ginput_container_radio') ? 'radio' : type;
      type = $inputContainer.hasClass('ginput_container_checkbox') ? 'checkbox' : type;

      switch (type) {
        // radio and checkbox do not get replacements with asterisks
        case 'radio':
          break;
        case 'checkbox':
          break;

        // complex fields (like address) need each child looped and replaced individually
        case 'complex':
          $inputContainer.find('span').each(function () {
            if (!$(this).hasClass('address_line_2')) {
              addAsterisks($(this));
            }
          });
          break;

        // default is to replace the placeholder with an asterisk at the end
        default:
          addAsterisks($(this));
      }
    });
  })(jQuery);
}
