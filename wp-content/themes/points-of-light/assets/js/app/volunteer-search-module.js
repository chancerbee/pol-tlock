function volunteerSearchModule() {
  (function($) {
    const loadSearchPage = function() {
      const locParam =
        $("#algolia-location").val() == "#" ? "" : $("#algolia-location").val();
      const tagParam =
        $("#algolia-tag").val() == "#" ? "" : $("#algolia-tag").val();

      // get the params
      const params = {
        // vars
        q: encodeURIComponent($("#algolia-query").val()),
        aLLVIP: locParam,
        "tR[0]": encodeURIComponent(tagParam)
      };

      // compose the url
      const qs = "?" + decodeURIComponent($.param(params)),
        url = $("#algolia-fetch").attr("href") + qs;

      // send user to search page
      window.location.href = url;
    };

    // Handlers
    $("#algolia-search").on("click", () => loadSearchPage());
    $(".search-inputs").on("keyup", e =>
      e.keyCode == 13 ? loadSearchPage() : null
    );
  })(jQuery);
}
