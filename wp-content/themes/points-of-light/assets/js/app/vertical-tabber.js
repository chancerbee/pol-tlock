function verticalTabber() {
  (function($) {
    if ($(".js-vertical-tabber").length) {
      $(".js-vertical-tabber").each(function() {
        const $tabber = $(this),
          $indicator = $(this).find($(".indicator"));

        // define functions
        function init() {
          $tabber
            .find($(".js-tab-content.js-pre-init"))
            .removeClass("js-pre-init")
            .css("display", "none");
        }
        function updateState(i) {
          $tabber
            .find($('[data-state="active"]'))
            .attr("data-state", "inactive");
          $tabber.find($(`[data-tab=${i}]`)).attr("data-state", "active");
        }
        function repositionIndicator(i) {
          const $targetControl = $tabber.find(
              $(`.js-tab-control[data-tab=${i}]`)
            ),
            setTop = Math.abs(
              $tabber.offset().top - $targetControl.offset().top
            );

          $indicator.css("top", setTop);
        }
        function animateState($this, $next) {
          $this.fadeOut(150, function() {
            $next.fadeIn(150);
          });
        }

        // init
        setTimeout(() => init(), 10);

        // handle clicks
        $tabber.find(".js-tab-control").on("click", function() {
          const targetIndex = $(this).attr("data-tab"),
            currentIndex = $tabber
              .find($('.js-tab-control[data-state="active"]'))
              .attr("data-tab"),
            $targetContent = $tabber.find(
              $(`.js-tab-content[data-tab=${targetIndex}]`)
            ),
            $currentContent = $tabber.find(
              $(`.js-tab-content[data-tab=${currentIndex}]`)
            );

          updateState(targetIndex);
          repositionIndicator(targetIndex);
          animateState($currentContent, $targetContent);
        });
      });
    }

    // clicking tab-content inside an accordion should not propagate
    $(".tab-content").on("click", function(e) {
      e.stopPropagation();
    });
  })(jQuery);
}
