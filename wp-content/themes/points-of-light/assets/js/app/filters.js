function filters() {
  (function($) {
    const $resultSet = $(".alm-listing");

    // on complete (success)
    $.fn.almComplete = function(alm) {
      globalCompleteActions(alm);
      blogCompleteActions();
      IEObjectFitFix();
    };

    // on complete (empty)
    $.fn.almEmpty = function() {
      $resultSet.html(
        `<p class="alm-empty">Sorry, no results matched your search. Try broadening your filters.</p>`
      );
    };

    function globalCompleteActions(alm) {
      // removing the alm-reveal container doesn't work with shortcode when filters are enabled
      // (bug in the plugin)
      // this band-aid moves children of the reveal into the main container, and deletes the reveal
      const currentPageNum = alm.page + alm.start_page;
      const $currentReveal = $(".alm-reveal[data-page=" + currentPageNum + "]");
      $currentReveal.children().appendTo($resultSet);
      $currentReveal.remove();
    }

    function blogCompleteActions() {
      // trigger our huge DPOL posts
      const $firstDPOL = $resultSet.find(".dpol.card").first();
      $firstDPOL.addClass("huge").prependTo($resultSet);
    }
  })(jQuery);
}
