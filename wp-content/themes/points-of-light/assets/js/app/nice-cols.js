function jsNiceCols() {
  (function($) {
    if ($(".js-nice-cols").length) {
      $(".js-nice-cols").each(function() {
        // first kill maxHeight, in case niceCols has already been called
        $(this).css("maxHeight", "none");

        // perform the math
        const half = Math.round($(this).children().length / 2),
          $break = $(this).find(`:nth-child(${half + 1})`),
          height = $break.offset().top - $(this).offset().top;

        // set the maxHeight
        $(this).css("maxHeight", height);

        // do it differently for IE
        const version = detectIE();
        if (version !== false && version <= 12) {
          $(this).css("maxHeight", height + 1);
          $(this).css("height", height + 1);
        }
      });
    }
  })(jQuery);
}
