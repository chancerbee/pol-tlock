/*
 * SEE DOCUMENTATION FOR DROPDOWN MARKUP PATTERN AT BOTTOM OF FILE
 */

function dropdowns() {
  (function($) {
    const $dropdowns = $(".dropdown");

    $dropdowns.each(function() {
      const $dropdown = $(this),
        $items = $(this).find(".dropdown-item"),
        $button = $(this).find(".dropdown-button");

      // Clear dropdowns when clicking outside
      $(document).on("click", () => $dropdown.removeClass("is-expanded"));

      // Clicking button expands/collapses dropdown
      $button.on("click", function(event) {
        event.stopPropagation();
        $(".dropdown")
          .not($dropdown)
          .removeClass("is-expanded");
        $dropdown.toggleClass("is-expanded");
        jsNiceCols();
      });

      // Pressing down or up opens the dropdown (mimic <select> behavior)
      $button.keydown(event => {
        if (event.keyCode === 38 || event.keyCode === 40) {
          event.preventDefault(); // prevent page scroll
          $dropdown.addClass("is-expanded");
          $dropdown
            .find(".dropdown-item:visible")
            .first()
            .focus();
        }
      });

      $items.each(function() {
        const $item = $(this);

        // Clicking dropdown item selects that option and closes dropdown
        $item.on("click", function(e) {
          e.stopPropagation();

          const value = $item.attr("data-value"),
            title = $item.text();

          $dropdown.attr("data-value", value).removeClass("is-expanded");
          $button.find(".title").text(title);
        });

        // Clicking a heading in a combined dropdown will not propagate
        $(".dropdown-heading").on("click", e => e.stopPropagation());

        // Navigating Items mimics <select> -> <option> behavior
        $item.keydown(event => {
          // press down
          if (event.keyCode === 40) {
            event.preventDefault(); // stops page scrolling
            const $next = $item.nextAll(".dropdown-item")[0];
            $next && $next.focus();
          }
          // press up
          if (event.keyCode === 38) {
            event.preventDefault(); // stops page scrolling
            const $prev = $item.prevAll(".dropdown-item")[0];
            $prev && $prev.focus();
          }
          // press esc
          if (event.keyCode === 27) {
            $dropdown.removeClass("is-expanded");
            $button.focus();
          }
          // press tab
          if (event.keyCode === 9) {
            event.preventDefault(); // tab shouldn't advance
          }
        });
      });
    });
  })(jQuery);
}

/*

The markup should be structured as follows:

  * Give dropdown items href="" or leave off entirely if you are not linking to a page,
    do not use href="#".

  * The first dropdown item should match the dropdown button


<div data-value="#" class="dropdown">
  <a href="javscript:void(0)" class="dropdown-button button navy" data-value="#">
    <span class="title">Dropdown Title</span>
    <i class="icon-arrow-down"></i>
  </a>
  <div class="dropdown-items">
    <a class="dropdown-item" href="" data-value="#">
      Dropdown Title
    </a>
    <a class="dropdown-item" href="/your-page" data-value="option-one">
      Option One
    </a>
    <a class="dropdown-item" href="http://www.this-website.com/your-page" data-value="option-two">
      Option Two
    </a>
  </div>
</div>

*/
