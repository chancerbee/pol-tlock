function volunteerSearch() {
  (function($) {
    // cache DOM
    const $result = $("#algolia-result"),
      $moreButton = $("#algolia-more"),
      $footer = $(".volunteer-search-results-footer"),
      $queryInput = $("#algolia-query"),
      $locInput = $("#algolia-location"),
      $tagInput = $("#algolia-tag");

    // setup
    const params = { hitsPerPage: 5 },
      algolia = algoliasearch("D3FBYGGL4F", "78874879a175ef6e2e94264be4081171"),
      algoliaHelper = algoliasearchHelper(algolia, "new_opportunities", params);

    algoliaHelper.setQueryParameter("attributesToRetrieve", [
      "title",
      "description",
      "_tags",
      "durations",
      "locs",
      "detailURL"
    ]);

    // define the search function
    const performQuery = function(history = "replace") {
      // set loading text
      $result.text("Searching...");

      // get vars from form inputs
      const query = $queryInput.val(),
        useLocationViaIP = $locInput.val() == "#" ? "" : $locInput.val(),
        tag = $tagInput.val();

      // set location param
      if (useLocationViaIP == 1) {
        algoliaHelper.setQueryParameter("aroundLatLngViaIP", true);
      } else {
        algoliaHelper.setQueryParameter("aroundLatLngViaIP", false);
      }

      // clear existing tags and set new tag param
      algoliaHelper.clearTags();
      if (tag !== "#") {
        algoliaHelper.addTag(tag);
      }

      // set query and search
      algoliaHelper.setQuery(query).search();

      // update history with this query encoded in url
      const qs = algoliasearchHelper.url.getQueryStringFromState(
        algoliaHelper.getState()
      );

      history == "replace" && replaceHistory(qs);
      history == "push" && pushToHistory(qs);
    };

    // define the url-based search function
    const performUrlQuery = function(qs) {
      // get state from url
      const state = algoliasearchHelper.url.getStateFromQueryString(qs);

      // original state
      if (!state.index) {
        $footer.removeClass("visible");
        $result.html("<p>Try a new search!</p>");
        $queryInput.val("");
        $locInput
          .prev(".dropdown")
          .find(".title")
          .text("Filter By Location");
        $tagInput
          .prev(".dropdown")
          .find(".title")
          .text("Filter By Issue");
        return;
      }

      // update inputs' text to match state
      // query
      $queryInput.val(state.query);
      // tag
      if (state.tagRefinements) {
        const $matchingTagOpt = $tagInput.find(
          `option[value="${state.tagRefinements[0]}"]`
        );
        $tagInput
          .prev(".dropdown")
          .find(".title")
          .text($matchingTagOpt.text());
      } else {
        $tagInput
          .prev(".dropdown")
          .find(".title")
          .text("Filter By Issue");
      }
      // loc
      if (state.aroundLatLngViaIP == true) {
        $locInput
          .prev(".dropdown")
          .find(".title")
          .text("Near Me");
      } else {
        $locInput
          .prev(".dropdown")
          .find(".title")
          .text("Everywhere");
      }

      // set loading text
      $result.text("Searching...");

      // set state and search
      algoliaHelper.setState(state).search();
    };

    // define the result function
    const handleResults = function(data) {
      // clear out any rendered results if this result is first page
      if (algoliaHelper.getPage() == 0) {
        $result.html("");
      }

      // render results or empty notice
      if (data.hits.length > 0) {
        data.hits.forEach(hit => {
          // format tag data
          const maybeTags = hit._tags.length
            ? `<p class="event-meta">${hit._tags[0]}</p>`
            : "";

          // format date data
          let maybeDate = "";
          if (hit.durations.length) {
            const dateFormat = {
              weekday: "short",
              day: "numeric",
              month: "short",
              year: "numeric"
            };
            let startDate, endDate;
            if (hit.durations[0].startDate) {
              startDate = new Date(
                hit.durations[0].startDate
              ).toLocaleDateString("en-US", dateFormat);
            }
            if (hit.durations[0].endDate) {
              endDate = new Date(hit.durations[0].endDate).toLocaleDateString(
                "en-US",
                dateFormat
              );
            } else if (hit.durations[0].openEnded) {
              endDate = "Open-Ended";
            }
            maybeDate = startDate ? `<h4 class="result-date">${startDate}` : "";
            maybeDate += endDate && startDate ? ` - ${endDate}</h4>` : "</h4>";

            if (!startDate && hit.durations[0].openEnded == "Yes") {
              maybeDate = '<h4 class="result-date">Timeframe Open-Ended</h4>';
            }
          }

          // formate time data
          let maybeTime = "";
          if (hit.durations.length) {
            const timeFormat = {
              hour: "numeric",
              minute: "numeric",
              hour12: true
            };
            let startTime, endTime;
            if (
              hit.durations[0].startTime &&
              hit.durations[0].startTime !== "00:00:00"
            ) {
              startTime = new Date(
                `${hit.durations[0].startDate}T${hit.durations[0].startTime}`
              ).toLocaleTimeString("en-US", timeFormat);
            }
            if (
              hit.durations[0].endTime &&
              hit.durations[0].endTime !== "00:00:00"
            ) {
              endTime = new Date(
                `${hit.durations[0].startDate}T${hit.durations[0].endTime}`
              ).toLocaleTimeString("en-US", timeFormat);
            }
            maybeTime = startTime ? `<h6 class="result-time">${startTime}` : "";
            maybeTime += endTime && startTime ? ` - ${endTime}</h6>` : "</h6>";
          }

          // Format Location Data
          let city, region, country;
          let maybeLocation = [];
          city = hit.locs[0].city ? hit.locs[0].city : null;
          region = hit.locs[0].region ? hit.locs[0].region : null;
          country = hit.locs[0].country ? hit.locs[0].country : null;
          const regionIsJustNumericLol = /^\d+$/.test(region);

          city && maybeLocation.push(city);
          region && !regionIsJustNumericLol && maybeLocation.push(region);
          country !== "US" && maybeLocation.push(country);

          if (maybeLocation.length) {
            maybeLocation = `
              <p class="result-location">
                ${maybeLocation.join(", ")}
              </p>
            `;
          } else {
            maybeLocation = "";
          }

          // here's a random case I found that needed a better format.
          // generally we wouldn't want to deal with bad data,
          // but this was the first result of an 'everything' search :(
          if (
            !city &&
            !region &&
            country &&
            hit.locs[0].has_been_geocoded == 1
          ) {
            maybeLocation = `
              <p class="result-location">
                ${hit.locs[0].geocode_string}
              </p>
            `;
          }

          $result.append(`
            <div class="module-type-search-result">
              <div class="columns-two">
                <div class="main-col">
                  <h2 class="main-heading">${hit.title}</h2>
                  <div class="main-text wysiwyg">
                    <p>${hit.description}</p>
                  </div>
                  <a class="button" href="${hit.detailURL}" target="_blank">
                    Learn More
                  </a>
                </div>
                <div class="left-col col-type-event">
                  <div class="event-content">
                    ${maybeDate}
                    ${maybeTime}
                    ${maybeLocation}
                    ${maybeTags}
                  </div>
                </div>
              </div>
            </div>
          `);
        });
        $footer.addClass("visible");
      } else {
        if (algoliaHelper.getPage() == 0) {
          // no results on first page: render empty notice
          $result.html(
            "<p>Sorry, we couldn't find any opportunities that match your search.</p><p>Please try a different set of filters.</p>"
          );
          $footer.removeClass("visible");
        } else {
          // no results on subsequent page: kill load more button and render finished notice
          $footer.removeClass("visible");
          $result.append("<p>All matching results shown.</p>");
        }
      }
    };

    // define the load more function
    const loadMore = function() {
      algoliaHelper.setPage(algoliaHelper.getPage() + 1).search();
      $moreButton.blur();
    };

    // Handlers
    $("#algolia-search").on("click", () => performQuery("push"));
    $(".algolia-search-inputs *").on("keyup", e =>
      e.keyCode == 13 ? performQuery("push") : null
    );
    algoliaHelper.on("result", data => handleResults(data));
    $moreButton.on("click", () => loadMore());

    if (window.location.search.length) {
      performQuery("replace");
    }

    $(window).on("popstate", () => {
      const searchDataString = window.location.search.slice(1);
      performUrlQuery(searchDataString.replace("%20&%20", "%20%26%20"));
    });
  })(jQuery);
}
