function hiveFunctions() {
  (function($) {
    // AUTOGROW TEXTAREAS
    $("textarea").autogrow();

    // EMPTY HREF LINKS
    /* must let tlock know if you want to edit this line, it has implications on dropdown tabbing */
    $('a[href=""], a[href="#"]').click(e => e.preventDefault());

    // SMOOTH SCROLLING ANCHOR LINKS
    $('a[href*="#"]:not([href="#"])').click(function(e) {
      if (
        location.pathname.replace(/^\//, "") ==
          this.pathname.replace(/^\//, "") &&
        location.hostname == this.hostname
      ) {
        e.preventDefault();
        var target = $(this.hash);
        target = target.length
          ? target
          : $("[name=" + this.hash.slice(1) + "]");
        if (target.length) {
          $("html, body").animate(
            {
              scrollTop: target.offset().top
            },
            1000
          );
          return false;
        }
      }
    });

    // ADD CLASS TO HEADER ON SCROLL
    $(window).scroll(function() {
      var scroll = $(window).scrollTop();
      if (scroll === 0) {
        $(".site-header").removeClass("scrolled");
      } else {
        $(".site-header").addClass("scrolled");
      }
    });
    $(window).scroll();

    // MOBILE NAV
    $(".mobile-menu-toggle").on("click", function() {
      $(this)
        .add(".mobile-menu")
        .toggleClass("is-open")
        .blur();
    });

    // VIDEO LINKS IN MODAL
    $('a[href^="https://www.youtube.com"]').each(function() {
      $(this).addClass("video-link");
    });
    $('a[href^="https://youtube.com"]').each(function() {
      $(this).addClass("video-link");
    });
    $('a[href^="https://www.vimeo.com"]').each(function() {
      $(this).addClass("video-link");
    });
    $('a[href^="https://vimeo.com"]').each(function() {
      $(this).addClass("video-link");
    });
    if ($("a.video-link").length) {
      $("a.video-link").magnificPopup({
        type: "iframe",
        mainClass: "mfp-fade",
        removalDelay: 160,
        preloader: false,
        fixedContentPos: false
      });
    }

    // SLICK SLIDERS
    if ($(".home-slider").length) {
      $(".home-slider").slick({
        dots: true,
        autoplay: true,
        autoplaySpeed: 6000,
        pauseOnHover: false,
        pauseOnFocus: false,
        infinite: true,
        speed: 500,
        fade: true,
        cssEase: "linear",
        arrows: false
      });
    }
    if ($(".slider-icon").length) {
      $(".slider-icon").slick({
        infinite: true,
        speed: 500,
        fade: false,
        cssEase: "linear",
        arrows: true,
        slidesToShow: 6,
        slidesToScroll: 6,
        prevArrow: $(".slider-icon-prev-arrow"),
        nextArrow: $(".slider-icon-next-arrow"),
        responsive: [
          {
            breakpoint: 1040,
            settings: {
              slidesToShow: 4,
              slidesToScroll: 4
            }
          },
          {
            breakpoint: 750,
            settings: {
              slidesToShow: 3,
              slidesToScroll: 3
            }
          },
          {
            breakpoint: 600,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
              rows: 2
            }
          }
        ]
      });
    }
    if ($(".grid-icon").length) {
      $(".grid-icon").slick({
        infinite: true,
        speed: 500,
        fade: false,
        cssEase: "linear",
        arrows: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        rows: 2,
        prevArrow: $(".grid-icon-prev-arrow"),
        nextArrow: $(".grid-icon-next-arrow"),
        mobileFirst: true,
        responsive: [
          {
            breakpoint: 600,
            settings: "unslick"
          }
        ]
      });
    }

    if ($(".stories-slider").length) {
      $(".stories-slider").slick({
        infinite: false,
        speed: 500,
        fade: false,
        cssEase: "linear",
        arrows: false,
        dots: true,
        appendDots: $(".story-dots")
      });
    }

    function setSlickStoryHeight() {
      if ($(".stories-slider").length) {
        var storyHeight = -1;
        $(".stories-slider .story-slide").each(function() {
          storyHeight =
            storyHeight > $(this).height() ? storyHeight : $(this).height();
        });

        $(".stories-slider .story-slide").each(function() {
          $(this).height(storyHeight);
        });

        return storyHeight;
      }
    }
    /* Set Heights For Stories so they dont grow */
    $(window).load(function() {
      setSlickStoryHeight();
    });
    $(window).on("resize orientationchange", function(e) {
      var resizeTimer;
      clearTimeout(resizeTimer);
      resizeTimer = setTimeout(function() {
        setSlickStoryHeight();
      }, 500);
    });

    // SINGLE PERSON PHOTO
    if ($("body").hasClass("single-people")) {
      var $portrait = $("img.portrait");
      $portrait.length &&
        $(".wysiwyg p")
          .first()
          .after($portrait);
    }

    // SINGLE PRESS RELEASE FEATURE TEXT
    if ($("body").hasClass("single-press_release")) {
      var $featureText = $(".feature-text");
      $featureText.length &&
        $(".wysiwyg p")
          .first()
          .after($featureText);
    }

    // SINGLE BLOG POST TOPIC LINKS
    if ($("body").hasClass("single-post")) {
      var $blogTopics = $(".blog-topics");
      $blogTopics.length &&
        $(".wysiwyg p")
          .first()
          .after($blogTopics);
    }

    // VIDEO EMBED PLAY FUNCTION
    $(".play-button").click(function(e) {
      e.preventDefault();
      const parent = $(this).parents(".overlay");
      parent.hide("fast");
      const iframe = parent.siblings("iframe");
      let src = iframe.attr("src");
      const autoplay = "autoplay=1";
      if (src.indexOf("?") === -1) {
        src = src + "?" + autoplay;
      } else {
        src = src + "&" + autoplay;
      }
      iframe.attr("src", src);
    });

    // INSTAGRAM FEED
    if ($(".instagram-grid").length) {
      let template;
      const version = detectIE();
      if (version !== false && version <= 12) {
        template = `
          <li>
            <a href="{{link}}" target="_blank" class="">
              <div class="photo ie-object-fit ie-object-fit-active" style="background-image:url({{image}})">
                <img src="{{image}}">
              </div>
            </a>
          </li>
        `;
      } else {
        template = `
          <li>
            <a href="{{link}}" target="_blank" class="">
              <div class="photo ie-object-fit">
                <img src="{{image}}">
              </div>
            </a>
          </li>
        `;
      }
      const target = document.querySelector(".instagram-grid");
      var account = "179679881";
      var client = "77a64203a7094ce3a256ea1ec1390911";
      var token = "179679881.77a6420.7b105d513f33484a90858f737a8a7fe1";
      var amount = 4;
      var feed = new Instafeed({
        get: "user",
        userId: account,
        clientId: client,
        accessToken: token,
        sortBy: "most-recent",
        resolution: "standard_resolution",
        limit: amount,
        target: target,
        template: template
      });
      feed.run();
    }

    //Donation Form Mask
    $('.donation-form [type="text"]').maskMoney();

    //THIS SOLUTION ONLY ALLOWS WHOLE NUMBERS

    // var $form = $(".donation-form");
    // var $input = $form.find("input");

    // $input.on("keyup", function (event) {


    //   // When user select text in the document, also abort.
    //   var selection = window.getSelection().toString();
    //   if (selection !== '') {
    //     return;
    //   }

    //   // When the arrow keys are pressed, abort.
    //   if ($.inArray(event.keyCode, [38, 40, 37, 39]) !== -1) {
    //     return;
    //   }

    //   var $this = $(this);

    //   // Get the value.
    //   var input = $this.val();

    //   var input = input.replace(/[\D\s\._\-]+/g, "");
    //   console.log(input);
    //   input = input ? parseInt(input, 10) : 0;

    //   $this.val(function () {
    //     return (input === 0) ? "" : input.toLocaleString("en-US");
    //   });
    // });

    //remove commas from number on submit
    $(".donation-form").on("submit", function() {
      var donationInput = $(".donation-form .donation-number");
      donationInput.val(donationInput.val().replace(/,/g, ""));
    });

    // Don't use 'card-grid-advanced' (css grid) on IE
    const version = detectIE();
    if (version !== false && version <= 12 && $(".card-grid-advanced").length) {
      $(".card-grid-advanced")
        .removeClass("card-grid-advanced")
        .addClass("card-grid");
    }
  })(jQuery);
}
