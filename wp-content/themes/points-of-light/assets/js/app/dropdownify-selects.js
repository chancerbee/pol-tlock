/*
 * SEE DOCUMENTATION FOR <select> MARKUP PATTERN AT BOTTOM OF FILE
 */

// Public helpers
// Process ALM filters right away
function processAlmFilters(select) {
  const parentFilters = select.closest(".alm-filters"),
    button = parentFilters.querySelector("button");
  button.click();
}

// Process Algolia filters right away
function processAlgoliaFilters() {
  document.getElementById("algolia-search").click();
}

// Refresh Dropdown State (used in popstate)
function refreshDropdownFromSelect(select) {
  const dropdown = select.previousSibling;
  if (!window.location.search) {
    dropdown.setAttribute("data-value", "#");
    dropdown.querySelector(".title").textContent = dropdown.querySelector(
      ".dropdown-item"
    ).textContent;
  } else {
    dropdown.setAttribute("data-value", select.value);
    dropdown.querySelector(".title").textContent = document.querySelector(
      `option[value="${select.value}"]`
    ).textContent;
  }
}

function dropdownify() {
  // Bail if this is IE 11 and below
  const version = detectIE();
  if (version !== false && version <= 12) return;

  // Get all selects on page
  let selects = Array.from(document.querySelectorAll("select"));

  // Process Exceptions
  function processExceptions() {
    let exceptions = [];

    // Name, Class, ID, and ALM parent meta-key Exceptions
    const exceptionIdentifiers = [
      "region",
      "country",
      // "state",
      "for_nonprofits",
      "for_individuals",
      "for_companies"
    ];

    exceptionIdentifiers.forEach(identifier => {
      exceptions.push(
        document.querySelector(`[data-meta-key="${identifier}"] select`),
        document.querySelector(`select[name="${identifier}"]`),
        document.querySelector(`select[class="${identifier}"]`),
        document.querySelector(`select[id="${identifier}"]`)
      );
    });

    selects = selects.filter(exception => {
      return exceptions.indexOf(exception) < 0;
    });
    return selects;
  }

  // Create our dropdowns
  function createDropdownFromSelect(select) {
    const title = select.options[select.selectedIndex].innerHTML,
      titleVal = select.options[select.selectedIndex].value,
      options = Array.from(select.options);

    let listMarkup = "";
    options.forEach(option => {
      const href = option.getAttribute("href") || "#";

      listMarkup += `
        <a
          class="dropdown-item"
          href="${href}"
          data-value="${option.value}">
            ${option.innerHTML}
        </a>
      `;
    });

    const dropdownInnerMarkup = `
      <a
        class="dropdown-button button navy"
        data-value="${titleVal}"
        href="#"
      >
          <span class="title">${title}</span>
          <i class="icon-arrow-down"></i>
      </a>
      <div class="dropdown-items">
        ${listMarkup}
      </div>
    `;

    let dropdown = document.createElement("div");
    dropdown.setAttribute("data-value", select.value);
    dropdown.classList.add("dropdown");
    dropdown.innerHTML = dropdownInnerMarkup;
    select.before(dropdown);

    // call linking function
    linkDropdownToSelect(dropdown, select);
  }

  // Link our dropdown-items to our select-options
  function linkDropdownToSelect(dropdown, select) {
    const MutationObserver =
      window.MutationObserver ||
      window.WebKitMutationObserver ||
      window.MozMutationObserver;

    const observer = new MutationObserver(mutations => {
      mutations.forEach(mutation => {
        if (mutation.type == "attributes") {
          const value = dropdown.getAttribute("data-value");

          if (select.value !== value) {
            select.value = value;
            select.parentElement.classList.contains("alm-filter--select") &&
              processAlmFilters(select);
            document.body.classList.contains("page-id-762") &&
              processAlgoliaFilters();
          }
        }
      });
    });
    observer.observe(dropdown, { attributes: true });
  }

  // Handlers
  selects = processExceptions();
  selects.forEach(select => {
    createDropdownFromSelect(select);
    select.classList.add("hidden", "dropdownified");
  });

  window.onpopstate = () => {
    selects.forEach(select => {
      refreshDropdownFromSelect(select);
    });

    // also handle dropdowns here because we can only have one popstate event
    // refreshDropdownState() was made Public to allow this
    let comboDropdowns = Array.from(
      document.querySelectorAll(".combo-dropdown")
    );
    comboDropdowns.forEach(dropdown => {
      refreshDropdownState(dropdown, findMatchedSelects(dropdown));
    });
  };
}

/*

To use dropdownified select fields, the markup should be structured as follows:

  * Optionally use class of 'hidden' or inline display:none to prevent FOUT

  * Pass an href attribute to <option> if you want to link to a page

<select class="hidden">
  <option value="#">The Title Option</option>
  <option value="option-one">Option One</option>
  <option value="option-two" href="/your-page">Option Two</option>
  <option value="option-three" href="http://www.this-site.com/your-page">Option Three</option>
</select>

*/
