<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package Hive
 */

get_header(); ?>

<script type="text/javascript">
  (function ($) {
    $(document).ready(function () {
        // Handler for .ready() called.
        window.setTimeout(function () {
            location.href = "<?= get_home_url() ?>";
        }, 3000);
    });
  })(jQuery);
</script>

<?php
get_footer();
