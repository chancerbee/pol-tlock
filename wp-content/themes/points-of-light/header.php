<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<!-- FAVICONS -->
<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
<link rel="manifest" href="/site.webmanifest">
<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="theme-color" content="#ffffff">

<!-- FONTS -->
<link rel="stylesheet" href="https://s3.amazonaws.com/icomoon.io/34692/PointsofLight/style.css?vj6a6g">
<link href="https://fonts.googleapis.com/css?family=Barlow+Condensed:500,600,600i,700|Barlow:400,400i,500,700|Zilla+Slab:300,400,500,600" rel="stylesheet">

<?php wp_head(); ?>

<?php
  $header_scripts = get_field('header_scripts', 'option');
  if ($header_scripts) print $header_scripts;
?>

<?php if ( get_the_ID() == 762 ): ?>
  <!-- Algolia Search Client & 'Helper' Libraries -->
  <script src="https://cdn.jsdelivr.net/algoliasearch/3/algoliasearch.min.js"></script>
  <script src="https://cdn.jsdelivr.net/algoliasearch.helper/2/algoliasearch.helper.min.js"></script>
<?php endif; ?>

</head>

<body <?php body_class(); ?>>

<div id="page">

  <?php include_once('templates/chrome/site-header.php'); ?>

	<main id="content" class="site-content">
    <?php
      include_once('templates/chrome/page-header.php');
    ?>

    <?php if (get_query_var('form_success')): ?>
      <div class="success_message">
        <div class="container">
          <p><strong>Your submission was successful.</strong> Thank you for your interest!</p>
        </div>
      </div>
    <?php endif; ?>
