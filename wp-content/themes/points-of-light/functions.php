<?php
/**
 * HIVE!!!
 */

/********* SETUP *********/

require get_template_directory() . '/functions/setup.php';
require get_template_directory() . '/functions/widgets.php';

/********* IMAGE SIZES *********/

function hive_image_sizes() {
  if ( function_exists( 'add_image_size' ) ) {
    add_image_size( 'bg', 1600, 9999, false );
    add_image_size( 'logo', 300, 150 );
    add_image_size( 'team-grid', 190, 190, true );
    add_image_size( 'single-team', 245, 368, false);
    add_image_size( 'card-top', 250, 110, true );
    add_image_size( 'card-top-huge', 520, 300, true );
    add_image_size( 'spotlight-person', 200, 200, true);

    add_image_size( 'megamenu-link', 180, 180, true );
    add_image_size( 'megamenu-blog', 120, 120, true );
    add_image_size( 'megamenu-event', 160, 180, true );
    add_image_size( 'megamenu-resource', 206, 9999, false );

    add_image_size( 'one-third-container', 340, 9999, false );
    add_image_size( 'two-third-container', 700, 9999, false );
    add_image_size( 'full-container', 1060, 9999, false );
  }
}
add_action( 'after_setup_theme', 'hive_image_sizes' );

/********* SCRIPTS & STYLES *********/

function hive_scripts() {
	$url = $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];

	// dev environments use un-minified assets
	if ( strstr( $url, '.aob' )						 ||
			 strstr( $url, '.local' )					 ||
			 strstr( $url, '.armyofbees.net' ) ||
       strstr( $url, 'pantheonsite.io' )
		//  ( strstr( $url, '.pantheonsite.io') && substr( $url, 0, 4 ) === "dev-" )
	) {
    $root_url = get_template_directory_uri() . '/assets/build/';
    $min = '';

	// production and test environments use minified assets
	} else {
    $root_url = get_template_directory_uri() . '/assets/dist/';
    $min = '.min';
  }

  // STYLES & SCRIPTS
  wp_enqueue_style ( 'styles',          $root_url . 'style'          . $min . '.css',  array(),                             '20190319' );
  wp_enqueue_script( 'vendor_scripts',  $root_url . 'vendor_scripts' . $min . '.js',   array( 'jquery' ),                   '20190305', true );
  wp_enqueue_script( 'app_scripts',     $root_url . 'app_scripts'    . $min . '.js',   array( 'jquery', 'vendor_scripts' ), '20190319.1', true );

}
add_action( 'wp_enqueue_scripts', 'hive_scripts' );

/********* BODY CLASSES *********/

function hive_body_classes( $classes ) {
	// $classes[] = 'your-class-here';
	return $classes;
}
add_filter( 'body_class', 'hive_body_classes' );

/********* AOB FUNCTION LIBRARY *********/

require get_template_directory() . '/functions/acf.php';
require get_template_directory() . '/functions/admin.php';
require get_template_directory() . '/functions/alm-filters.php';
require get_template_directory() . '/functions/api-keys.php';
require get_template_directory() . '/functions/blog.php';
require get_template_directory() . '/functions/hive.php';
require get_template_directory() . '/functions/misc.php';
require get_template_directory() . '/functions/search.php';
require get_template_directory() . '/functions/wysiwyg.php';
require get_template_directory() . '/functions/gforms.php';
// require get_template_directory() . '/functions/videos.php';

/********* CUSTOM POST TYPES & TAXONOMIES *********/

require get_template_directory() . '/functions/custom-types.php';
require get_template_directory() . '/functions/custom-tax.php';
