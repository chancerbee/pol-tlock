    <?php if(get_field('include_instagram_feed'))
      include_once('templates/includes/instagram-feed.php');

    ?>

    </main>

    <?php include_once('templates/chrome/site-footer.php'); ?>

</div><!-- #page -->

<?php wp_footer(); ?>

<?php
  $footer_scripts = get_field('footer_scripts', 'option');
  if ($footer_scripts) print $footer_scripts;
?>

</body>
</html>
