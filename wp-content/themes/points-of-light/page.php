<?php
/****** PAGE ******/

get_header();

  while ( have_posts() ) : the_post();

    $id = get_the_ID();

    // ****** CUSTOM PAGES ******
    // Use format "id" => "template file"
    // e.g. $custom_pages = array('373' => 'templates/page-plans.php');
    $custom_pages = array(
      '124' => 'templates/pages/blog.php',
      '511' => 'templates/pages/history.php',
      '861' => 'templates/pages/global-affiliate-network.php',
      '953' => 'templates/pages/service-enterprise-network.php',
      '969' => 'templates/pages/gated-resources.php',
      '965' => 'templates/pages/open-resources.php'
    );

    if (array_key_exists($id,$custom_pages)):
      // Load template file if custom
      include($custom_pages[$id]);
    endif;

    // ****** FLEX CONTENT BLOCKS ******
    include('templates/includes/flex-blocks.php');

  endwhile;

?>

<?php

get_footer();
