msgid ""
msgstr ""
"Project-Id-Version: Ajax Load More Cache\n"
"POT-Creation-Date: 2018-04-16 12:58-0500\n"
"PO-Revision-Date: 2018-04-16 12:58-0500\n"
"Last-Translator: Darren Cooney <darren@connekthq.com>\n"
"Language-Team: \n"
"Language: en_CA\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.7.3\n"
"X-Poedit-Basepath: .\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Poedit-KeywordsList: __;_e\n"
"X-Poedit-SearchPath-0: ..\n"

#: ../admin/includes/cta/writeable.php:2
msgid "Cache Status"
msgstr ""

#: ../admin/includes/cta/writeable.php:9
msgid "Nice"
msgstr ""

#: ../admin/includes/cta/writeable.php:10
msgid "Read/Write access is enabled within the cache directory"
msgstr ""

#: ../admin/includes/cta/writeable.php:16
msgid "Access Denied"
msgstr ""

#: ../admin/includes/cta/writeable.php:19
msgid ""
"You must enable read and write access for the Ajax Load More cache directory "
"to save cache data"
msgstr ""

#: ../admin/includes/cta/writeable.php:20
msgid ""
"Please contact your hosting provider or site administrator for more "
"information."
msgstr ""

#: ../admin/views/cache.php:15 ../ajax-load-more-cache.php:522
msgid "Cache deleted successfully"
msgstr ""

#: ../admin/views/cache.php:21
msgid "Are you sure you want to delete this cache?"
msgstr ""

#: ../admin/views/cache.php:25
msgid "Cache"
msgstr ""

#: ../admin/views/cache.php:26
msgid "Manage your active Ajax Load More Cache"
msgstr ""

#: ../admin/views/cache.php:36
msgid "Cache successfully deleted."
msgstr ""

#: ../admin/views/cache.php:37
msgid "Got it"
msgstr ""

#: ../admin/views/cache.php:41
msgid "Cache Dashboard"
msgstr ""

#: ../admin/views/cache.php:50
msgid "Collapse All"
msgstr ""

#: ../admin/views/cache.php:50
msgid "Expand All"
msgstr ""

#: ../admin/views/cache.php:55
msgid "Search by cache ID or cache URL "
msgstr ""

#: ../admin/views/cache.php:83
msgid "Delete this cache"
msgstr ""

#: ../admin/views/cache.php:83
msgid "Delete"
msgstr ""

#: ../admin/views/cache.php:94
msgid "Cached URL"
msgstr ""

#: ../admin/views/cache.php:95
msgid "Date Created"
msgstr ""

#: ../admin/views/cache.php:102
msgid "Cached files in this directory"
msgstr ""

#: ../admin/views/cache.php:128
msgid "Ajax Load More cache is currently empty."
msgstr ""

#: ../admin/views/cache.php:143
msgid "Statistics"
msgstr ""

#: ../admin/views/cache.php:160
msgid "Page(s)"
msgstr ""

#: ../admin/views/cache.php:162
msgid "Cached File(s)"
msgstr ""

#: ../admin/views/cache.php:165
msgid "Are you sure you want to delete the entire Ajax Load More cache?"
msgstr ""

#: ../admin/views/cache.php:167 ../ajax-load-more-cache.php:601
msgid "Delete Cache"
msgstr ""

#: ../ajax-load-more-cache.php:33
msgid ""
"You must install and activate <a href=\"https://wordpress.org/plugins/ajax-"
"load-more/\">Ajax Load More</a> before installing the Ajax Load More Cache "
"add-on."
msgstr ""

#: ../ajax-load-more-cache.php:42 ../ajax-load-more-cache.php:230
#: ../ajax-load-more-cache.php:235
msgid ""
"Error creating cache directory. Please contact your hosting administrator."
msgstr ""

#: ../ajax-load-more-cache.php:47
msgid ""
"Error accessing uploads/alm-cache directory. This add-on is required to read/"
"write to your server. Please contact your hosting administrator."
msgstr ""

#: ../ajax-load-more-cache.php:170
msgid "Cache Settings"
msgstr ""

#: ../ajax-load-more-cache.php:176
msgid "Published Posts"
msgstr ""

#: ../ajax-load-more-cache.php:183
msgid "Known Users"
msgstr ""

#: ../ajax-load-more-cache.php:238
msgid "Unable to create text file!"
msgstr ""

#: ../ajax-load-more-cache.php:246
msgid ""
"Unable to write to text file. Please contact your hosting administrator."
msgstr ""

#: ../ajax-load-more-cache.php:292 ../ajax-load-more-cache.php:294
#: ../ajax-load-more-cache.php:329 ../ajax-load-more-cache.php:363
msgid "Error opening file - please contact your hosting administrator."
msgstr ""

#: ../ajax-load-more-cache.php:297 ../ajax-load-more-cache.php:331
#: ../ajax-load-more-cache.php:365
msgid "Error writing to cache file. Please contact your hosting administrator."
msgstr ""

#: ../ajax-load-more-cache.php:466
msgid "Error - Unable to verify nonce."
msgstr ""

#: ../ajax-load-more-cache.php:592
msgid "Ajax Load More Cache"
msgstr ""

#: ../ajax-load-more-cache.php:617
msgid ""
"Customize your installation of the <a href=\"http://connekthq.com/plugins/"
"ajax-load-more/cache/\">Cache</a> add-on."
msgstr ""

#: ../ajax-load-more-cache.php:639
msgid "Delete cache when new posts are published."
msgstr ""

#: ../ajax-load-more-cache.php:640
msgid ""
"Cache will be fully cleared whenever a post, page or Custom Post Type is "
"published or updated."
msgstr ""

#: ../ajax-load-more-cache.php:665
msgid "Don't cache files for logged in users."
msgstr ""

#: ../ajax-load-more-cache.php:666
msgid ""
"Logged in users will retrieve content directly from the database and will "
"not view any cached content."
msgstr ""

#~ msgid "404 Error"
#~ msgstr "Erreur 404 "

#~ msgid "Sorry, the page you have requested cannot be found."
#~ msgstr "Désolé, la page que vous demandez est introuvable."

#~ msgid ""
#~ "Be sure to check your spelling. If all else fails, you can go back to the "
#~ "page you came from, return to the <a href=\"/\">homepage</a>, or try "
#~ "searching."
#~ msgstr ""
#~ "Veuillez vérifier l'orthographe. Si tout échoue, retournez à la page "
#~ "précédente, retournez à la <a href=\"/\">page d'accueil</a>, ou essayez "
#~ "une recherche."

#~ msgid "Connect with Willow"
#~ msgstr "Connectez-vous avec Willow"

#~ msgid ""
#~ "Sign up to stay in touch and receive Willow news, updates and event "
#~ "information."
#~ msgstr ""
#~ "Inscrivez-vous pour rester en contact avec nous et recevoir des nouvelles "
#~ "de Willow (Disponibles en anglais seulement)"

#~ msgid "all our funders"
#~ msgstr "tous nos donateurs"

#~ msgid "call our helpline"
#~ msgstr "Appelez notre ligne de soutien"

#~ msgid "share"
#~ msgstr "Partager"

#~ msgid "Share via Email"
#~ msgstr "Partager par courriel"

#~ msgid "Your search returned <strong>%s</strong> result%s"
#~ msgstr "Votre recherche a généré <strong>%s</strong>  résultat%s "

#~ msgid "search"
#~ msgstr "Recherche"

#~ msgid "Showing"
#~ msgstr "Montrant"

#~ msgid "result(s) in"
#~ msgstr "résultat(s) dans"

#~ msgid "For more information please contact:"
#~ msgstr "Pour de plus amples renseignements, veuillez contacter : "

#~ msgid "filter support groups"
#~ msgstr "Filtrer les groupes de soutien"

#~ msgid "select a province and city to find a support group"
#~ msgstr ""
#~ "Sélectionnez votre province et votre ville pour trouver un groupe de "
#~ "soutien"

#~ msgid "Filter"
#~ msgstr "Filtrer"
