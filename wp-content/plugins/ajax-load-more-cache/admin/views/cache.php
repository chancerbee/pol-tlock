<?php
if(isset($_GET['action'])) {
	
	// Delete cache action
   if($_GET['action'] === 'delete'){ 
	   
	   $result = ALMCache::alm_delete_full_cache();

   	// Redirect user to ?action=deleted to prevent double form submit
   	echo'<script> window.location="admin.php?page=ajax-load-more-cache&action=deleted"; </script> ';
	}
	
	// Cache deleted action
	if($_GET['action'] === 'deleted'){ 
   	$result = __('Cache deleted successfully', 'ajax-load-more-cache');
   }
   
   unset($_GET);   
}
?>
<div class="admin ajax-load-more" id="alm-cache" data-msg="<?php _e('Are you sure you want to delete this cache?', 'ajax-load-more-cache'); ?>">
	<div class="wrap main-cnkt-wrap">
      <header class="header-wrap">
         <h1>
            <?php echo ALM_TITLE; ?>: <strong><?php _e('Cache', 'ajax-load-more-cache'); ?></strong>
            <em><?php _e('Manage your active Ajax Load More Cache', 'ajax-load-more-cache'); ?></em>
         </h1>
      </header>
      
      <div class="ajax-load-more-inner-wrapper">
         
   		<div class="cnkt-main">      		
      		<?php
		      if(isset($result)) {
		         echo '<div class="cache-cleared"><i class="fa fa-check-square-o"></i> ';
               echo __('Cache successfully deleted.', 'ajax-load-more-cache');
               echo '<span class="remove"><a href="admin.php?page=ajax-load-more-cache">' . __('Got it', 'ajax-load-more-cache') . '</a></span>';
               echo '</div>';
		      }
		      ?>
		      <h3><?php _e('Cache Dashboard', 'ajax-load-more-cache'); ?></h3>
		      <p>Below you will find the listing of your entire Ajax Load More cache. This listing is grouped by the <strong>Cache ID</strong> assigned when your <a href="admin.php?page=ajax-load-more-shortcode-builder">Shortcode</a> was created.</p>
		      <p><a href="admin.php?page=ajax-load-more"><strong>Cache Settings</strong></a> &nbsp;|&nbsp;  <a href="https://connekthq.com/plugins/ajax-load-more/add-ons/cache/" target="_blank"><strong>Get Help</strong></a></p>

		      <div class="spacer"></div>
   
   		   <div class="group no-shadow">
   		      <?php
   			   if(!isset($result)) { ?>
   		      <span class="toggle-all"><span class="inner-wrap"><em class="collapse"><?php _e('Collapse All', 'ajax-load-more-cache'); ?></em><em class="expand"><?php _e('Expand All', 'ajax-load-more-cache'); ?></em></span></span>
   		      <?php } ?>
   			   <div class="row no-brd">
      			      
   			      <div class="alm-cache-search-wrap" style="margin-top: 3px;">
      			      <input type="text" name="alm-cache-search" id="alm-cache-search" value="" placeholder="<?php _e('Search by cache ID or cache URL ', 'ajax-load-more-cache'); ?>">
      			      <i class="fa fa-search"></i>
   			      </div>
   
   		         <?php   
   		         // Loop thru our cache directories
                  $path = ALMCache::alm_get_cache_path();
                  $directoy_total = 0;
   
                  $staticDirectories = array();
   
                  if(is_dir($path)){ // confirm directory exists
   
                     // Loop the directories and store values in array for sorting
                     foreach (new DirectoryIterator($path) as $file) {
                        if ($file->isDot())
                        	continue;
   
                        if ($file->isDir())
      	               	$staticDirectories[] = $file->getFilename();
                     }
   
                     asort($staticDirectories); // Sort the directory array
                     foreach($staticDirectories as $directory){ // Loop thru our sorted directories and store files in array for sorting
   
                        $directoy_total++;
   
                        echo '<div class="alm-dir-listing">';
                           echo '<h3 class="heading dir dir-title" title="'. $path . '/' . $directory .'">'. $directory . ' <a href="javascript:void(0);" class="delete" data-id="'. $directory .'" title="'.__('Delete this cache', 'ajax-load-more-cache').'">'. __('Delete', 'ajax-load-more-cache') .'</a></h3>';
   
                           echo '<div class="expand-wrap">';
   
                           $sub_path = $path . $directory;
   
                           // Get value of _info.txt
                           $url = file_get_contents(ALMCache::alm_get_cache_path() .'/'. $directory.'/_info.txt');
                           if($url){
      	                     echo '<ul class="cache-details">';
                           	$info = unserialize($url);
      	                     echo '<li title="'.__('Cached URL', 'ajax-load-more-cache').'"><i class="fa fa-globe"></i> <a href="' . $info['url'] . '" target="_blank">' . $info['url'] . '</a></li>';
      	                     echo '<li title="'.__('Date Created', 'ajax-load-more-cache').'"><i class="fa fa-clock-o"></i> ' . $info['created'] . '</li>';
      								echo '</ul>';
                           }
   
                           // Display cached pages
                           echo '<div class="cache-page-wrap">';
                           echo '<ul>';
      	                  echo '<div class="cache-page-title">'.__('Cached files in this directory', 'ajax-load-more-cache').':</div>';
   
      	                  $staticFiles = array();
                           foreach (new DirectoryIterator($sub_path) as $sub_file) { // each file
                              if ($sub_file->isDot() || $sub_file->getFilename() === '_info.txt')
                              	continue;
   
                              if ($sub_file->isFile())
                              	$staticFiles[] = $sub_file->getFilename();
                           }
   
                           asort($staticFiles); // Sort the file array
                           foreach($staticFiles as $static){ // Loop the sorted array to display static html
                              echo '<li class="file"><i class="fa fa-file-text-o"></i> <a href="'. ALMCache::alm_get_cache_url() . $directory . '/'. $static .'" target="_blank">'. $static . '</a></li>';
                           }
   
                           echo '</div>';
                           echo '</ul>';
                           echo '</div>';
                        echo '</div>';
                     }
                  }
   
                  // Is empty?
                  if($directoy_total == 0){
                     echo '<div class="dir-empty">';
                     echo __('Ajax Load More cache is currently empty.', 'ajax-load-more-cache');
                     echo '<style>.toggle-all, .alm-cache-search-wrap{display:none !important;}</style>';
                     echo '</div>';
                  }
   
                  ?>
   			   </div>
   		   </div>
   
   	   </div>
   
   	   <aside class="cnkt-sidebar">	   	   	   	   
	   	   <div id="cnkt-sticky-wrapper">
		   	   <div id="cnkt-sticky">		   	     
		   	      <div class="cta">
		   	         <h3><?php _e('Statistics', 'ajax-load-more-cache'); ?></h3>
			   	         <div class="cta-inner">
			   	         <?php
			   
			                  $dircount = 0;
			                  $filecount = 0;
			                  $directories = glob($path . "*");
			   
			                  foreach($directories as $directory){
			                     $dir = glob($directory . "*");
			                     $val = count(glob($dir[0] .'/*.html'));
			                     if($val > 0){
			                        $dircount++;
			                        $filecount = $filecount + $val;
			                     }
			                  }
			               ?>
			   	         <p class="cache-stats"><span class="stat"><?php echo $dircount; ?></span><?php _e('Page(s)', 'ajax-load-more-cache'); ?></p>
			   	         <div class="spacer"></div>	  
			   	         <p class="cache-stats last"><span class="stat"><?php echo $filecount; ?></span><?php _e('Cached File(s)', 'ajax-load-more-cache'); ?></p> 	         
		   	         </div>
		   	         <div class="major-publishing-actions">
			   	         <form id="delete-all-cache" name="delete-all-cache" action="admin.php" method="GET" data-msg="<?php _e('Are you sure you want to delete the entire Ajax Load More cache?', 'ajax-load-more-cache'); ?>">
			   		         <input type="hidden" value="ajax-load-more-cache" name="page">
			   		         <button type="submit" class="button-primary" name="action" value="delete"><?php _e('Delete Cache', 'ajax-load-more-cache'); ?></button>
			   		      </form>
			         	</div>   
		   	      </div>
		   	   	<?php include_once( ALM_CACHE_ADMIN_PATH . 'admin/includes/cta/writeable.php'); ?>		   	   	
		   	   </div>
	   	   </div>
				<div class="clear"></div>
   	   </aside>
   	   
   	   <div class="clear"></div>
      </div>
	</div>
</div>
