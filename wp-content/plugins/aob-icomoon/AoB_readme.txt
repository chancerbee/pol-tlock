Changes from stock module:

/acf-icomoon-select/fields/acf-icomoon_select-v5.php
• commented all field settings related to json path (lines 111 - 139)
• replaced $path variable definitions with hardcoded path (lines 176, 483)

/acf-icomoon-select/assets/js/input.js
• added 'parent' variable to all functions and updated all selectors to use it