( function() {  
    tinymce.PluginManager.add( 'aob_blockquote', function( editor, url ) {

        // Add a button that opens a window
        editor.addButton( 'aob_blockquote', {
            icon: 'aob-blockquote',
            width: 600,
            height: 400,
            tooltip: 'Insert Blockquote',
            onclick: function() {
                // Open window
                editor.windowManager.open( {
                    title: 'Add Blockquote',
                    body: [
                      { type: 'textbox', name: 'quote', label: 'Quote', autofocus: true, multiline: true, minHeight: 150 },
                      { type: 'textbox', name: 'author', label: 'Byline', size: 40 }
                    ],
                    onsubmit: function( e ) {
                        // Insert content when the window form is submitted
                        var content = '<blockquote>';
                        content += '<p><i class="icon-quote-open"></i>'+e.data.quote+'<i class="icon-quote-close"></i></p>';
                        if (e.data.author.trim() != '') {
                          content += '<p class="cite">'+e.data.author+'</p>';
                        }
                        content += '</blockquote>';
                        editor.insertContent( content );
                    }

                } );
            }

        } );

    } );

} )();
