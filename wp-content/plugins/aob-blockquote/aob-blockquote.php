<?php
/**
 * Plugin Name: AOB Blockquote
 * Description: Adds a custom blockquote to TinyMCE
 * Plugin URI:
 * Version:     0.0.1
 * Author:      Army of Bees
 * Author URI:  http://www.armyofbees.com
 * License:     GPLv2
 * License URI: ./assets/license.txt
 * Text Domain:
 * Domain Path: /languages
 * Network:     false
 */

add_action( 'admin_head', 'fb_add_tinymce_aobblockquote' );
function fb_add_tinymce_aobblockquote() {
    global $typenow;

    /*
    // only on Post Type: post and page
    if( ! in_array( $typenow, array( 'post', 'page', 'story', 'guide' ) ) )
        return ; */

    add_filter( 'mce_external_plugins', 'fb_add_tinymce_plugin_aobblockquote' );
    // Add to line 1 form WP TinyMCE
    add_filter( 'mce_buttons', 'fb_add_tinymce_button_aobblockquote' );
}

// inlcude the js for tinymce
function fb_add_tinymce_plugin_aobblockquote( $plugin_array ) {

    $plugin_array['aob_blockquote'] = plugins_url( '/plugin.js', __FILE__ );
    // Print all plugin js path
    // var_dump( $plugin_array );
    return $plugin_array;
}

// Add the button key for address via JS
function fb_add_tinymce_button_aobblockquote( $buttons ) {

    array_push( $buttons, 'aob_blockquote' );
    // Print all buttons
    // var_dump( $buttons );
    return $buttons;
}
