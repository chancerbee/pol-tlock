( function() {
    tinymce.PluginManager.add( 'fb_button', function( editor, url ) {

        // Add a button that opens a window
        editor.addButton( 'fb_button_button_key', {
            icon: 'button',
            width: 500,
            height: 300,
            tooltip: 'Insert Button',
            onclick: function() {
                // Open window
                editor.windowManager.open( {
                    title: 'Add Button',
                    body: [
                      { type: 'textbox', name: 'button_text', label: 'Button Text', autofocus: true, size: 40 },
                      { type: 'textbox', name: 'href', label: 'Link URL', size: 40 },
                      { type: 'listbox',
                          name: 'size',
                          label: 'Button Size',
                          'values': [
                              {text: 'Small', value: 'small'},
                              {text: 'Normal', value: 'reg'},
                              {text: 'Large', value: 'large'}
                          ]
                      },
                      { type: 'listbox',
                          name: 'color',
                          label: 'Button Color',
                          'values': [
                              {text: 'Green', value: 'green'},
                              {text: 'Blue', value: 'blue'},
                              {text: 'Navy', value: 'navy'}
                          ]
                      }
                    ],
                    onsubmit: function( e ) {
                        // Insert content when the window form is submitted
                        var content = '<a class="button ' + e.data.color + ' ' + e.data.size;
                        content += '" href="' + e.data.href + '">' + e.data.button_text + '</a>';
                        editor.insertContent( content );
                    }

                } );
            }

        } );

    } );

} )();
