<?php
/**
 * Plugin Name: AOB Button
 * Description: Adds a custom button to TinyMCE
 * Plugin URI:
 * Version:     0.0.1
 * Author:      Army of Bees
 * Author URI:  http://www.armyofbees.com
 * License:     GPLv2
 * License URI: ./assets/license.txt
 * Text Domain:
 * Domain Path: /languages
 * Network:     false
 */

add_action( 'admin_head', 'fb_add_tinymce' );
function fb_add_tinymce() {
    global $typenow;

    /*
    // only on Post Type: post and page
    if( ! in_array( $typenow, array( 'post', 'page', 'story', 'guide' ) ) )
        return ; */

    add_filter( 'mce_external_plugins', 'fb_add_tinymce_plugin' );
    // Add to line 1 form WP TinyMCE
    add_filter( 'mce_buttons', 'fb_add_tinymce_button' );
}

// inlcude the js for tinymce
function fb_add_tinymce_plugin( $plugin_array ) {

    $plugin_array['fb_button'] = plugins_url( '/plugin.js', __FILE__ );
    // Print all plugin js path
    // var_dump( $plugin_array );
    return $plugin_array;
}

// Add the button key for address via JS
function fb_add_tinymce_button( $buttons ) {

    array_push( $buttons, 'fb_button_button_key' );
    // Print all buttons
    // var_dump( $buttons );
    return $buttons;
}
