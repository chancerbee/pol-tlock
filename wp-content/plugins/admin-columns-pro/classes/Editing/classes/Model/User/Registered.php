<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class ACP_Editing_Model_User_Registered extends ACP_Editing_Model {

	public function get_edit_value( $id ) {
		return date( 'Ymd', strtotime( get_userdata( $id )->user_registered ) );
	}

	public function get_view_settings() {
		return array(
			'type' => 'date',
		);
	}

	public function save( $id, $value ) {
		$time = strtotime( "1970-01-01 " . date( 'H:i:s', strtotime( get_userdata( $id )->user_registered ) ) );
		$date = date( 'Y-m-d H:i:s', strtotime( $value ) + $time );

		wp_update_user( array(
			'ID'              => $id,
			'user_registered' => $date,
		) );
	}

}
