# Points of Light

Welcome to the Points of Light repository.

This project is built and maintained by Army Of Bees and Narwhal Digital.

## SETUP

This Wordpress install is configured for Pantheon.

### 1. Local Development

Use whatever local setup you prefer, but min requirements are: PHP 7, and a mysql database.

Database migrations will be handled via *WP Migrate DB Pro* in the wordpress backend.

Configure your local database in `wp-config-local.php`, (create this file in the root directory, it is already ignored by git, use `wp-config-sample.php` as a starting point)

Generally, we want to enable Wordpress debugging during local development -- lets not pollute the site with warnings and notices that you can't see on your local environment. Set `define('WP_DEBUG', true);` in `wp-config-local.php`

Create an uploads folder at `/wp-content/uploads/` and set permissions to read-write, and make sure `/wp-content/mu-plugins/` folder is also read-writable to ensure smooth database migrations with WP Migrate DB Pro

Your local environment will need an .htaccess for pretty permalinks. Create `.htaccess`in the project root and add the following:
```
# BEGIN WordPress
<IfModule mod_rewrite.c>
RewriteEngine On
RewriteBase /
RewriteRule ^index\.php$ - [L]
RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d
RewriteRule . /index.php [L]
</IfModule>

# END WordPress
```

### 2. Codebase Management

We are using _Bitbucket Pipelines_ for continuous delivery to Pantheon. **_Do not push directly to Pantheon_**.

Pushing to master will automatically compile assets and deploy to Pantheon Dev environment, as will merging a PR into master on Bitbucket. To minimize conflicts compiled css/js are not tracked in source control.

#### Note: If you initially cloned your repo from Pantheon rather than Bitbucket, your 'origin' will incorrectly be Pantheon.

To fix this, open up `.git/config` and change the origin url from Pantheon to Bitbucket: Your config should look like this:

```
[remote "origin"]
  url = git@bitbucket.org:armyofbees/points-of-light.git
  fetch = +refs/heads/*:refs/remotes/origin/*
```

### 3. Asset Build Tooling

We use gulp to process and compile css and javascript, and npm to manage dependencies. If you don't already have npm installed, get it at [https://www.npmjs.com/get-npm]

**Install:**
  From the theme directory run `npm install` to install gulp and all project dependencies.

  *Do* commit any changes to `package-lock.json`, so we all stay up-to-date with packages/versions.

**Continuously watch & build:**
  From the theme directory run `gulp watch`

**Build once:**
  From the theme directory run `gulp`.

####Be sure to compile assets when pulling the latest or rebasing off master, as compiled js/css *is not tracked in source control*

## STYLE

Please refer to the Army of Bees style guide [here](#) [oops... link coming soon]

ES6 javascript is supported should you fancy it.

Please write _meaningful, useful_ commit messages, and try to keep a clean history.

## and remember... GLHF
